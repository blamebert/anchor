package games.sunken.anchor.chat.channel.impl;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.chat.channel.ChatChannel;
import games.sunken.anchor.chat.channel.StaffChatData;
import games.sunken.anchor.database.redis.Redis;
import games.sunken.anchor.player.SunkenPlayer;
import games.sunken.anchor.util.XSound;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.metadata.FixedMetadataValue;
import redis.clients.jedis.Jedis;

public class StaffChannel extends ChatChannel {

    private Anchor plugin;

    public StaffChannel(Anchor plugin) {
        super("Staff", "S", ChatColor.RED, ChatColor.RED, 0, true, false);

        this.plugin = plugin;
    }

    @Override
    public boolean canSeeChannel(SunkenPlayer player) {
        return player.getBukkitPlayer().hasPermission("sunken.chat.staff");
    }

    @Override
    public boolean sendMessage(SunkenPlayer sender, String message) {
        try {
            sender.getBukkitPlayer().setMetadata(getInternalName(), new FixedMetadataValue(plugin, System.currentTimeMillis()));

            if (plugin.isRedis()) {
                StaffChatData data = new StaffChatData(sender.getBukkitPlayer().getDisplayName(), plugin.getServerId(), prepareMessage(sender.getBukkitPlayer(), message));

                Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
                    try (Jedis jedis = Redis.getJedisPool().getResource()) {
                        jedis.publish(Anchor.STAFF_CHAT_CHANNEL, plugin.getGSON().toJson(data));
                    }
                });
            } else {
                for (SunkenPlayer sunkenPlayer : getListeners()) {
                    if (canSeeChannel(sunkenPlayer)) {
                        sunkenPlayer.getBukkitPlayer().spigot().sendMessage(prepareMessage(sender.getBukkitPlayer(), message));
                    }
                }
            }
        } catch (Exception e) {
            sender.sendMessage(MsgType.DANGER, XSound.BLOCK_NOTE_BLOCK_BASS, new TextComponent(e.getMessage()));
        }

        return true;
    }

    public boolean sendSystemMessage(String message) {
        try {
            if (plugin.isRedis()) {
                StaffChatData data = new StaffChatData("SYSTEM", plugin.getServerId(), prepareSystemMessage(message));

                Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
                    try (Jedis jedis = Redis.getJedisPool().getResource()) {
                        jedis.publish(Anchor.STAFF_CHAT_CHANNEL, plugin.getGSON().toJson(data));
                    }
                });
            } else {
                for (SunkenPlayer sunkenPlayer : getListeners()) {
                    if (canSeeChannel(sunkenPlayer)) {
                        sunkenPlayer.getBukkitPlayer().spigot().sendMessage(prepareSystemMessage(message));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

}