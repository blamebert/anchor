package games.sunken.anchor.chat.channel;

import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import org.bukkit.ChatColor;

public class StaffChatData {

    private String sender;
    private String serverId;
    private String message;

    public StaffChatData(String sender, String serverId, TextComponent message) {
        this.sender = sender;
        this.serverId = serverId;
        this.message = ComponentSerializer.toString(message);
    }

    public String getSender() {
        return sender;
    }

    public String getServerId() {
        return serverId;
    }

    public String getMessage() {
        return message;
    }

    public TextComponent toSend() {
        TextComponent component = new TextComponent(ChatColor.DARK_GRAY + "[" + ChatColor.DARK_AQUA + serverId + ChatColor.DARK_GRAY + "] ");
        component.addExtra(new TextComponent(ComponentSerializer.parse(message)));

        return component;
    }

}