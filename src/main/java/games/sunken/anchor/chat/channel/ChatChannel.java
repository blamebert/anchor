package games.sunken.anchor.chat.channel;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.chat.TimeFormatter;
import games.sunken.anchor.player.SunkenPlayer;
import games.sunken.anchor.util.Reflection;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.apache.commons.lang.WordUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

public abstract class ChatChannel {

    private String name;
    private String nickname;
    private ChatColor prefixColor;
    private ChatColor chatColor;
    private long chatDelay;
    private boolean itemLinking;
    private boolean colorOverride;

    public ChatChannel(String name, String nickname, ChatColor prefixColor, ChatColor chatColor, long chatDelay, boolean itemLinking, boolean colorOverride) {
        this.name = name;
        this.nickname = nickname;
        this.prefixColor = prefixColor;
        this.chatColor = chatColor;
        this.chatDelay = chatDelay;
        this.itemLinking = itemLinking;
        this.colorOverride = colorOverride;
    }

    public String getName() {
        return name;
    }

    public String getNickname() {
        return nickname;
    }

    public ChatColor getPrefixColor() {
        return prefixColor;
    }

    public ChatColor getChatColor() {
        return chatColor;
    }

    public long getChatDelay() {
        return chatDelay;
    }

    public boolean allowItemLinking() {
        return itemLinking;
    }

    public boolean allowColorOverride() {
        return colorOverride;
    }

    public String getPrefix() {
        return ChatColor.DARK_GRAY + "[" + getPrefixColor() + getNickname() + ChatColor.DARK_GRAY + "]";
    }

    public TextComponent prepareSystemMessage(String message) {
        TextComponent channelPrefix = new TextComponent(getPrefix() + " " + ChatColor.WHITE);

        TextComponent displayName = new TextComponent(ChatColor.DARK_RED + "" + ChatColor.BOLD + "SYSTEM" + ChatColor.DARK_GRAY + ": " + ChatColor.RED);

        TextComponent formatted = new TextComponent("");
        formatted.setColor(getPrefixColor().asBungee());
        formatted.addExtra(channelPrefix);
        formatted.addExtra(displayName);
        formatted.addExtra(message);

        return formatted;
    }

    public TextComponent prepareMessage(Player sender, String message) throws Exception {
        SunkenPlayer sunkenPlayer = Anchor.getPlugin(Anchor.class).getPlayerManager().getPlayer(sender.getUniqueId());
        message = message.replaceAll("(?i)" + Pattern.quote("[ITEM]"), "[item]");

        if (!sender.hasPermission("sunken.chat." + getName().toLowerCase().replaceAll(" ", "_"))) {
            throw new Exception("You do not have permission to chat in this channel!");
        }

        if (sender.hasMetadata(getInternalName())) {
            long lastMessage = sender.getMetadata(getInternalName()).get(0).asLong();

            if (System.currentTimeMillis() - lastMessage < getChatDelay() && !sender.hasPermission("sunken.nochatdelay")) {
                throw new Exception("You must wait another " + TimeFormatter.formatDateDiff(lastMessage + getChatDelay(), true)
                        + " before sending another message to this channel!");
            }
        }

        TextComponent channelPrefix = new TextComponent(getPrefix() + " " + ChatColor.WHITE);

        String prefix = sunkenPlayer.getPrefix() + (ChatColor.stripColor(sunkenPlayer.getPrefix()).isEmpty() ? "" : " ");
        String suffix = (ChatColor.stripColor(sunkenPlayer.getSuffix()).isEmpty() ? "" : " ") + sunkenPlayer.getSuffix();

        TextComponent playerDisplayName = new TextComponent(prefix + sender.getDisplayName() + suffix + ChatColor.DARK_GRAY + ": " + (allowColorOverride() ? sunkenPlayer.getChatColor() : getChatColor()));

        TextComponent itemChat = new TextComponent("");
        if (message.toLowerCase().contains("[item]")) {
            if (sender.hasPermission("sunken.itemchat")) {
                if (!allowItemLinking()) throw new Exception("This chat channel does not support item linking!");

                ItemStack item = (Reflection.VERSION_NUMBER < 190) ? sender.getInventory().getItemInHand() : (ItemStack) (sender.getInventory().getClass().getMethod("getItemInOffHand", ItemStack.class).invoke(ItemStack.class) != null ? sender.getInventory().getClass().getMethod("getItemInOffHand", ItemStack.class).invoke(ItemStack.class) : sender.getInventory().getClass().getMethod("getItemInMainHand", ItemStack.class).invoke(ItemStack.class));

                if (item == null) throw new Exception("You must be holding an item to link!");

                String itemJson = Anchor.getPlugin(Anchor.class).getItemUtil().serializeItemJSON(item);

                BaseComponent[] hoverEventComponents = new BaseComponent[]{new TextComponent(itemJson)};

                HoverEvent hoverEvent = new HoverEvent(HoverEvent.Action.SHOW_ITEM, hoverEventComponents);

                if (item.hasItemMeta() && !item.getItemMeta().getDisplayName().isEmpty()) {
                    itemChat = new TextComponent(ChatColor.DARK_GRAY + "[" + ChatColor.WHITE + item.getItemMeta().getDisplayName() + ChatColor.DARK_GRAY + "]" + (allowColorOverride() ? sunkenPlayer.getChatColor() : getChatColor()));
                } else {
                    itemChat = new TextComponent(ChatColor.DARK_GRAY + "[" + ChatColor.WHITE + WordUtils.capitalizeFully(item.getType().name().toLowerCase().replaceAll("_", " ")) + ChatColor.DARK_GRAY + "]" + (allowColorOverride() ? sunkenPlayer.getChatColor() : getChatColor()));
                }

                itemChat.setHoverEvent(hoverEvent);
            } else {
                throw new Exception("You do not have permission to link items in chat!");
            }
        }

        TextComponent formatted = new TextComponent("");
        formatted.setColor(getPrefixColor().asBungee());
        formatted.addExtra(channelPrefix);
        formatted.addExtra(playerDisplayName);

        if (!itemChat.getText().isEmpty()) {
            if (message.contains("[item]")) {
                String[] pieces = message.split(Pattern.quote("[item]"));

                if (pieces.length >= 1 && pieces[0] != null && !pieces[0].isEmpty()) {
                    TextComponent part1 = new TextComponent((allowColorOverride() ? sunkenPlayer.getChatColor() : getChatColor()) + pieces[0].replaceAll(Pattern.quote("["), ""));
                    part1.setColor((allowColorOverride() ? sunkenPlayer.getChatColor() : getChatColor()).asBungee());

                    formatted.addExtra(part1);
                }

                formatted.addExtra(itemChat);

                if (pieces.length > 1 && pieces[1] != null && !pieces[1].isEmpty()) {
                    TextComponent part2 = new TextComponent((allowColorOverride() ? sunkenPlayer.getChatColor() : getChatColor()) + pieces[1].replaceAll(Pattern.quote("]"), "") + (allowColorOverride() ? sunkenPlayer.getChatColor() : getChatColor()));
                    part2.setColor((allowColorOverride() ? sunkenPlayer.getChatColor() : getChatColor()).asBungee());

                    formatted.addExtra(part2);
                }
            }
        } else {
            TextComponent msg = new TextComponent((allowColorOverride() ? sunkenPlayer.getChatColor() : getChatColor()) + message);
            msg.setColor((allowColorOverride() ? sunkenPlayer.getChatColor() : getChatColor()).asBungee());

            formatted.addExtra(msg);
        }

        return formatted;
    }

    public Set<SunkenPlayer> getListeners() {
        Set<SunkenPlayer> players = new HashSet<>();

        for (SunkenPlayer player : Anchor.getPlugin(Anchor.class).getPlayerManager().getOnlinePlayers()) {
            if (canSeeChannel(player)) {
                players.add(player);
            }
        }

        return players;
    }

    public String getInternalName() {
        return "chatchannel-" + getName().toLowerCase().replaceAll(" ", "_");
    }

    public abstract boolean canSeeChannel(SunkenPlayer player);

    public abstract boolean sendMessage(SunkenPlayer sender, String message);

}