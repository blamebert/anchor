package games.sunken.anchor.chat.channel;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.database.redis.RedisModule;
import games.sunken.anchor.player.SunkenPlayer;

import javax.annotation.Nonnull;
import java.util.function.BiConsumer;

public class StaffChatRedisModule implements RedisModule {

    @Nonnull
    @Override
    public String getName() {
        return "Staff Chat Receiver";
    }

    @Nonnull
    @Override
    public String getChannel() {
        return Anchor.STAFF_CHAT_CHANNEL;
    }

    @Override
    public boolean isDaemonThread() {
        return true;
    }

    @Override
    public BiConsumer<String, String> onMessage() {
        return (channel, message) -> {
            StaffChatData data = Anchor.getPlugin(Anchor.class).getGSON().fromJson(message, StaffChatData.class);

            for (SunkenPlayer player : Anchor.getPlugin(Anchor.class).getChannelManager().getByName("staff").getListeners()) {
                player.getBukkitPlayer().spigot().sendMessage(data.toSend());
            }
        };
    }

    @Override
    public BiConsumer<String, Integer> onSubscribe() {
        return (s, integer) -> Anchor.getPlugin(Anchor.class).getLogger().info("*** Staff Chat Module Started! ***");
    }

    @Override
    public BiConsumer<String, Integer> onUnsubscribe() {
        return (s, integer) -> Anchor.getPlugin(Anchor.class).getLogger().info("*** Staff Chat Module Stopped! ***");
    }

    @Override
    public Runnable onDisable() {
        return null;
    }

}