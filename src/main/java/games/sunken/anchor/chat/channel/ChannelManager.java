package games.sunken.anchor.chat.channel;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ChannelManager {

    private ChatChannel defaultChannel;
    private Map<String, ChatChannel> nameToChannel;
    private Map<String, ChatChannel> codeToChannel;

    public ChannelManager(ChatChannel defaultChannel) {
        this.defaultChannel = defaultChannel;
        this.nameToChannel = new HashMap<>();
        this.codeToChannel = new HashMap<>();

        register(defaultChannel);
    }

    public void register(ChatChannel chatChannel) {
        nameToChannel.put(chatChannel.getName().toLowerCase(), chatChannel);
        codeToChannel.put(chatChannel.getNickname().toLowerCase(), chatChannel);

        if (defaultChannel == null) {
            defaultChannel = chatChannel;
        }
    }

    public Collection<ChatChannel> getChannels() {
        return nameToChannel.values();
    }

    public ChatChannel getDefaultChannel() {
        return defaultChannel;
    }

    public ChatChannel getByName(String name) {
        return nameToChannel.getOrDefault(name.toLowerCase(), null);
    }

    public ChatChannel getByShortcut(String shortcut) {
        return codeToChannel.getOrDefault(shortcut.toLowerCase(), null);
    }

}