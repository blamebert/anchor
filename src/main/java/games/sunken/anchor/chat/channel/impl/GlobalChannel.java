package games.sunken.anchor.chat.channel.impl;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.chat.channel.ChatChannel;
import games.sunken.anchor.player.SunkenPlayer;
import games.sunken.anchor.util.XSound;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.metadata.FixedMetadataValue;

public class GlobalChannel extends ChatChannel {

    public GlobalChannel() {
        super("Global", "G", ChatColor.GRAY, ChatColor.GRAY, 1000 * 3, true, true);
    }

    @Override
    public boolean canSeeChannel(SunkenPlayer player) {
        return !player.isHiding(this);
    }

    @Override
    public boolean sendMessage(SunkenPlayer sender, String message) {
        try {
            for (SunkenPlayer sunkenPlayer : getListeners()) {
                if (canSeeChannel(sunkenPlayer)) {
                    sunkenPlayer.getBukkitPlayer().spigot().sendMessage(prepareMessage(sender.getBukkitPlayer(), message));
                }
            }

            sender.getBukkitPlayer().setMetadata(getInternalName(), new FixedMetadataValue(Anchor.getPlugin(Anchor.class), System.currentTimeMillis()));
        } catch (Exception e) {
            sender.sendMessage(MsgType.DANGER, XSound.BLOCK_NOTE_BLOCK_BASS, new TextComponent(e.getMessage()));
        }

        return true;
    }

}