package games.sunken.anchor.chat;

import net.md_5.bungee.api.ChatColor;

import java.util.regex.Pattern;

public enum MsgType {

    SYSTEM(ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "SUNKEN" + ChatColor.DARK_GRAY + "│" + ChatColor.AQUA, ChatColor.AQUA, ChatColor.DARK_AQUA),
    INFO(ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "S" + ChatColor.DARK_GRAY + "│" + ChatColor.AQUA, ChatColor.AQUA, ChatColor.WHITE),
    SUCCESS(ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "S" + ChatColor.DARK_GRAY + "│" + ChatColor.GREEN, ChatColor.GREEN, ChatColor.WHITE),
    WARNING(ChatColor.GOLD + "" + ChatColor.BOLD + "S" + ChatColor.DARK_GRAY + "│" + ChatColor.YELLOW, ChatColor.YELLOW, ChatColor.WHITE),
    DANGER(ChatColor.DARK_RED + "" + ChatColor.BOLD + "S" + ChatColor.DARK_GRAY + "│" + ChatColor.RED, ChatColor.RED, ChatColor.WHITE),
    JOIN(ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "<+>" + ChatColor.GREEN, ChatColor.GREEN, ChatColor.WHITE),
    LEAVE(ChatColor.DARK_RED + "" + ChatColor.BOLD + "<->" + ChatColor.RED, ChatColor.RED, ChatColor.WHITE),
    CENTERED("", ChatColor.RESET, ChatColor.RESET),
    DEFAULT("", ChatColor.RESET, ChatColor.RESET);

    private String prefix;
    private ChatColor primaryColor;
    private ChatColor accentColor;

    MsgType(String prefix, ChatColor primaryColor, ChatColor accentColor) {
        this.prefix = prefix;
        this.primaryColor = primaryColor;
        this.accentColor = accentColor;
    }

    public String getPrefix() {
        return prefix;
    }

    public ChatColor getPrimaryColor() {
        return primaryColor;
    }

    public ChatColor getAccentColor() {
        return accentColor;
    }

    public String format(String msg, Object... args) {
        if (this == CENTERED) {
            if (msg == null || msg.equals("")) return "";

            String message = this.primaryColor + msg;

            if (args.length > 0) {
                for (Object arg : args) {
                    message = message.replaceFirst(Pattern.quote("<p>"), arg.toString());
                }
            }

            final int CENTER_PX = 154;
            int messagePxSize = 0;
            boolean previousCode = false;
            boolean isBold = false;

            for (char c : message.toCharArray()) {
                if (c == '§') {
                    previousCode = true;
                } else if (previousCode) {
                    previousCode = false;
                    isBold = c == 'l' || c == 'L';
                } else {
                    DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
                    messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
                    messagePxSize++;
                }
            }

            int halvedMessageSize = messagePxSize / 2;
            int toCompensate = CENTER_PX - halvedMessageSize;
            int spaceLength = DefaultFontInfo.SPACE.getLength() + 1;
            int compensated = 0;

            StringBuilder sb = new StringBuilder();

            while (compensated < toCompensate) {
                sb.append(" ");
                compensated += spaceLength;
            }

            return sb.toString() + message;
        } else {
            if (args.length == 0) {
                return this.prefix + this.primaryColor + " " + msg;
            }

            String formatted = this.prefix + this.primaryColor + " " + msg;

            for (Object arg : args) {
                formatted = formatted.replaceFirst(Pattern.quote("<p>"), this.accentColor + arg.toString() + this.primaryColor);
            }

            return formatted;
        }
    }

}