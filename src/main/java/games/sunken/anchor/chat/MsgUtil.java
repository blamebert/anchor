package games.sunken.anchor.chat;

import com.comphenix.protocol.wrappers.WrappedChatComponent;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import org.bukkit.ChatColor;

public class MsgUtil {
    public static ChatColor getChatColorById(int id) {
        for (ChatColor value : ChatColor.values()) { // I would just use reflection for this, but I'm trying not to make performance issues via retrieving a chat color.
            if (getChatColorId(value) == id) {
                return value;
            }
        }

        return null;
    }

    public static int getChatColorId(ChatColor chatColor) {
        int chatColorId = 0;

        for (ChatColor value : ChatColor.values()) { // I would just use reflection for this, but I'm trying not to make performance issues via retrieving a chat color.
            if (value == chatColor) {
                break;
            }
            chatColorId++;
        }

        return chatColorId;
    }

    public static String retrieveLegacyText(WrappedChatComponent wrappedChatComponent) { return retrieveLegacyText(ComponentSerializer.parse(wrappedChatComponent.getJson())); }

    public static String retrieveLegacyText(BaseComponent[] baseComponent) {
        StringBuilder string = new StringBuilder();

        for(BaseComponent baseComponentObject : baseComponent) {
            string.append(baseComponentObject.toLegacyText());
        }

        return string.toString();
    }
}
