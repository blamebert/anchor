package games.sunken.anchor.chat;

import games.sunken.anchor.Anchor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.List;

public class ChatPaginator {

    private Player player;

    private String command;

    private int page = 1;
    private int pages;
    private int pageSize;

    private List<TextComponent> messages;

    public ChatPaginator(Player player, String command, int pageSize, List<TextComponent> messages) {
        this.player = player;
        this.command = command;
        this.pageSize = pageSize;
        this.pages = (int) Math.ceil((double) messages.size() / (double) pageSize);
        this.messages = messages;

        Bukkit.getPluginManager().registerEvents(new PagedChatListener(this), Anchor.getPlugin(Anchor.class));

        setPage(1);
    }

    public Player getPlayer() {
        return player;
    }

    public String getCommand() {
        return command;
    }

    public int getPage() {
        return page;
    }

    public int getPages() {
        return pages;
    }

    public int getPageSize() {
        return pageSize;
    }

    public List<TextComponent> getMessages() {
        return messages;
    }

    public void setPage(int page) {
        this.page = page;

        //if (messages.size() % pageSize > 0) pages++;

        if (page <= 0) {
            this.page = 1;
            return;
        }

        if (page > pages) {
            this.page = pages;
            return;
        }

        for (int i = 0; i < getMessages().size(); i++) {
            if (i >= (getPage() - 1) * getPageSize() && i < getPage() * getPageSize()) {
                player.sendMessage(messages.get(i));
            }
        }

        player.sendMessage(" ");

        if (pages > 1) {
            TextComponent main = new TextComponent("");

            if (page > 1) {
                TextComponent previous = new TextComponent(ChatColor.DARK_AQUA + "" + ChatColor.BOLD + " <- " + ChatColor.WHITE + "Previous Page");
                previous.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Click " + ChatColor.GRAY + "to go to the previous page.").create()));
                previous.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command + " previous"));

                main.addExtra(previous);

                main.addExtra(new TextComponent(ChatColor.GRAY + " (" + getPage()));
                main.addExtra(new TextComponent(ChatColor.DARK_GRAY + "/"));
                main.addExtra(new TextComponent(ChatColor.GRAY + "" + getPages() + ") "));
            }

            if (page + 1 <= pages) {
                TextComponent next = new TextComponent(ChatColor.WHITE + "Next Page " + ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "->");
                next.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Click " + ChatColor.GRAY + "to go to the next page.").create()));
                next.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command + " next"));

                if (page == 1) {
                    main.addExtra(new TextComponent(ChatColor.GRAY + " (" + getPage()));
                    main.addExtra(new TextComponent(ChatColor.DARK_GRAY + "/"));
                    main.addExtra(new TextComponent(ChatColor.GRAY + "" + getPages() + ") "));
                }

                main.addExtra(next);
            }

            player.sendMessage(main);
            player.sendMessage(" ");
        }
    }

    private class PagedChatListener implements Listener {

        private ChatPaginator chatPaginator;

        public PagedChatListener(ChatPaginator chatPaginator) {
            this.chatPaginator = chatPaginator;
        }

        @EventHandler
        public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
            if (this.chatPaginator.getPlayer().equals(event.getPlayer())) {
                if (event.getMessage().startsWith(this.chatPaginator.getCommand())) {
                    String message = event.getMessage().substring(this.chatPaginator.getCommand().length()).trim();

                    if (!message.isEmpty()) {
                        if (message.equalsIgnoreCase("next")) {
                            this.chatPaginator.setPage(this.chatPaginator.getPage() + 1);
                            event.setCancelled(true);
                        } else if (message.equalsIgnoreCase("previous")) {
                            this.chatPaginator.setPage(this.chatPaginator.getPage() - 1);
                            event.setCancelled(true);
                        }
                    }
                }
            }
        }

        @EventHandler
        public void onPlayerQuit(PlayerQuitEvent event) {
            if (this.chatPaginator.getPlayer().equals(event.getPlayer())) {
                HandlerList.unregisterAll(this);
            }
        }

    }

}