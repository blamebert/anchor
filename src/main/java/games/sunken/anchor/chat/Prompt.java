package games.sunken.anchor.chat;

import games.sunken.anchor.util.XSound;
import org.apache.commons.lang.Validate;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;

public abstract class Prompt {

    private final MsgType msgType;
    private final String promptText;

    private XSound xSound;
    private List<String> subtext;

    public Prompt(MsgType msgType, String promptText, @Nullable XSound xSound, @Nullable String... subtext) {
        this.msgType = msgType;
        this.promptText = promptText;
        this.xSound = xSound;

        if (subtext != null) {
            this.subtext = Arrays.asList(subtext);
        }
    }

    public void sendMessage(Player player) {
        Validate.notNull(msgType, "msgType cannot be null!");
        Validate.notNull(promptText, "promptText cannot be null!");

        player.playSound(player.getLocation(), this.xSound.parseSound(), 1f, 1f);

        player.sendMessage(" ");
        player.sendMessage(msgType.format(promptText));

        if (subtext != null && !subtext.isEmpty()) {
            for (String string : subtext) {
                player.sendMessage("§7§o" + string);
            }
        }

        player.sendMessage(" ");
    }

    public abstract boolean onSubmit(Player player, String submittedText);

    public abstract void onCancel(Player player);

}