package games.sunken.anchor.util;

import org.bukkit.ChatColor;
import org.bukkit.DyeColor;

public enum Color {

    DARK_GREEN("Green", ChatColor.DARK_GREEN, org.bukkit.Color.fromRGB(30, 130, 76), DyeColor.GREEN),
    DARK_AQUA("Teal", ChatColor.DARK_AQUA, org.bukkit.Color.fromRGB(0, 190, 190), DyeColor.CYAN),
    DARK_PURPLE("Purple", ChatColor.DARK_PURPLE, org.bukkit.Color.fromRGB(190, 0, 190), DyeColor.PURPLE),
    GOLD("Orange", ChatColor.GOLD, org.bukkit.Color.fromRGB(217, 163, 52), DyeColor.ORANGE),
    SILVER("Silver", ChatColor.GRAY, org.bukkit.Color.fromBGR(189, 195, 199), (Reflection.VERSION_NUMBER >= 190) ? DyeColor.valueOf("LIGHT_GRAY") : DyeColor.valueOf("SILVER")),
    GRAY("Gray", ChatColor.DARK_GRAY, org.bukkit.Color.fromRGB(46, 49, 49), DyeColor.GRAY),
    BLUE("Blue", ChatColor.BLUE, org.bukkit.Color.fromRGB(63, 63, 254), DyeColor.BLUE),
    GREEN("Lime", ChatColor.GREEN, org.bukkit.Color.fromRGB(63, 254, 63), DyeColor.LIME),
    AQUA("Aqua", ChatColor.AQUA, org.bukkit.Color.fromRGB(63, 254, 254), DyeColor.LIGHT_BLUE),
    RED("Red", ChatColor.RED, org.bukkit.Color.fromRGB(254, 63, 63), DyeColor.RED),
    PINK("Pink", ChatColor.LIGHT_PURPLE, org.bukkit.Color.fromRGB(254, 63, 254), DyeColor.PINK),
    YELLOW("Yellow", ChatColor.YELLOW, org.bukkit.Color.fromRGB(254, 254, 63), DyeColor.YELLOW),
    WHITE("White", ChatColor.WHITE, org.bukkit.Color.fromRGB(255, 255, 255), DyeColor.WHITE),
    BLACK("Black", ChatColor.BLACK, org.bukkit.Color.fromBGR(0, 0, 0), DyeColor.BLACK);

    private String name;
    private ChatColor chatColor;
    private org.bukkit.Color color;
    private DyeColor dyeColor;

    Color(String name, ChatColor chatColor, org.bukkit.Color color, DyeColor dyeColor) {
        this.name = name;
        this.chatColor = chatColor;
        this.color = color;
        this.dyeColor = dyeColor;
    }

    public String getName() {
        return name;
    }

    public ChatColor getChatColor() {
        return chatColor;
    }

    public org.bukkit.Color getColor() {
        return color;
    }

    public DyeColor getDyeColor() {
        return dyeColor;
    }

    public static Color getByName(String name) {
        for (Color color : Color.values()) {
            if (color.getName().equalsIgnoreCase(name)) {
                return color;
            }
        }

        return null;
    }

}