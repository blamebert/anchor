package games.sunken.anchor.util;

import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import games.sunken.anchor.util.packetwrapper.WrapperPlayServerTitle;
import games.sunken.anchor.util.packetwrapper.packetoverlay.OverlayPlayServerTitle;
import org.bukkit.entity.Player;

public class TitleUtil {

    public static void send(Player player, String title, String subtitle, String actionBar, int fadeInTime, int showTime, int fadeOutTime) {
        try {
            if(title != null && !title.equals("")) {
                new OverlayPlayServerTitle(EnumWrappers.TitleAction.TITLE, WrappedChatComponent.fromText(title), fadeInTime, showTime, fadeOutTime).sendPacket(player);
            }

            if(subtitle != null && !subtitle.equals("")) {
                new OverlayPlayServerTitle(EnumWrappers.TitleAction.SUBTITLE, WrappedChatComponent.fromText(subtitle), fadeInTime, showTime, fadeOutTime).sendPacket(player);
            }

            if(actionBar != null && !actionBar.equals("")) {
                new OverlayPlayServerTitle(EnumWrappers.TitleAction.ACTIONBAR, WrappedChatComponent.fromText(actionBar), fadeInTime, showTime, fadeOutTime).sendPacket(player);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void send(Player player, String title, String subtitle, String actionBar) { send(player, title, subtitle, actionBar, 0, 60, 0); }

    public static void sendTitle(Player player, String title, int fadeInTime, int showTime, int fadeOutTime) { send(player, title, null, null, fadeInTime, showTime, fadeOutTime); }

    public static void sendTitle(Player player, String title) {
        sendTitle(player, title, 0, 60, 0);
    }

    public static void sendSubtitle(Player player, String subtitle, int fadeInTime, int showTime, int fadeOutTime) { send(player, null, subtitle, null, fadeInTime, showTime, fadeOutTime); }

    public static void sendSubtitle(Player player, String subtitle) {
        sendSubtitle(player, subtitle, 0, 60, 0);
    }

    public static void sendActionBar(Player player, String actionBar, int fadeInTime, int showTime, int fadeOutTime) { send(player, null, actionBar, null, fadeInTime, showTime, fadeOutTime); }

    public static void sendActionBar(Player player, String actionBar) {
        sendActionBar(player, actionBar, 0, 60, 0);
    }

}
