package games.sunken.anchor.util.packetwrapper.packetoverlay;

// Overlay to allow for cross compatibility when sending the packet for an action bar.

import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import games.sunken.anchor.util.Reflection;
import games.sunken.anchor.util.packetwrapper.WrapperPlayServerChat;
import games.sunken.anchor.util.packetwrapper.WrapperPlayServerTitle;
import org.bukkit.entity.Player;

public class OverlayPlayServerTitle {
    private WrapperPlayServerChat wrapperPlayServerChat;
    private WrapperPlayServerTitle wrapperPlayServerTitle;

    public OverlayPlayServerTitle(EnumWrappers.TitleAction action, WrappedChatComponent title, int fadeIn, int stay, int fadeOut) {
        if (action == EnumWrappers.TitleAction.ACTIONBAR && Reflection.VERSION_NUMBER < 190) {
            this.wrapperPlayServerChat = new WrapperPlayServerChat(title, (byte) 2);
        } else {
            this.wrapperPlayServerTitle = new WrapperPlayServerTitle(action, title, fadeIn, stay, fadeOut);
        }
    }

    public void sendPacket(Player player) {
        if (this.wrapperPlayServerChat != null) {
            this.wrapperPlayServerChat.sendPacket(player);
        } else {
            this.wrapperPlayServerTitle.sendPacket(player);
        }
    }
}

