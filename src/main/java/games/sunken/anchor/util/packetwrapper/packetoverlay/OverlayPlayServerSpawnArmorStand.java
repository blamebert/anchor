package games.sunken.anchor.util.packetwrapper.packetoverlay;

// Overlay to allow for cross compatibility when sending the packet for an armor stand.

import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import games.sunken.anchor.util.Reflection;
import games.sunken.anchor.util.packetwrapper.WrapperPlayServerEntityMetadata;
import games.sunken.anchor.util.packetwrapper.WrapperPlayServerSpawnEntity;
import games.sunken.anchor.util.packetwrapper.WrapperPlayServerSpawnEntityLiving;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.UUID;

public class OverlayPlayServerSpawnArmorStand {
    private WrapperPlayServerEntityMetadata wrapperPlayServerEntityMetadata;
    private WrapperPlayServerSpawnEntity wrapperPlayServerSpawnEntity;
    private WrapperPlayServerSpawnEntityLiving wrapperPlayServerSpawnEntityLiving;

    public OverlayPlayServerSpawnArmorStand(UUID uuid, int entityID, double x, double y, double z, float yaw, float pitch, float headPitch, double velocityX, double velocityY, double velocityZ, WrappedDataWatcher wrappedDataWatcher) {
        if (Reflection.VERSION_NUMBER > 199) {
            this.wrapperPlayServerEntityMetadata = new WrapperPlayServerEntityMetadata(entityID, wrappedDataWatcher.getWatchableObjects());
            this.wrapperPlayServerSpawnEntity = new WrapperPlayServerSpawnEntity(entityID, uuid, x, y, z, velocityX, velocityY, velocityZ, pitch, yaw, WrapperPlayServerSpawnEntity.ObjectTypes.ARMORSTAND);
        } else {
            this.wrapperPlayServerSpawnEntityLiving = new WrapperPlayServerSpawnEntityLiving(uuid, entityID, EntityType.ARMOR_STAND, x, y, z, yaw, pitch, headPitch, velocityX, velocityY, velocityZ, wrappedDataWatcher);
        }
    }

    public int getEntityID() {
        return Reflection.VERSION_NUMBER > 199 ? this.wrapperPlayServerSpawnEntity.getEntityID() : this.wrapperPlayServerSpawnEntityLiving.getEntityID();
    }

    public void sendPacket(Player player) {
        if (Reflection.VERSION_NUMBER > 199) {
            this.wrapperPlayServerSpawnEntity.sendPacket(player);
            this.wrapperPlayServerEntityMetadata.sendPacket(player);
        } else {
            this.wrapperPlayServerSpawnEntityLiving.sendPacket(player);
        }
    }
}
