package games.sunken.anchor.util.packetwrapper;

/**
 * PacketWrapper - ProtocolLib wrappers for Minecraft packets
 * Copyright (C) dmulloy2 <http://dmulloy2.net>
 * Copyright (C) Kristian S. Strangeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Modified by Fabelz for instantiation construction.
 *
 */

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.reflect.IntEnum;
import com.comphenix.protocol.utility.MinecraftReflection;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import games.sunken.anchor.chat.MsgUtil;
import games.sunken.anchor.util.Reflection;
import org.bukkit.ChatColor;

import java.util.Collection;
import java.util.List;

public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
    public static final PacketType TYPE =
            PacketType.Play.Server.SCOREBOARD_TEAM;

    public WrapperPlayServerScoreboardTeam() {
        super(new PacketContainer(TYPE), TYPE);
        handle.getModifier().writeDefaults();
    }

    public WrapperPlayServerScoreboardTeam(String name, WrappedChatComponent displayName, WrappedChatComponent prefix, WrappedChatComponent suffix, String nameTagVisibility, ChatColor color, String collisionRule, List<String> players, int mode, int packOptionData) {
        this();

        setName(name);
        setDisplayName(displayName);
        setPrefix(prefix);
        setSuffix(suffix);
        setNameTagVisibility(nameTagVisibility);
        setColor(color);
        setCollisionRule(collisionRule);
        setPlayers(players);
        setMode(mode);
        setPackOptionData(packOptionData);
    }

    public WrapperPlayServerScoreboardTeam(PacketContainer packet) {
        super(packet, TYPE);
    }

    /**
     * Enum containing all known modes.
     *
     * @author dmulloy2
     */
    public static class Mode extends IntEnum {
        public static final int TEAM_CREATED = 0;
        public static final int TEAM_REMOVED = 1;
        public static final int TEAM_UPDATED = 2;
        public static final int PLAYERS_ADDED = 3;
        public static final int PLAYERS_REMOVED = 4;

        private static final Mode INSTANCE = new Mode();

        public static Mode getInstance() {
            return INSTANCE;
        }
    }

    /**
     * Retrieve Team Name.
     * <p>
     * Notes: a unique name for the team. (Shared with scoreboard).
     *
     * @return The current Team Name
     */
    public String getName() {
        return handle.getStrings().read(0);
    }

    /**
     * Set Team Name.
     *
     * @param value - new value.
     */
    public void setName(String value) {
        handle.getStrings().write(0, value);
    }

    /**
     * Retrieve Team Display Name.
     * <p>
     * Notes: only if Mode = 0 or 2.
     *
     * @return The current Team Display Name
     */
    public WrappedChatComponent getDisplayName() {
        return Reflection.VERSION_NUMBER > 189 ? handle.getChatComponents().read(0) : WrappedChatComponent.fromText(handle.getStrings().read(1));
    }

    /**
     * Set Team Display Name.
     *
     * @param value - new value.
     */
    public void setDisplayName(WrappedChatComponent value) {
        if (Reflection.VERSION_NUMBER > 189) {
            handle.getChatComponents().write(0, value);
        } else {
            handle.getStrings().write(1, value != null ? value.getJson().substring(1, value.getJson().length()-1) : null);
        }
    }

    /**
     * Retrieve Team Prefix.
     * <p>
     * Notes: only if Mode = 0 or 2. Displayed before the players' name that are
     * part of this team.
     *
     * @return The current Team Prefix
     */
    public WrappedChatComponent getPrefix() { return Reflection.VERSION_NUMBER > 189 ? handle.getChatComponents().read(1) : WrappedChatComponent.fromText(handle.getStrings().read(2)); }

    /**
     * Set Team Prefix.
     *
     * @param value - new value.
     */
    public void setPrefix(WrappedChatComponent value) {
        if (Reflection.VERSION_NUMBER > 189) {
            handle.getChatComponents().write(1, value);
        } else {
            handle.getStrings().write(2, value != null ? value.getJson().substring(1, value.getJson().length()-1) : null);
        }
    }

    /**
     * Retrieve Team Suffix.
     * <p>
     * Notes: only if Mode = 0 or 2. Displayed after the players' name that are
     * part of this team.
     *
     * @return The current Team Suffix
     */
    public WrappedChatComponent getSuffix() { return Reflection.VERSION_NUMBER > 189 ? handle.getChatComponents().read(2) : WrappedChatComponent.fromText(handle.getStrings().read(3)); }

    /**
     * Set Team Suffix.
     *
     * @param value - new value.
     */
    public void setSuffix(WrappedChatComponent value) {
        if (Reflection.VERSION_NUMBER > 189) {
            handle.getChatComponents().write(2, value);
        } else {
            handle.getStrings().write(3, value != null ? value.getJson().substring(1, value.getJson().length()-1) : null);
        }
    }

    /**
     * Retrieve Name Tag Visibility.
     * <p>
     * Notes: only if Mode = 0 or 2. always, hideForOtherTeams, hideForOwnTeam,
     * never.
     *
     * @return The current Name Tag Visibility
     */
    public String getNameTagVisibility() { return Reflection.VERSION_NUMBER > 189 ? handle.getStrings().read(4) : handle.getStrings().read(1); }

    /**
     * Set Name Tag Visibility.
     *
     * @param value - new value.
     */
    public void setNameTagVisibility(String value) { handle.getStrings().write(Reflection.VERSION_NUMBER > 189 ? 1 : 4, value); }

    /**
     * Retrieve Color.
     * <p>
     * Notes: only if Mode = 0 or 2. Same as Chat colors.
     *
     * @return The current Color
     */
    public ChatColor getColor() { return Reflection.VERSION_NUMBER > 189 ? handle.getEnumModifier(ChatColor.class, MinecraftReflection.getMinecraftClass("EnumChatFormat")).read(0) : MsgUtil.getChatColorById(handle.getIntegers().read(0)); }

    /**
     * Set Color.
     *
     * @param value - new value.
     */
    public void setColor(ChatColor value) {
        if (Reflection.VERSION_NUMBER > 189) {
            handle.getEnumModifier(ChatColor.class, MinecraftReflection.getMinecraftClass("EnumChatFormat")).write(0, value);
        } else {
            handle.getIntegers().write(0, MsgUtil.getChatColorId(value));
        }
    }

    /**
     * Get the collision rule.
     * Notes: only if Mode = 0 or 2. always, pushOtherTeams, pushOwnTeam, never.
     * @return The current collision rule
     */
    public String getCollisionRule() {
        return handle.getStrings().read(2);
    }

    /**
     * Sets the collision rule.
     * @param value - new value.
     */
    public void setCollisionRule(String value) {
        if (Reflection.VERSION_NUMBER >= 190) handle.getStrings().write(2, value);
    }

    /**
     * Retrieve Players.
     * <p>
     * Notes: only if Mode = 0 or 3 or 4. Players to be added/remove from the
     * team. Max 40 characters so may be uuid's later
     *
     * @return The current Players
     */
    @SuppressWarnings("unchecked")
    public List<String> getPlayers() {
        return (List<String>) handle.getSpecificModifier(Collection.class)
                .read(0);
    }

    /**
     * Set Players.
     *
     * @param value - new value.
     */
    public void setPlayers(List<String> value) {
        handle.getSpecificModifier(Collection.class).write(0, value);
    }

    /**
     * Retrieve Mode.
     * <p>
     * Notes: if 0 then the team is created. If 1 then the team is removed. If 2
     * the team team information is updated. If 3 then new players are added to
     * the team. If 4 then players are removed from the team.
     *
     * @return The current Mode
     */
    public int getMode() {
        return handle.getIntegers().read(Reflection.VERSION_NUMBER > 189 ? 0 : 1);
    }

    /**
     * Set Mode.
     *
     * @param value - new value.
     */
    public void setMode(int value) {
        handle.getIntegers().write(Reflection.VERSION_NUMBER > 189 ? 0 : 1, value);
    }

    /**
     * Retrieve pack option data. Pack data is calculated as follows:
     *
     * <pre>
     * <code>
     * int data = 0;
     * if (team.allowFriendlyFire()) {
     *     data |= 1;
     * }
     * if (team.canSeeFriendlyInvisibles()) {
     *     data |= 2;
     * }
     * </code>
     * </pre>
     *
     * @return The current pack option data
     */
    public int getPackOptionData() {
        return handle.getIntegers().read(Reflection.VERSION_NUMBER > 189 ? 1 : 2);
    }

    /**
     * Set pack option data.
     *
     * @param value - new value
     * @see #getPackOptionData()
     */
    public void setPackOptionData(int value) { handle.getIntegers().write(Reflection.VERSION_NUMBER > 189 ? 1 : 2, value); }
}
