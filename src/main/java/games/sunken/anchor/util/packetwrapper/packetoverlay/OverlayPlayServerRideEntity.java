package games.sunken.anchor.util.packetwrapper.packetoverlay;

// Overlay to allow for cross compatibility when sending the packet to mount an entity.

import games.sunken.anchor.util.Reflection;
import games.sunken.anchor.util.packetwrapper.WrapperPlayServerAttachEntity;
import games.sunken.anchor.util.packetwrapper.WrapperPlayServerMount;
import org.bukkit.entity.Player;

public class OverlayPlayServerRideEntity {
    private WrapperPlayServerAttachEntity wrapperPlayServerAttachEntity;
    private WrapperPlayServerMount wrapperPlayServerMount;

    public OverlayPlayServerRideEntity() {
        if (Reflection.VERSION_NUMBER < 190) {
            this.wrapperPlayServerAttachEntity = new WrapperPlayServerAttachEntity();
        } else {
            this.wrapperPlayServerMount = new WrapperPlayServerMount();
        }
    }

    public OverlayPlayServerRideEntity(int[] passengerEntityID, int vehicleEntityID) {
        this();

        setPassengerID(passengerEntityID);
        setVehicleID(vehicleEntityID);
    }

    public int getVehicleID() {
        return this.wrapperPlayServerAttachEntity != null ? this.wrapperPlayServerAttachEntity.getVehicleId() : this.wrapperPlayServerMount.getEntityID();
    }

    public void setVehicleID(int vehicleID) {
        if (this.wrapperPlayServerAttachEntity != null) {
            this.wrapperPlayServerAttachEntity.setVehicleId(vehicleID);
        } else {
            this.wrapperPlayServerMount.setEntityID(vehicleID);
        }
    }

    public int[] getPassengerID() {
        return this.wrapperPlayServerAttachEntity != null ? new int[] {this.wrapperPlayServerAttachEntity.getEntityID()} : this.wrapperPlayServerMount.getPassengerIds();
    }

    public void setPassengerID(int[] passengerEntityID) {
        if (this.wrapperPlayServerAttachEntity != null) {
            this.wrapperPlayServerAttachEntity.setEntityID(passengerEntityID[0]);
        } else {
            this.wrapperPlayServerMount.setPassengerIds(passengerEntityID);
        }
    }

    public void sendPacket(Player player) {
        if (this.wrapperPlayServerAttachEntity != null) {
            this.wrapperPlayServerAttachEntity.sendPacket(player);
        } else {
            this.wrapperPlayServerMount.sendPacket(player);
        }
    }
}
