package games.sunken.anchor.util;

import com.google.common.io.BaseEncoding;
import games.sunken.anchor.Anchor;
import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.logging.Level;

public class ItemUtil {

    private Anchor plugin;

    public ItemUtil(Anchor plugin) {
        this.plugin = plugin;
    }

    public Object getTag(ItemStack item, String key) {
        if (!hasTag(item, key)) return null;

        return NBTUtil.getItemTag(item, key);
    }

    public boolean hasTag(ItemStack item, String key) {
        if (!item.hasItemMeta()) return false;

        return NBTUtil.contains(item, key);
    }

    public ItemStack setTag(ItemStack item, String key, Object value) {
        return NBTUtil.set(item, value, key);
    }

    public ItemStack removeTag(ItemStack item, String key) {
        if (!hasTag(item, key)) return item;

        return NBTUtil.set(item, null, key);
    }

    public String serializeItem(ItemStack itemStack) {
        if (itemStack == null) return null;

        ByteArrayOutputStream outputStream = null;

        try {
            Class<?> nbtTagCompoundClass = Reflection.getMcClass("NBTTagCompound");
            Constructor<?> nbtTagCompoundConstructor = nbtTagCompoundClass.getConstructor();
            Object nbtTagCompound = nbtTagCompoundConstructor.newInstance();
            Object nmsItemStack = Reflection.getCraftClass("inventory.CraftItemStack").getMethod("asNMSCopy", ItemStack.class).invoke(null, itemStack);

            Reflection.getMcClass("ItemStack").getMethod("save", nbtTagCompoundClass).invoke(nmsItemStack, nbtTagCompound);

            outputStream = new ByteArrayOutputStream();

            Reflection.getMcClass("NBTCompressedStreamTools").getMethod("a", nbtTagCompoundClass, OutputStream.class).invoke(null, nbtTagCompound, outputStream);
        } catch (SecurityException | NoSuchMethodException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return BaseEncoding.base64().encode(outputStream.toByteArray());
    }


    public ItemStack deserializeItemStack(String itemStackString) {
        if (itemStackString.equals("null")) return null;

        ByteArrayInputStream inputStream = new ByteArrayInputStream(BaseEncoding.base64().decode(itemStackString));

        Class<?> nbtTagCompoundClass = Reflection.getMcClass("NBTTagCompound");
        Class<?> nmsItemStackClass = Reflection.getMcClass("ItemStack");
        Object nbtTagCompound = null;
        ItemStack itemStack = null;

        try {
            nbtTagCompound = Reflection.getMcClass("NBTCompressedStreamTools").getMethod("a", InputStream.class).invoke(null, inputStream);
            Object craftItemStack = nmsItemStackClass.getMethod("createStack", nbtTagCompoundClass).invoke(null, nbtTagCompound);
            itemStack = (ItemStack) Reflection.getCraftClass("inventory.CraftItemStack").getMethod("asBukkitCopy", nmsItemStackClass).invoke(null, craftItemStack);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
            e.printStackTrace();
        }

        return itemStack;
    }

    public String serializeItemJSON(ItemStack itemStack) {
        if (itemStack != null) {
            // ItemStack methods to get a net.minecraft.server.ItemStack object for serialization
            Class<?> craftItemStackClazz = Reflection.getCraftClass("inventory.CraftItemStack");
            Method asNMSCopyMethod = Reflection.getMethod(craftItemStackClazz, "asNMSCopy", ItemStack.class);

            // NMS Method to serialize a net.minecraft.server.ItemStack to a valid Json string
            Class<?> nmsItemStackClazz = Reflection.getMcClass("ItemStack");
            Class<?> nbtTagCompoundClazz = Reflection.getMcClass("NBTTagCompound");
            Method saveNmsItemStackMethod = Reflection.getMethod(nmsItemStackClazz, "save", nbtTagCompoundClazz);

            Object nmsNbtTagCompoundObj; // This will just be an empty NBTTagCompound instance to invoke the saveNms method
            Object nmsItemStackObj; // This is the net.minecraft.server.ItemStack object received from the asNMSCopy method
            Object itemAsJsonObject; // This is the net.minecraft.server.ItemStack after being put through saveNmsItem method

            try {
                nmsNbtTagCompoundObj = nbtTagCompoundClazz.newInstance();
                nmsItemStackObj = Objects.requireNonNull(asNMSCopyMethod).invoke(null, itemStack);
                itemAsJsonObject = Objects.requireNonNull(saveNmsItemStackMethod).invoke(nmsItemStackObj, nmsNbtTagCompoundObj);
            } catch (Throwable t) {
                Bukkit.getLogger().log(Level.SEVERE, "Failed to serialize itemstack to nms item", t);
                return null;
            }

            // Return a string representation of the serialized object
            return itemAsJsonObject.toString();
        }

        return "";
    }

    @Deprecated
    public ItemStack deserializeItemJSON(String json) {
        Class<?> mojangsonParserClazz = Reflection.getMcClass("MojangsonParser");
        Class<?> nmsItemStackClazz = Reflection.getMcClass("ItemStack");
        Class<?> craftItemStackClazz = Reflection.getCraftClass("inventory.CraftItemStack");
        Class<?> nbtTagCompoundClazz = Reflection.getMcClass("NBTTagCompound");

        Method asBukkitCopyMethod = Reflection.getMethod(craftItemStackClazz, "asBukkitCopy", nmsItemStackClazz);
        Method parseMethod = Reflection.getMethod(mojangsonParserClazz, "parse", String.class);
        Method createStackMethod;

        if (Reflection.VERSION_STRING.contains("1_8") || Reflection.VERSION_STRING.contains("1_9") || Reflection.VERSION_STRING.contains("1_10")) {
            createStackMethod = Reflection.getMethod(nmsItemStackClazz, "createStack", nbtTagCompoundClazz);
        } else if (Reflection.VERSION_STRING.contains("1_11") || Reflection.VERSION_STRING.contains("1_12")) { // These are weird and don't really know what they want to do with their lives
            try {
                return (ItemStack) Objects.requireNonNull(asBukkitCopyMethod).invoke(null, nmsItemStackClazz.getConstructor(nbtTagCompoundClazz)
                        .newInstance(Objects.requireNonNull(parseMethod).invoke(null, json)));
            } catch (IllegalAccessException | InvocationTargetException | InstantiationException | NoSuchMethodException e) {
                Bukkit.getLogger().log(Level.SEVERE, "Failed to deserialize json string to itemstack");
                e.printStackTrace();

                return null;
            }
        } else { // Why did they revert back to the same method used from 1.8-1.10, but drop the real name in the process??? MOJANG PLS
            createStackMethod = Reflection.getMethod(nmsItemStackClazz, "a", nbtTagCompoundClazz);
        }

        try {
            Object nbtTagCompound = Objects.requireNonNull(parseMethod).invoke(null, json);
            Object nmsItemStack = Objects.requireNonNull(createStackMethod).invoke(null, nbtTagCompound);
            Object craftItemStack = Objects.requireNonNull(asBukkitCopyMethod).invoke(null, nmsItemStack);

            return (ItemStack) craftItemStack;
        } catch (IllegalAccessException | InvocationTargetException e) {
            Bukkit.getLogger().log(Level.SEVERE, "Failed to deserialize json string to itemstack");
            e.printStackTrace();

            return null;
        }
    }

}