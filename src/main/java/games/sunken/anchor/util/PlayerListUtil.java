package games.sunken.anchor.util;

import games.sunken.anchor.util.packetwrapper.WrapperPlayServerPlayerListHeaderFooter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class PlayerListUtil {

    public static void send(Player player, String header, String footer) { new WrapperPlayServerPlayerListHeaderFooter(header, footer).sendPacket(player); }

    public static void send(String header, String footer) {
        WrapperPlayServerPlayerListHeaderFooter wrapperPlayServerPlayerListHeaderFooter = new WrapperPlayServerPlayerListHeaderFooter(header, footer);

        Bukkit.getOnlinePlayers().forEach(wrapperPlayServerPlayerListHeaderFooter::sendPacket);
    }

    public static void sendHeader(Player player, String header) { send(player, header, null); }

    public static void sendHeader(String header) { send(header, null); }

    public static void sendFooter(Player player, String footer) { send(player, null, footer); }

    public static void sendFooter(String footer) { send(null, footer); }

}
