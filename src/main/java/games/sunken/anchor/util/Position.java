package games.sunken.anchor.util;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.BlockFace;
import org.bukkit.util.Vector;

import java.text.DecimalFormat;
import java.util.Objects;

public class Position {

    private static final BlockFace[] CARDINAL_AXIS = {
            BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST
    };
    private static final BlockFace[] CARDINAL_RADIAL = {
            BlockFace.NORTH, BlockFace.NORTH_EAST, BlockFace.EAST, BlockFace.SOUTH_EAST, BlockFace.SOUTH,
            BlockFace.SOUTH_WEST, BlockFace.WEST, BlockFace.NORTH_WEST
    };

    /**
     * Formatting
     */
    private static final DecimalFormat df = new DecimalFormat("#.00");
    /**
     * X Value
     */
    private double x;
    /**
     * Y Value
     */
    private double y;
    /**
     * Z Value
     */
    private double z;
    /**
     * YAW
     */
    private float yaw;
    /**
     * Pitch
     */
    private float pitch;

    public Position(Location location) {
        this.x = location.getX();
        this.y = location.getY();
        this.z = location.getZ();
        this.yaw = location.getYaw();
        this.pitch = location.getPitch();
    }

    public Position(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = 0;
        this.pitch = 0;
    }

    public Position(double x, double y, double z, float yaw, float pitch) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
    }

    public Position(String string) {
        String[] parts = string.split(",");

        this.x = Double.parseDouble(parts[0].replaceAll("[^\\d-.]", ""));
        this.y = Double.parseDouble(parts[1]);
        this.z = Double.parseDouble(parts[2]);
        this.yaw = Float.parseFloat(parts[3]);
        this.pitch = Float.parseFloat(parts[4]);
    }

    /**
     * Computes the distance between this Position and Position {@code (x1, y1, z1)}.
     *
     * @param x1 the x coordinate of other position
     * @param y1 the y coordinate of other position
     * @param z1 the z coordinate of other position
     * @return the distance between this position and position {@code (x1, y1, z1)}.
     */
    public double distance(double x1, double y1, double z1) {
        double a = getX() - x1;
        double b = getY() - y1;
        double c = getZ() - z1;
        return Math.sqrt(a * a + b * b + c * c);
    }

    /**
     * Computes the distance between this position and the specified {@code position}.
     *
     * @param position the other position
     * @return the distance between this position and the specified {@code position}.
     * @throws NullPointerException if the specified {@code position} is null
     */
    public double distance(Position position) {
        return distance(position.getX(), position.getY(), position.getZ());
    }

    /**
     * Computes the distance between this Position and Position {@code (x1, y1, z1)}.
     *
     * @param x1 the x coordinate of other position
     * @param y1 the y coordinate of other position
     * @param z1 the z coordinate of other position
     * @return the distance between this position and position {@code (x1, y1, z1)}.
     */
    public double distanceSquared(double x1, double y1, double z1) {
        double a = getX() - x1;
        double b = getY() - y1;
        double c = getZ() - z1;
        return a * a + b * b + c * c;
    }

    /**
     * Computes the distance between this position and the specified {@code position}.
     *
     * @param position the other position
     * @return the distance between this position and the specified {@code position}.
     * @throws NullPointerException if the specified {@code position} is null
     */
    public double distanceSquared(Position position) {
        return distanceSquared(position.getX(), position.getY(), position.getZ());
    }

    /**
     * Add a Position to Position
     *
     * @param position Position to add
     * @return A Postion
     */
    public Position add(Position position) {
        return new Position(this.x + position.getX(), this.y + position.getY(), this.z + position.getZ());
    }

    /**
     * Add x, y, z to Position
     *
     * @param x X amount to add
     * @param y Y amount to add
     * @param z Z amount to add
     * @return A Position
     */
    public Position add(double x, double y, double z) {
        return new Position(this.x + x, this.y + y, this.z + z);
    }

    /**
     * Subtract a Position to Position
     *
     * @param position Position to subtract
     * @return A Postion
     */
    public Position subtract(Position position) {
        return new Position(this.x - position.getX(), this.y - position.getY(), this.z - position.getZ());
    }

    /**
     * Add x, y, z to Position
     *
     * @param x X amount to subtract
     * @param y Y amount to subtract
     * @param z Z amount to subtract
     * @return A Position
     */
    public Position subtract(double x, double y, double z) {
        return new Position(this.x - x, this.y - y, this.z - z);
    }

    /**
     * @param min Minimum Position
     * @param max Maximum Position
     * @return A boolean
     */
    public boolean isBetween(Position min, Position max) {
        return this.x > min.getX() && this.x < max.getX()
                && this.y > min.getY() && this.y < max.getY()
                && this.z > min.getZ() && this.z < max.getZ();
    }

    /**
     * Returns a position which lies in the middle between this position and the
     * specified coordinates.
     *
     * @param x the X coordinate of the second endposition
     * @param y the Y coordinate of the second endposition
     * @param z the Z coordinate of the second endposition
     * @return the position in the middle
     */
    public Position midpoint(double x, double y, double z) {
        return new Position(
                x + (getX() - x) / 2.0,
                y + (getY() - y) / 2.0,
                z + (getZ() - z) / 2.0);
    }

    /**
     * Returns a position which lies in the middle between this position and the
     * specified position.
     *
     * @param position the other endposition
     * @return the position in the middle
     * @throws NullPointerException if the specified {@code position} is null
     */
    public Position midpoint(Position position) {
        return midpoint(position.getX(), position.getY(), position.getZ());
    }

    /**
     * @param world World to use
     * @return A {@link Location}
     */
    public Location toLocation(World world) {
        return new Location(world, this.x, this.y, this.z, this.yaw, this.pitch);
    }

    /**
     * @return A {@link Vector}
     */
    public Vector toVector() {
        return new Vector(this.x, this.y, this.z);
    }

    public BlockFace getCardinalDirection(boolean subDirections) {
        if (subDirections) {
            return CARDINAL_RADIAL[Math.round(getYaw() / 45f) & 0x7].getOppositeFace();
        }

        return CARDINAL_AXIS[Math.round(getYaw() / 90f) & 0x3].getOppositeFace();
    }

    /**
     * Check if the chunk is loaded in the world for this {@link Position}
     *
     * @param world World to use
     * @return A boolean
     */
    public boolean isChunkLoaded(World world) {
        return world != null && world.isChunkLoaded((int) this.x >> 4, (int) this.z >> 4);
    }

    /**
     * @return A {@link Position}
     */
    public Position asBlockPos() {
        return new Position(this.getBlockX(), this.getBlockY(), this.getBlockZ());
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public int getBlockX() {
        return (int) Math.floor(x);
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public int getBlockY() {
        return (int) Math.floor(y);
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public int getBlockZ() {
        return (int) Math.floor(z);
    }

    public float getYaw() {
        return yaw;
    }

    public void setYaw(float yaw) {
        this.yaw = yaw;
    }

    public float getPitch() {
        return pitch;
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
    }


    @Override
    public String toString() {
        return df.format(this.x) + "," + df.format(this.y) + "," + df.format(this.z) + "," + df.format(this.yaw) + "," + df.format(this.pitch);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return Double.compare(position.x, x) == 0 &&
                Double.compare(position.y, y) == 0 &&
                Double.compare(position.z, z) == 0 &&
                Float.compare(position.yaw, yaw) == 0 &&
                Float.compare(position.pitch, pitch) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z, yaw, pitch);
    }

}