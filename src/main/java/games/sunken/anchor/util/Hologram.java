package games.sunken.anchor.util;

import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import games.sunken.anchor.util.packetwrapper.WrapperPlayServerEntityDestroy;
import games.sunken.anchor.util.packetwrapper.WrapperPlayServerSpawnEntityLiving;
import games.sunken.anchor.util.packetwrapper.packetoverlay.OverlayPlayServerRideEntity;
import games.sunken.anchor.util.packetwrapper.packetoverlay.OverlayPlayServerSpawnArmorStand;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.*;

public class Hologram {

    private static final double ABS = 0.25D; // 0.23
    private final List<WrapperPlayServerEntityDestroy> destroyCache;
    private final List<OverlayPlayServerSpawnArmorStand> spawnCache;
    private final List<UUID> players;
    private final List<String> lines;
    private final Location loc;

    private int vehicleID = -1;

    public Hologram(Location loc, String... lines) {
        this(loc, Arrays.asList(lines));
    }

    public Hologram(Location loc, List<String> lines) {
        this.lines = lines;
        this.loc = loc;
        this.players = new ArrayList<>();
        this.spawnCache = new ArrayList<>();
        this.destroyCache = new ArrayList<>();

        // Init
        Location displayLoc = loc.clone();
        for (int i = 0; i < lines.size(); i++) {
            int entityID = new Random().nextInt(Integer.MAX_VALUE);
            WrappedDataWatcher wrappedDataWatcher = new WrappedDataWatcher();

            if (Reflection.VERSION_NUMBER > 189) {
                wrappedDataWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(0, WrappedDataWatcher.Registry.get(Byte.class)), (byte) 0x20);
                wrappedDataWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(2, WrappedDataWatcher.Registry.getChatComponentSerializer(true)), Optional.of(WrappedChatComponent.fromText(lines.get(i)).getHandle()));
                wrappedDataWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(3, WrappedDataWatcher.Registry.get(Boolean.class)), true);
                wrappedDataWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(5, WrappedDataWatcher.Registry.get(Boolean.class)), true);
                wrappedDataWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(11, WrappedDataWatcher.Registry.get(Byte.class)), (byte) 0xFF);
            } else {
                wrappedDataWatcher.setObject(0, (byte) 0x20);
                wrappedDataWatcher.setObject(2, lines.get(i));
                wrappedDataWatcher.setObject(3, (byte) 1);
                wrappedDataWatcher.setObject(10, (byte) 0xFF);
            }

            this.spawnCache.add(new OverlayPlayServerSpawnArmorStand(UUID.randomUUID(), entityID, displayLoc.getX(), displayLoc.getY(), displayLoc.getZ(), displayLoc.getYaw(), displayLoc.getPitch(), displayLoc.getPitch(), 0.0, 0.0, 0.0, wrappedDataWatcher));
            this.destroyCache.add(new WrapperPlayServerEntityDestroy(new int[]{entityID}));
            displayLoc = displayLoc.add(0, ABS * (-1), 0);
        }
    }

    public boolean display(Player p) {
        for (OverlayPlayServerSpawnArmorStand overlayPlayServerSpawnArmorStand : this.spawnCache) {
            overlayPlayServerSpawnArmorStand.sendPacket(p);
        }

        this.players.add(p.getUniqueId());

        return true;
    }

    public boolean hide(Player p) {
        if (this.players.contains(p.getUniqueId())) {
            for (WrapperPlayServerEntityDestroy wrapperPlayServerEntityDestroy : this.destroyCache) {
                wrapperPlayServerEntityDestroy.sendPacket(p);
            }

            this.players.remove(p.getUniqueId());

            return true;
        }

        return false;
    }

    public void mountOn(int vehicleID) {
        int entityToMount = vehicleID;

        for (OverlayPlayServerSpawnArmorStand overlayPlayServerSpawnArmorStand : this.spawnCache) {
            WrapperPlayServerSpawnEntityLiving silverfish1 = spawnSilverfish();
            WrapperPlayServerSpawnEntityLiving silverfish2 = spawnSilverfish();
            WrapperPlayServerSpawnEntityLiving slime = spawnSlime();

            this.destroyCache.add(new WrapperPlayServerEntityDestroy(new int[]{silverfish1.getEntityID()}));
            this.destroyCache.add(new WrapperPlayServerEntityDestroy(new int[]{silverfish2.getEntityID()}));
            this.destroyCache.add(new WrapperPlayServerEntityDestroy(new int[]{slime.getEntityID()}));

            OverlayPlayServerRideEntity silverfish1Ride = new OverlayPlayServerRideEntity(new int[]{silverfish1.getEntityID()}, entityToMount);
            OverlayPlayServerRideEntity silverfish2Ride = new OverlayPlayServerRideEntity(new int[]{silverfish2.getEntityID()}, silverfish1.getEntityID());
            OverlayPlayServerRideEntity slimeRide = new OverlayPlayServerRideEntity(new int[]{slime.getEntityID()}, silverfish2.getEntityID());
            OverlayPlayServerRideEntity armorStandRide = new OverlayPlayServerRideEntity(new int[]{overlayPlayServerSpawnArmorStand.getEntityID()}, slime.getEntityID());

            for (UUID players : this.players) {
                Player player = Bukkit.getPlayer(players);

                if (player != null) {
                    silverfish1.sendPacket(player);
                    silverfish2.sendPacket(player);
                    slime.sendPacket(player);

                    silverfish1Ride.sendPacket(player);
                    silverfish2Ride.sendPacket(player);
                    slimeRide.sendPacket(player);
                    armorStandRide.sendPacket(player);
                }
            }

            entityToMount = overlayPlayServerSpawnArmorStand.getEntityID();
        }

        this.vehicleID = vehicleID;
    }

    private WrapperPlayServerSpawnEntityLiving spawnSlime() {
        WrappedDataWatcher wrappedDataWatcher = new WrappedDataWatcher();

        if (Reflection.VERSION_NUMBER > 189) {
            wrappedDataWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(0, WrappedDataWatcher.Registry.get(Byte.class)), (byte) 0x20);
            wrappedDataWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(5, WrappedDataWatcher.Registry.get(Boolean.class)), true);
            wrappedDataWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(11, WrappedDataWatcher.Registry.get(Byte.class)), (byte) 0x01);
            wrappedDataWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(16, WrappedDataWatcher.Registry.get(Byte.class)), (byte) -1);
        } else {
            wrappedDataWatcher.setObject(0, (byte) 0x20);
            wrappedDataWatcher.setObject(15, (byte) 1);
            wrappedDataWatcher.setObject(16, (byte) -1);
        }

        return new WrapperPlayServerSpawnEntityLiving(UUID.randomUUID(), new Random().nextInt(Integer.MAX_VALUE), EntityType.SLIME, this.loc.getX(), this.loc.getY(), this.loc.getZ(), this.loc.getYaw(), this.loc.getPitch(), this.loc.getPitch(), 0.0, 0.0, 0.0, wrappedDataWatcher);
    }

    private WrapperPlayServerSpawnEntityLiving spawnSilverfish() {
        WrappedDataWatcher wrappedDataWatcher = new WrappedDataWatcher();

        if (Reflection.VERSION_NUMBER > 189) {
            wrappedDataWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(0, WrappedDataWatcher.Registry.get(Byte.class)), (byte) 0x20);
            wrappedDataWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(5, WrappedDataWatcher.Registry.get(Boolean.class)), true);
            wrappedDataWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(11, WrappedDataWatcher.Registry.get(Byte.class)), (byte) 0x01);
        } else {
            wrappedDataWatcher.setObject(0, (byte) 0x20);
            wrappedDataWatcher.setObject(15, (byte) 1);
        }

        return new WrapperPlayServerSpawnEntityLiving(UUID.randomUUID(), new Random().nextInt(Integer.MAX_VALUE), EntityType.SILVERFISH, this.loc.getX(), this.loc.getY(), this.loc.getZ(), this.loc.getYaw(), this.loc.getPitch(), this.loc.getPitch(), 0.0, 0.0, 0.0, wrappedDataWatcher);
    }

}
