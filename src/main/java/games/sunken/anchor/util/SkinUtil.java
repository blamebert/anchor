package games.sunken.anchor.util;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.minecraft.MinecraftSessionService;
import com.mojang.authlib.properties.Property;
import com.mojang.authlib.yggdrasil.YggdrasilMinecraftSessionService;
import games.sunken.anchor.Anchor;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.UUID;

public class SkinUtil {
    private Anchor anchor;
    private YggdrasilMinecraftSessionService yggdrasilMinecraftSessionService;

    private HashMap<UUID, GameProfile> cachedGameProfiles;

    public SkinUtil(Anchor anchor) {
        this.anchor = anchor;

        try {
            Object craftServer = Reflection.getCraftClass("CraftServer").cast(Bukkit.getServer());
            Object minecraftServer = craftServer.getClass().getMethod("getServer").invoke(craftServer);

            for(Method method : craftServer.getClass().getMethod("getServer").invoke(craftServer).getClass().getMethods()) {
                if (method.getReturnType() == MinecraftSessionService.class) {
                    this.yggdrasilMinecraftSessionService = (YggdrasilMinecraftSessionService) method.invoke(minecraftServer);
                }
            }
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }

        this.cachedGameProfiles = new HashMap<>();
    }

    public GameProfile getGameProfile(UUID uuid, String name) {
        if (!this.cachedGameProfiles.containsKey(uuid)) {
            this.cachedGameProfiles.put(uuid, this.yggdrasilMinecraftSessionService.fillProfileProperties(new GameProfile(uuid, name), true));

            new BukkitRunnable() {
                @Override
                public void run() {
                    cachedGameProfiles.remove(uuid);
                }
            }.runTaskLater(this.anchor, 1200);
        }

        return this.cachedGameProfiles.get(uuid);
    }

    public GameProfile makeProfile(String name, UUID skinId, GameProfile gameProfile) {
        GameProfile profile = new GameProfile(UUID.randomUUID(), name);

        if (skinId != null && gameProfile.getProperties().get("textures") != null && gameProfile.getProperties().get("textures").size() > 0) {
            Property textures = gameProfile.getProperties().get("textures").iterator().next();
            profile.getProperties().put("textures", textures);
        }

        return profile;
    }
}
