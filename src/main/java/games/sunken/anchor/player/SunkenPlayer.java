package games.sunken.anchor.player;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.chat.Prompt;
import games.sunken.anchor.chat.channel.ChatChannel;
import games.sunken.anchor.event.PlayerPreSetPermissionsEvent;
import games.sunken.anchor.game.map.MapCreator;
import games.sunken.anchor.perms.Rank;
import games.sunken.anchor.perms.RankManager;
import games.sunken.anchor.scoreboard.DynamicScoreboard;
import games.sunken.anchor.util.PlayerListUtil;
import games.sunken.anchor.util.TitleUtil;
import games.sunken.anchor.util.XSound;
import games.sunken.anchor.util.packetwrapper.WrapperPlayServerGameStateChange;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.mongodb.morphia.annotations.*;

import javax.annotation.Nullable;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.*;
import java.util.stream.Collectors;

@Entity("players")
public class SunkenPlayer {

    @Id
    private String id;

    @Indexed(options = @IndexOptions(unique = true))
    private String uuid;

    @Indexed
    private String username;

    @Transient
    private Prompt prompt;

    private String focus;

    @Embedded
    private Set<ChatChannel> hiding;

    private String prefix;
    private String suffix;
    private ChatColor chatColor;

    @Reference
    private Rank rank;

    @Transient
    private PermissionAttachment permissionAttachment;

    private Set<String> permissions;

    @Property("persistent_strings")
    private Map<String, String> persistentStrings;

    @Property("persistent_data")
    private Map<String, Integer> persistentData;

    @Transient
    private DynamicScoreboard scoreboard;

    @Transient
    private MapCreator mapCreator;

    @Transient
    private boolean frozen;

    @Transient
    private BukkitTask frozenRunnable;

    public SunkenPlayer() {
    }

    public SunkenPlayer(UUID uuid, String username) {
        this.id = uuid.toString();
        this.uuid = uuid.toString();
        this.username = username;
        this.prompt = null;
        this.focus = Anchor.getPlugin(Anchor.class).getChannelManager().getDefaultChannel().getName();
        this.hiding = new HashSet<>();
        this.rank = Anchor.getPlugin(Anchor.class).getRankManager().getDefaultRank();
        this.prefix = rank.getPrefix();
        this.suffix = rank.getSuffix();
        this.chatColor = rank.getChatColor();
        this.permissions = new HashSet<>();
        this.persistentStrings = new HashMap<>();
        this.persistentData = new HashMap<>();
        this.scoreboard = null;
        this.mapCreator = null;
    }

    // region Player

    public UUID getUUID() {
        return UUID.fromString(uuid);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Player getBukkitPlayer() {
        return Bukkit.getPlayer(getUUID());
    }

    // endregion

    // region Chat

    public void sendMessage(MsgType msgType, @Nullable XSound xSound, TextComponent message, Object... args) {
        if (xSound != null && xSound.parseSound() != null) playSound(xSound, 1f, 1f, false);

        message.setColor(msgType.getPrimaryColor());
        message.setText(msgType.format(message.getColor() + message.toLegacyText(), args));

        getBukkitPlayer().spigot().sendMessage(message);
    }

    public Prompt getPrompt() {
        return prompt;
    }

    public void setPrompt(Prompt prompt) {
        this.prompt = prompt;

        if (prompt != null) {
            prompt.sendMessage(getBukkitPlayer());
        }
    }

    public ChatChannel getChatFocus() {
        if (focus == null || Anchor.getPlugin(Anchor.class).getChannelManager().getByName(focus) == null)
            Anchor.getPlugin(Anchor.class).getChannelManager().getDefaultChannel();

        return Anchor.getPlugin(Anchor.class).getChannelManager().getByName(focus);
    }

    public void setChatFocus(ChatChannel focus) {
        this.setChatFocus(focus, false);
    }

    public void setChatFocus(ChatChannel focus, boolean silent) {
        this.focus = focus.getName();

        if (!silent) {
            sendMessage(MsgType.SUCCESS, null, new TextComponent("Primary chat focus set to <p>!"), focus.getChatColor() + focus.getName());
        }
    }

    public Set<ChatChannel> getHiddenChannels() {
        return hiding == null ? new HashSet<>() : hiding;
    }

    public boolean isHiding(ChatChannel chatChannel) {
        return getHiddenChannels().contains(chatChannel);
    }

    public void toggleHiding(ChatChannel chatChannel) {
        if (isHiding(chatChannel)) {
            getHiddenChannels().remove(chatChannel);
        } else {
            getHiddenChannels().add(chatChannel);
        }

        hiding = getHiddenChannels();

        sendMessage(MsgType.SUCCESS, null, new TextComponent("Visibility for <p> toggled <p>!"), chatChannel.getName(), (isHiding(chatChannel) ? ChatColor.RED + "" + ChatColor.BOLD + "OFF" :
                ChatColor.GREEN + "" + ChatColor.BOLD + "ON"));
    }

    // endregion

    // region Ranks

    public String getPrefix() {
        if (this.prefix == null) {
            if (this.rank == null) {
                if (getBukkitPlayer() != null && getBukkitPlayer().isOnline()) return (getBukkitPlayer().isOp() ? ChatColor.RED : ChatColor.WHITE) + "";

                return ChatColor.GRAY + "";
            } else {
                return ChatColor.translateAlternateColorCodes('&', this.rank.getPrefix());
            }
        } else {
            return ChatColor.translateAlternateColorCodes('&', this.prefix);
        }
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        if (this.suffix == null) {
            if (this.rank == null) {
                return "";
            } else {
                return ChatColor.translateAlternateColorCodes('&', this.rank.getSuffix());
            }
        } else {
            return ChatColor.translateAlternateColorCodes('&', this.suffix);
        }
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public ChatColor getChatColor() {
        if (this.chatColor == null) {
            if (this.rank == null) {
                return ChatColor.GRAY;
            } else {
                return this.rank.getChatColor();
            }
        } else {
            return this.chatColor;
        }
    }

    public void setChatColor(ChatColor chatColor) {
        this.chatColor = chatColor;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public Set<String> getPlayerPermissions() {
        if (permissions == null) this.permissions = new HashSet<>();

        return permissions;
    }

    public void setPlayerPermissions(Set<String> permissions) {
        this.permissions = permissions;
    }

    public void recalculatePermissions(boolean silent) {
        Player player = getBukkitPlayer();

        if (player == null || !player.isOnline()) return;

        if (this.permissionAttachment != null) {
            player.removeAttachment(permissionAttachment);

            this.permissionAttachment = null;
        }

        this.permissionAttachment = player.addAttachment(Anchor.getPlugin(Anchor.class));

        Map<String, Boolean> proposedPermissions = new HashMap<>();
        RankManager rankManager = Anchor.getPlugin(Anchor.class).getRankManager();

        if (this.rank == null && rankManager.getDefaultRank() != null) setRank(rankManager.getDefaultRank());

        if (rank != null) { // Rank permissions, lowest priority -> highest priority
            Rank current = rankManager.getLowestRank();

            List<Rank> sortedRanks = rankManager.getRanks().stream()
                    .sorted(Comparator.comparing(Rank::getPriority))
                    .collect(Collectors.toList());

            if (!rank.equals(current)) {
                for (Rank r : sortedRanks) {
                    if (rank.getInherits().contains(r)) {
                        if (!r.getPermissions().isEmpty()) {
                            for (String perm : r.getPermissions()) {
                                boolean hasPerm = true;

                                if (perm.substring(0, 1).equals("-")) {
                                    hasPerm = false;
                                    perm = perm.substring(1);
                                }

                                proposedPermissions.put(perm, hasPerm);
                            }
                        }
                    }
                }
            }

            for (String perm : rank.getPermissions()) {
                boolean hasPerm = true;

                if (perm.substring(0, 1).equals("-")) {
                    hasPerm = false;
                    perm = perm.substring(1);
                }

                proposedPermissions.put(perm, hasPerm);
            }
        }

        for (String perm : getPlayerPermissions()) { // Player permissions. Higher priority than ranks
            boolean hasPerm = true;

            if (perm.substring(0, 1).equals("-")) {
                hasPerm = false;
                perm = perm.substring(1);
            }

            proposedPermissions.put(perm, hasPerm);
        }

        PlayerPreSetPermissionsEvent event = new PlayerPreSetPermissionsEvent(this, proposedPermissions);
        Bukkit.getPluginManager().callEvent(event);

        boolean hasWildcard = false;
        boolean hasChildPerms;

        do {
            hasChildPerms = false;

            for (Map.Entry<String, Boolean> perm : new ArrayList<>(event.getProposedPermissions().entrySet())) {
                if (perm.getKey().equals("*")) {
                    hasWildcard = true;
                } else {
                    Permission bukkitPerm;

                    if ((bukkitPerm = Bukkit.getPluginManager().getPermission(perm.getKey())) == null) {
                        bukkitPerm = new Permission(perm.getKey());
                        Bukkit.getPluginManager().addPermission(bukkitPerm);
                    }

                    if (perm.getValue()) {
                        for (Map.Entry<String, Boolean> childPerm : bukkitPerm.getChildren().entrySet()) {
                            if ((!event.getProposedPermissions().containsKey(childPerm.getKey())) && (!event.getProposedPermissions().containsKey("-" + childPerm.getKey()))) {
                                proposedPermissions.put(childPerm.getKey(), childPerm.getValue());
                                hasChildPerms = true;
                            }
                        }
                    }

                    this.permissionAttachment.getPermissible().addAttachment(Anchor.getPlugin(Anchor.class), perm.getKey(), perm.getValue());
                }
            }
        } while (hasChildPerms);

        if (hasWildcard) {
            for (Permission permission : Bukkit.getPluginManager().getPermissions()) {
                if (!this.permissionAttachment.getPermissible().hasPermission(permission)) {
                    this.permissionAttachment.getPermissible().addAttachment(Anchor.getPlugin(Anchor.class), permission.getName(), true);
                }
            }
        }

        this.permissionAttachment.getPermissible().recalculatePermissions();
        player.recalculatePermissions(); // Now you can call Player.hasPermission() regularly

        if (!silent)
            sendMessage(MsgType.WARNING, XSound.BLOCK_NOTE_BLOCK_PLING, new TextComponent("Your rank and/or permissions have been updated!"));
    }

    // endregion

    // region Misc. Storage

    public Map<String, String> getPersistentStrings() {
        if (persistentStrings == null) this.persistentStrings = new HashMap<>();

        return persistentStrings;
    }

    public Map<String, Integer> getPersistentData() {
        if (persistentData == null) this.persistentData = new HashMap<>();

        return persistentData;
    }

    // endregion

    // region Utils

    public DynamicScoreboard getScoreboard() {
        return scoreboard;
    }

    public void setScoreboard(DynamicScoreboard scoreboard) {
        if (this.scoreboard != null) this.scoreboard.destroy();

        this.scoreboard = scoreboard;

        if (this.scoreboard != null) this.scoreboard.create();
    }

    public MapCreator getMapCreator() {
        return mapCreator;
    }

    public void setMapCreator(@Nullable MapCreator mapCreator) {
        this.mapCreator = mapCreator;
    }

    public void sendToServer(String targetServer) {
        if (getBukkitPlayer().isOnline()) {
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            DataOutputStream out = new DataOutputStream(b);

            try {
                out.writeUTF("Connect");
                out.writeUTF(targetServer);
            } catch (Exception e) {
                e.printStackTrace();
            }

            getBukkitPlayer().sendPluginMessage(Anchor.getPlugin(Anchor.class), "BungeeCord", b.toByteArray());
        }
    }

    public void playSound(XSound xSound, float volume, float pitch, boolean global) {
        Objects.requireNonNull(xSound.parseSound());

        if (global) {
            getBukkitPlayer().getWorld().playSound(getBukkitPlayer().getLocation(), xSound.parseSound(), volume, pitch);
        } else {
            getBukkitPlayer().playSound(getBukkitPlayer().getLocation(), xSound.parseSound(), volume, pitch);
        }
    }

    public void giveItemSafely(ItemStack item, boolean silent) {
        if (getBukkitPlayer().getInventory().firstEmpty() == -1) {
            if (!silent) {
                sendMessage(MsgType.DANGER, XSound.ENTITY_CHICKEN_EGG, new TextComponent("Your inventory is full! Your item has been dropped on the floor."));
            }

            getBukkitPlayer().getWorld().dropItem(getBukkitPlayer().getLocation(), item);
        } else {
            getBukkitPlayer().getInventory().addItem(item);
            getBukkitPlayer().updateInventory();
        }
    }

    public void depleteItem(ItemStack material, int toRemove) {
        int removed = toRemove;

        for (int i = 0; i < getBukkitPlayer().getInventory().getSize(); i++) {
            ItemStack item = getBukkitPlayer().getInventory().getItem(i);

            if (item != null) {
                if (item.equals(material)) {
                    if (removed >= item.getAmount()) {
                        removed -= item.getAmount();
                        getBukkitPlayer().getInventory().clear(i);
                    } else if (removed > 0) {
                        item.setAmount(item.getAmount() - removed);
                        removed = 0;
                    } else {
                        break;
                    }
                }
            }
        }

        getBukkitPlayer().updateInventory();
    }

    public void restoreSpeeds() {
        getBukkitPlayer().setWalkSpeed(0.2f);
        getBukkitPlayer().setFlySpeed(0.1f);
    }

    public void setFrozen(boolean frozen) {
        final Player player = getBukkitPlayer();

        this.frozen = frozen;

        if (frozen) {
            this.frozenRunnable = new BukkitRunnable() {
                WrapperPlayServerGameStateChange wrapperPlayServerGameStateChange = new WrapperPlayServerGameStateChange(3, 3);

                public void run() {
                    if (player != null) {
                        player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 192, true, false));
                        player.setWalkSpeed(0f);
                        player.setFlySpeed(0f);
                        player.setAllowFlight(true);
                        player.setFlying(true);

                        wrapperPlayServerGameStateChange.sendPacket(player);
                    } else {
                        this.cancel();
                    }
                }
            }.runTaskTimer(Anchor.getPlugin(Anchor.class), 0, 1);
        } else {
            if (frozenRunnable != null) {
                this.frozenRunnable.cancel();
                this.frozenRunnable = null;
            }

            restoreSpeeds();

            player.removePotionEffect(PotionEffectType.JUMP);
            player.setAllowFlight(false);
            player.setFlying(false);

            new WrapperPlayServerGameStateChange(3, player.getGameMode().getValue()).sendPacket(player);
        }
    }

    public void sendBars(String title, String subtitle, String actionBar, int fadeInTime, int showTime, int fadeOutTime) {
        TitleUtil.send(getBukkitPlayer(), title, subtitle, actionBar, fadeInTime, showTime, fadeOutTime);
    }

    public void sendBars(String title, String subtitle, String actionBar) {
        sendBars(title, subtitle, actionBar, 0, 60, 0);
    }

    public void sendTitle(String title, int fadeInTime, int showTime, int fadeOutTime) {
        sendBars(title, null, null, fadeInTime, showTime, fadeOutTime);
    }

    public void sendTitle(String title) {
        sendBars(title, null, null, 0, 60, 0);
    }

    public void sendSubtitle(String subtitle, int fadeInTime, int showTime, int fadeOutTime) {
        sendBars(null, subtitle, null, fadeInTime, showTime, fadeOutTime);
    }

    public void sendSubtitle(String subtitle) {
        sendBars(null, subtitle, null);
    }

    public void sendActionBar(String actionBar, int fadeInTime, int showTime, int fadeOutTime) {
        sendBars(null, null, actionBar, fadeInTime, showTime, fadeOutTime);
    }

    public void sendActionBar(String actionBar) {
        sendBars(null, null, actionBar);
    }

    public void sendPlayerListHeaderAndFooter(String header, String footer) {
        PlayerListUtil.send(getBukkitPlayer(), header, footer);
    }

    public void sendPlayerListHeader(String header) {
        PlayerListUtil.sendHeader(getBukkitPlayer(), header);
    }

    public void sendPlayerListFooter(String footer) {
        PlayerListUtil.sendFooter(getBukkitPlayer(), footer);
    }

    // endregion

}