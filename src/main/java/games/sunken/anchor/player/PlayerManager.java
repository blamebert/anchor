package games.sunken.anchor.player;

import com.google.common.collect.Sets;
import games.sunken.anchor.Anchor;
import games.sunken.anchor.database.Mongo;
import org.bukkit.Bukkit;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class PlayerManager {

    private Map<UUID, SunkenPlayer> playerMap;
    private Map<String, SunkenPlayer> offlinePlayerCache;

    public PlayerManager() {
        this.playerMap = new HashMap<>();
        this.offlinePlayerCache = new HashMap<>();
    }

    public boolean playerExists(UUID uuid) {
        return playerMap.containsKey(uuid);
    }

    public void addPlayer(SunkenPlayer player) {
        playerMap.put(player.getUUID(), player);
    }

    public SunkenPlayer getPlayer(UUID uuid) {
        return playerMap.get(uuid);
    }

    public SunkenPlayer getPlayer(String name) {
        return offlinePlayerCache.get(name);
    }

    public SunkenPlayer removePlayer(UUID uuid) {
        return playerMap.remove(uuid);
    }

    public Set<SunkenPlayer> getOnlinePlayers() {
        return Sets.newHashSet(playerMap.values());
    }

    public Map<String, SunkenPlayer> getOfflinePlayerCache() {
        return offlinePlayerCache;
    }

    public void cachePlayer(SunkenPlayer sunkenPlayer, int seconds) {
        if (sunkenPlayer.getBukkitPlayer() == null) {
            offlinePlayerCache.put(sunkenPlayer.getUsername(), sunkenPlayer);

            Bukkit.getScheduler().runTaskLater(Anchor.getPlugin(Anchor.class), () -> {
                Bukkit.getScheduler().runTaskAsynchronously(Anchor.getPlugin(Anchor.class), () -> Mongo.getPlayerDAO().save(sunkenPlayer));

                offlinePlayerCache.remove(sunkenPlayer.getUsername());
            }, seconds * 20);
        }
    }

}