package games.sunken.anchor.player;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class SunkenPlayerDAO extends BasicDAO<SunkenPlayer, String> {

    public SunkenPlayerDAO(Class<SunkenPlayer> entityClass, Datastore datastore) {
        super(entityClass, datastore);
    }

}