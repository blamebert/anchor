package games.sunken.anchor.gui;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;

public class GUIIcon {

    private ItemStack itemStack;
    private Map<Set<ClickType>, BiFunction<Player, Integer, String>> clickFunctions;

    public GUIIcon(ItemStack itemStack, Map<Set<ClickType>, BiFunction<Player, Integer, String>> clickFunctions) {
        this.itemStack = itemStack;
        this.clickFunctions = clickFunctions;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public Map<Set<ClickType>, BiFunction<Player, Integer, String>> getClickFunctions() {
        return clickFunctions;
    }

    public BiFunction<Player, Integer, String> getClickFunction(ClickType action) {
        for (Set<ClickType> actions : getClickFunctions().keySet()) {
            if (actions.contains(action)) return getClickFunctions().get(actions);
        }

        return null;
    }

}