package games.sunken.anchor.gui;

import games.sunken.anchor.util.XMaterial;
import org.bukkit.ChatColor;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public abstract class PagedGUI extends GUI {

    private int page;
    private int headerRows;
    private boolean border;
    private final int internalSize;

    public PagedGUI(String title, boolean border) {
        super(title, 54);
        this.border = border;
        this.internalSize = getInvSizeForCount(54 - 9);
    }

    public abstract List<GUIIcon> getIcons();

    public int getPage() {
        return page;
    }

    public abstract void onPlayerClickIcon(InventoryClickEvent event);

    public abstract void populateSpecial();

    public void setHeaderRows(int headerRows) {
        this.headerRows = headerRows;
    }

    public int getInternalSize() {
        return internalSize;
    }

    @Override
    protected final void onPlayerInventoryClick(InventoryClickEvent event) {
        this.onPlayerClickIcon(event);
    }

    @Override
    protected final void populate() {
        List<GUIIcon> items = getIcons();

        if (items == null) {
            items = new ArrayList<>();
        }

        if (border) {
            for (int i = 0; i < inventory.getSize(); i++) {
                if (i < 9 || i > internalSize || (i + 1) % 9 == 0 || i % 9 == 0) {
                    inventory.setItem(i, SPACER);
                }
            }
        }

        int pageSize = 28;
        int pages = (items.size() / pageSize);

        if (items.size() % pageSize > 0) pages++;

        if (page > pages) {
            this.page--;
            this.repopulate();
            return;
        }

        int slot = 0;

        for (int i = 0; i < items.size();) {
            if (inventory.getItem(slot) == null || inventory.getItem(slot).getType() == XMaterial.AIR.parseMaterial()) {
                setSlot(slot, items.get(i));
                i++;
            }

            slot++;
        }

        if (page > 0) {
            ItemStack arrow = XMaterial.ARROW.parseItem();
            ItemMeta arrowMeta = arrow.getItemMeta();
            arrowMeta.setDisplayName(ChatColor.GRAY + "<- Previous Page");
            arrow.setItemMeta(arrowMeta);

            setSlot(internalSize + 3, new IconBuilder(arrow)
                    .onAnyClick((player, itemSlot) -> {
                        if (page > 0) {
                            page--;
                            this.repopulate();

                            return null;
                        }

                        return "There are no more pages left to view!";
                    })
                    .build()
            );
        }

        if (page + 1 < pages) {
            ItemStack arrow = XMaterial.ARROW.parseItem();
            ItemMeta arrowMeta = arrow.getItemMeta();
            arrowMeta.setDisplayName(ChatColor.GRAY + "Next Page ->");
            arrow.setItemMeta(arrowMeta);

            int finalPages = pages;
            setSlot(internalSize + 5, new IconBuilder(arrow)
                    .onAnyClick((player, itemSlot) -> {
                        if (page + 1 < finalPages) {
                            page++;
                            this.repopulate();

                            return null;
                        }

                        return "There are no more pages left to view!";
                    })
                    .build()
            );
        }

        ItemStack pageNumber = XMaterial.MAP.parseItem();
        ItemMeta pageNumberMeta = pageNumber.getItemMeta();
        pageNumberMeta.setDisplayName(ChatColor.GRAY + "Page " + (page + 1) + "/" + pages);
        pageNumber.setItemMeta(pageNumberMeta);
        this.inventory.setItem(internalSize + 4, pageNumber);

        this.populateSpecial();
    }

}