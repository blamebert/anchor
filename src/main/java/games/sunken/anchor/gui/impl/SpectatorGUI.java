package games.sunken.anchor.gui.impl;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.game.Game;
import games.sunken.anchor.gui.GUIIcon;
import games.sunken.anchor.gui.IconBuilder;
import games.sunken.anchor.gui.PagedGUI;
import games.sunken.anchor.items.ItemBuilder;
import games.sunken.anchor.player.SunkenPlayer;
import games.sunken.anchor.util.XMaterial;
import games.sunken.anchor.util.XSound;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class SpectatorGUI extends PagedGUI {

    private Game game;
    private Anchor plugin;

    public SpectatorGUI(Game game) {
        super("Spectate", true);

        this.game = game;
        this.plugin = Anchor.getPlugin(Anchor.class);

        setUpdateTicks(20);
    }

    @Override
    public List<GUIIcon> getIcons() {
        List<GUIIcon> icons = new ArrayList<>();

        game.getPlayableTeams().forEach(team -> team.getMembers().forEach(uuid -> {
            Player target = Bukkit.getPlayer(uuid);

            if (target != null) {
                ItemStack item = new ItemBuilder(XMaterial.PLAYER_HEAD)
                        .withDisplayName(team.getColor().getChatColor() + target.getDisplayName())
                        .withLore(
                                " ",
                                ChatColor.WHITE + "Team: " + team.getColor().getChatColor() + team.getName(),
                                " ",
                                ChatColor.WHITE + "Left-Click " + ChatColor.GRAY + "to spectate.",
                                ChatColor.WHITE + "Right-Click " + ChatColor.GRAY + "to teleport."
                        )
                        .withNBTTag("player", uuid.toString())
                        .withSkullOwner(target.getDisplayName())
                        .build();

                icons.add(new IconBuilder(item)
                        .onLeftClick((player, slot) -> {
                            if (target.isOnline()) {
                                SunkenPlayer sunkenPlayer = plugin.getPlayerManager().getPlayer(player.getUniqueId());

                                player.setGameMode(GameMode.SPECTATOR);
                                player.teleport(target);
                                player.setSpectatorTarget(target);
                                sunkenPlayer.sendBars(ChatColor.DARK_AQUA + "Spectating: " + ChatColor.WHITE + target.getDisplayName(),
                                        ChatColor.AQUA + "Press SNEAK to exit spectator mode.", null, 0, 40, 20);
                                sunkenPlayer.sendMessage(MsgType.SUCCESS, XSound.BLOCK_WOODEN_PRESSURE_PLATE_CLICK_ON,
                                        new TextComponent("Now spectating <p>. Press SNEAK to exit spectator mode."), target.getDisplayName());

                                close();

                                return null;
                            }

                            return "Player not found!";
                        }, true)

                        .onRightClick((player, slot) -> {
                            if (target.isOnline()) {
                                player.teleport(target);
                                plugin.getPlayerManager().getPlayer(player.getUniqueId())
                                        .sendMessage(MsgType.SUCCESS, XSound.BLOCK_WOODEN_PRESSURE_PLATE_CLICK_ON, new TextComponent("Teleported to <p>!"), target.getDisplayName());

                                close();

                                return null;
                            }

                            return "Player not found!";
                        }, true)

                        .build()
                );
            }
        }));

        return icons;
    }

    @Override
    public void onPlayerClickIcon(InventoryClickEvent event) {

    }

    @Override
    public void populateSpecial() {
        inventory.setItem(4, new ItemBuilder(XMaterial.PLAYER_HEAD).withDisplayName(ChatColor.AQUA + "Spectate a player...").build());
    }

    @Override
    protected void onGUIInventoryClick(InventoryClickEvent event) {

    }

}