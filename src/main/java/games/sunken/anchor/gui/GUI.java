package games.sunken.anchor.gui;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.util.XMaterial;
import games.sunken.anchor.util.XSound;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.ChatPaginator;

import javax.annotation.Nullable;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

public abstract class GUI {

    protected static final ItemStack SPACER = XMaterial.BLUE_STAINED_GLASS_PANE.parseItem();

    static {
        ItemMeta spacerMeta = SPACER.getItemMeta();
        spacerMeta.setDisplayName(" ");
        SPACER.setItemMeta(spacerMeta);
    }

    protected InventoryProxy inventory;
    protected Player player = null;
    protected boolean populated = false;
    private Inventory bukkitInventory;
    private boolean invCheckOverride = false;
    private boolean allowDrag = false;
    private boolean allowShiftClicking = false;
    private BukkitRunnable updaterTask;

    private Map<Integer, ErrorIcon> errorIconMap = new HashMap<>();
    private Map<Integer, GUIIcon> guiIconMap = new HashMap<>();

    public GUI(String title, int size) {
        if (title.length() > 32) {
            title = title.substring(0, 32);
        }

        this.bukkitInventory = Bukkit.createInventory(null, getInvSizeForCount(size), title);
        this.inventory = new InventoryProxy(bukkitInventory, Bukkit.createInventory(null, getInvSizeForCount(size), title));

        Bukkit.getPluginManager().registerEvents(new GUIEvents(this), Anchor.getPlugin(Anchor.class));
    }

    public GUI(String title, InventoryType type) {
        this.bukkitInventory = Bukkit.createInventory(null, type, title);
        this.inventory = new InventoryProxy(bukkitInventory, Bukkit.createInventory(null, type, title));

        Bukkit.getPluginManager().registerEvents(new GUIEvents(this), Anchor.getPlugin(Anchor.class));
    }

    public GUI(Inventory bukkitInventory) {
        this.bukkitInventory = bukkitInventory;
        this.inventory = new InventoryProxy(bukkitInventory, bukkitInventory);

        Bukkit.getPluginManager().registerEvents(new GUIEvents(this), Anchor.getPlugin(Anchor.class));
    }

    public final InventoryView open(Player p) {
        this.player = p;

        try {
            if (!this.populated) {
                this.populate();
                this.inventory.apply();
                this.populated = true;
            }

            return this.openInventory(p);
        } catch (Throwable e) {
            e.printStackTrace();
        }

        return null;
    }

    public final void close() {
        for (HumanEntity human : new ArrayList<>(this.getBukkitInventory().getViewers())) {
            human.closeInventory();
        }
    }

    protected InventoryView openInventory(Player p) {
        return p.openInventory(this.getBukkitInventory());
    }

    public Inventory getBukkitInventory() {
        return bukkitInventory;
    }

    public Inventory getProxyInventory() {
        return inventory;
    }

    protected abstract void onGUIInventoryClick(InventoryClickEvent event);

    protected void onPlayerInventoryClick(InventoryClickEvent event) {
    }

    protected void onTickUpdate() {
    }

    protected void onPlayerCloseInv(InventoryCloseEvent event) {
    }

    protected void onPlayerDrag(InventoryDragEvent event) {
    }

    protected final int getInvSizeForCount(int count) {
        int size = count / 9 * 9;

        if (count % 9 > 0) {
            size += 9;
        }

        if (size < 9) {
            return 9;
        }

        return Math.min(size, 54);
    }

    public void setInvCheckOverride(boolean invCheckOverride) {
        this.invCheckOverride = invCheckOverride;
    }

    protected abstract void populate();

    private void cleanupErrors() {
        Iterator<Map.Entry<Integer, ErrorIcon>> iterator = errorIconMap.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry<Integer, ErrorIcon> entry = iterator.next();
            ErrorIcon icon = entry.getValue();

            if (System.currentTimeMillis() >= icon.getExpire()) {
                iterator.remove();
            }
        }
    }

    protected void repopulate() {
        try {
            this.inventory.clear();

            if (player == null || player.isOnline()) {
                this.populate();
                cleanupErrors();

                for (Map.Entry<Integer, GUIIcon> entry : guiIconMap.entrySet()) {
                    int slot = entry.getKey();
                    GUIIcon icon = entry.getValue();

                    this.inventory.setItem(slot, icon.getItemStack());
                }

                for (Map.Entry<Integer, ErrorIcon> entry : errorIconMap.entrySet()) {
                    int slot = entry.getKey();
                    ErrorIcon icon = entry.getValue();

                    this.inventory.setItem(slot, icon.toItem());
                }

                this.inventory.apply();
            }

            this.populated = true;
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    protected final void setUpdateTicks(int ticks) {
        this.setUpdateTicks(ticks, false);
    }

    protected final void setUpdateTicks(int ticks, boolean sync) {
        if (this.updaterTask != null) {
            this.updaterTask.cancel();
            this.updaterTask = null;
        }

        this.updaterTask = new GUIUpdateTask(this);

        if (sync) {
            this.updaterTask.runTaskTimer(Anchor.getPlugin(Anchor.class), 0, ticks);
        } else {
            this.updaterTask.runTaskTimerAsynchronously(Anchor.getPlugin(Anchor.class), 0, ticks);
        }
    }

    public void setSlot(int slot, @Nullable GUIIcon icon) {
        if (icon == null) {
            inventory.setItem(slot, null);
            guiIconMap.remove(slot);
        } else {
            inventory.setItem(slot, icon.getItemStack());
            guiIconMap.put(slot, icon);
        }
    }

    public void showError(int slot, String title, String... subtitle) {
        ErrorIcon icon = new ErrorIcon(title, subtitle, System.currentTimeMillis() + 3000);
        errorIconMap.put(slot, icon);
        repopulate();
    }

    protected final void scheduleOpen(final GUI gui, final Player player) {
        Bukkit.getScheduler().runTask(Anchor.getPlugin(Anchor.class), () -> gui.open(player));
    }

    protected void setAllowDrag(boolean allowDrag) {
        this.allowDrag = allowDrag;
    }

    protected boolean isAllowShiftClicking() {
        return allowShiftClicking;
    }

    protected void setAllowShiftClicking(boolean allowShiftClicking) {
        this.allowShiftClicking = allowShiftClicking;
    }

    protected Map<Integer, GUIIcon> getGuiIconMap() {
        return guiIconMap;
    }

    private class GUIEvents implements Listener {

        private GUI gui;

        public GUIEvents(GUI gui) {
            this.gui = gui;
        }

        @EventHandler
        public void onInventoryClick(InventoryClickEvent event) {
            try {
                if (this.gui.bukkitInventory.getViewers().contains(event.getWhoClicked())) {
                    List<InventoryAction> deniedActions = new ArrayList<>(Arrays.asList(
                            InventoryAction.CLONE_STACK,
                            InventoryAction.COLLECT_TO_CURSOR,
                            InventoryAction.UNKNOWN
                    ));

                    if (!allowShiftClicking) {
                        deniedActions.add(InventoryAction.MOVE_TO_OTHER_INVENTORY);
                    }

                    if (deniedActions.contains(event.getAction())) {
                        event.setCancelled(true);
                    }

                    if (!allowShiftClicking && event.getClick().isShiftClick()) {
                        event.setCancelled(true);
                    }

                    if (!invCheckOverride && event.getClickedInventory() == null) {
                        return;
                    }

                    if (!event.getClickedInventory().equals(gui.getBukkitInventory())) {
                        gui.onPlayerInventoryClick(event);
                        return;
                    }

                    event.setCancelled(true);

                    if (!(event.getWhoClicked() instanceof Player)) {
                        return;
                    }

                    if (gui.getGuiIconMap().containsKey(event.getRawSlot())) {
                        GUIIcon icon = gui.getGuiIconMap().get(event.getRawSlot());

                        if (icon.getClickFunction(event.getClick()) != null) {
                            String errorMsg = icon.getClickFunction(event.getClick()).apply(player, event.getRawSlot());

                            if (errorMsg != null) {
                                showError(event.getRawSlot(), "Uh oh!", ChatPaginator.wordWrap(errorMsg, 48));
                                player.playSound(player.getLocation(), Objects.requireNonNull(XSound.BLOCK_NOTE_BLOCK_BASS.parseSound()), 1f, 1f);
                            }

                            return; // Stop here to prevent double-firing from legacy system
                        }
                    }

                    gui.onGUIInventoryClick(event);
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }

        @EventHandler
        public void onInventoryClose(InventoryCloseEvent event) {
            if (!event.getInventory().equals(gui.getBukkitInventory())) {
                return;
            }

            if (bukkitInventory.getViewers().size() <= 1) {
                HandlerList.unregisterAll(this);

                try {
                    gui.onPlayerCloseInv(event);
                } catch (Throwable e) {
                    e.printStackTrace();
                }

                if (gui.updaterTask != null) {
                    gui.updaterTask.cancel();
                }
            }
        }

        @EventHandler
        public void onInventoryDrag(InventoryDragEvent event) {
            try {
                if (!event.getInventory().equals(gui.getBukkitInventory())) {
                    return;
                }

                if (!allowDrag) {
                    event.setCancelled(true);
                } else {
                    gui.onPlayerDrag(event);
                }
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    private class GUIUpdateTask extends BukkitRunnable {

        private GUI gui;

        public GUIUpdateTask(GUI gui) {
            this.gui = gui;
        }

        @Override
        public void run() {
            try {
                this.gui.repopulate();
                this.gui.onTickUpdate();
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    public class ErrorIcon {

        private String errorTitle;
        private String[] errorSubtitle;
        private long expire;

        public ErrorIcon(String errorTitle, String[] errorSubtitle, long expire) {
            this.errorTitle = errorTitle;
            this.errorSubtitle = errorSubtitle;
            this.expire = expire;
        }

        public ItemStack toItem() {
            ItemStack itemStack = XMaterial.BARRIER.parseItem();
            ItemMeta itemMeta = itemStack.getItemMeta();
            itemMeta.setDisplayName("§c§l" + errorTitle);

            List<String> lore = new ArrayList<>();
            for (String line : errorSubtitle) {
                lore.add("§7" + line);
            }

            itemMeta.setLore(lore);
            itemStack.setItemMeta(itemMeta);

            return itemStack;
        }

        public String getErrorTitle() {
            return errorTitle;
        }

        public String[] getErrorSubtitle() {
            return errorSubtitle;
        }

        public long getExpire() {
            return expire;
        }
    }

    public abstract class CrossCompatibilityInventory implements Inventory {
        public abstract HashMap<Integer, ItemStack> removeItemAnySlot(ItemStack... itemStacks) throws IllegalArgumentException;

        @Deprecated
        public abstract boolean contains(int i);

        public abstract ItemStack[] getStorageContents();

        public abstract void setStorageContents(ItemStack[] itemStacks);

        @Deprecated
        public abstract boolean contains(int i, int i1);

        @Deprecated
        public abstract HashMap<Integer, ? extends ItemStack> all(int i);

        @Deprecated
        public abstract int first(int i);

        @Deprecated
        public abstract void remove(int i);

        public abstract Location getLocation();
    }

    public class InventoryProxy extends CrossCompatibilityInventory {

        private Inventory mainInventory;
        private Inventory proxyInventory;

        private InventoryProxy(Inventory mainInventory, Inventory proxyInventory) {
            this.mainInventory = mainInventory;
            this.proxyInventory = proxyInventory;
        }

        public void apply() {
            this.mainInventory.setContents(this.proxyInventory.getContents());
        }

        @Override
        public int getSize() {
            return proxyInventory.getSize();
        }

        @Override
        public int getMaxStackSize() {
            return proxyInventory.getMaxStackSize();
        }

        @Override
        public void setMaxStackSize(int i) {
            proxyInventory.setMaxStackSize(i);
        }

        @Override
        public String getName() {
            return proxyInventory.getName();
        }

        @Override
        public ItemStack getItem(int i) {
            return proxyInventory.getItem(i);
        }

        @Override
        public void setItem(int i, ItemStack itemStack) {
            proxyInventory.setItem(i, itemStack);
        }

        @Override
        public HashMap<Integer, ItemStack> addItem(ItemStack... itemStacks) throws IllegalArgumentException { return proxyInventory.addItem(itemStacks); }

        @Override
        public HashMap<Integer, ItemStack> removeItem(ItemStack... itemStacks) throws IllegalArgumentException { return proxyInventory.removeItem(itemStacks); }

        @Override
        public HashMap<Integer, ItemStack> removeItemAnySlot(ItemStack... itemStacks) throws IllegalArgumentException {
            try {
                return (HashMap<Integer, ItemStack>) proxyInventory.getClass().getMethod("removeItemAnySlot", ItemStack[].class).invoke(proxyInventory, itemStacks);
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException ignored) {
                // Fallback if paper isn't installed.
                return removeItem(itemStacks);
            }
        }

        @Override
        public ItemStack[] getContents() {
            return proxyInventory.getContents();
        }

        @Override
        public void setContents(ItemStack[] itemStacks) throws IllegalArgumentException { proxyInventory.setContents(itemStacks); }

        @Override
        public boolean contains(int i) { return proxyInventory.contains(XMaterial.matchXMaterial(i, (byte) 0).parseMaterial()); }

        @Override
        public ItemStack[] getStorageContents() {
            return proxyInventory.getContents();
        }

        @Override
        public void setStorageContents(ItemStack[] itemStacks) throws IllegalArgumentException { proxyInventory.setContents(itemStacks); }

        @Override
        public boolean contains(Material material) throws IllegalArgumentException { return proxyInventory.contains(material); }

        @Override
        public boolean contains(ItemStack itemStack) {
            return proxyInventory.contains(itemStack);
        }

        @Override
        public boolean contains(int i, int i1) { return contains(XMaterial.matchXMaterial(i, (byte) 0).parseMaterial(), i1); }

        @Override
        public boolean contains(Material material, int i) throws IllegalArgumentException { return proxyInventory.contains(material, i); }

        @Override
        public boolean contains(ItemStack itemStack, int i) {
            return proxyInventory.contains(itemStack, i);
        }

        @Override
        public boolean containsAtLeast(ItemStack itemStack, int i) { return proxyInventory.containsAtLeast(itemStack, i); }

        @Override
        public HashMap<Integer, ? extends ItemStack> all(int i) { return all(XMaterial.matchXMaterial(i, (byte) 0).parseMaterial()); }

        @Override
        public HashMap<Integer, ? extends ItemStack> all(Material material) throws IllegalArgumentException { return proxyInventory.all(material); }

        @Override
        public HashMap<Integer, ? extends ItemStack> all(ItemStack itemStack) {
            return proxyInventory.all(itemStack);
        }

        @Override
        public int first(int i) { return first(XMaterial.matchXMaterial(i, (byte) 0).parseMaterial()); }

        @Override
        public int first(Material material) throws IllegalArgumentException { return proxyInventory.first(material); }

        @Override
        public int first(ItemStack itemStack) { return proxyInventory.first(itemStack); }

        @Override
        public int firstEmpty() { return proxyInventory.firstEmpty(); }

        @Override
        public void remove(int i) { proxyInventory.remove(XMaterial.matchXMaterial(i, (byte) 0).parseMaterial()); }

        @Override
        public void remove(Material material) throws IllegalArgumentException { proxyInventory.remove(material); }

        @Override
        public void remove(ItemStack itemStack) { proxyInventory.remove(itemStack); }

        @Override
        public void clear(int i) { proxyInventory.clear(i); }

        @Override
        public void clear() { proxyInventory.clear(); }

        @Override
        public List<HumanEntity> getViewers() { return mainInventory.getViewers(); }

        @Override
        public String getTitle() { return mainInventory.getTitle(); }

        @Override
        public InventoryType getType() { return mainInventory.getType(); }

        @Override
        public InventoryHolder getHolder() { return mainInventory.getHolder(); }

        @Override
        public ListIterator<ItemStack> iterator() { return proxyInventory.iterator(); }

        @Override
        public ListIterator<ItemStack> iterator(int i) { return proxyInventory.iterator(i); }

        @Override
        public Location getLocation() { return null; }
    }

}