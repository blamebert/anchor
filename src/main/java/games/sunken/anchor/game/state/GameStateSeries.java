package games.sunken.anchor.game.state;

import games.sunken.anchor.Anchor;
import org.bukkit.Bukkit;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class GameStateSeries {

    private boolean started = false;
    private int current = 0;
    private List<GameState> gameStates;

    public GameStateSeries(@Nonnull GameState defaultGameState) {
        gameStates = new ArrayList<>();
        gameStates.add(defaultGameState);
    }

    public void add(@Nonnull GameState gameState) {
        gameStates.add(gameState);
    }

    public void addAll(@Nonnull Collection<GameState> states) {
        gameStates.addAll(states);
    }

    public void insert(@Nonnull GameState gameState, int index) {
        gameStates.add(index, gameState);
    }

    public int getCurrentStateID() {
        return current;
    }

    public GameState getFirstState() {
        return gameStates.get(0);
    }

    public GameState getCurrentState() {
        return gameStates.get(current);
    }

    public GameState getLastState() {
        return gameStates.get(gameStates.size()-1);
    }

    public List<GameState> getGameStates() {
        return gameStates;
    }

    public boolean isStarted() {
        return started;
    }

    public void setCurrentState(GameState gameState) {
        Objects.requireNonNull(gameState);

        if (gameState != gameStates.get(current)) {
            if (gameStates.contains(gameState)) {
                int oldState = current;
                gameStates.get(oldState).onStateCleanup();
                gameStates.get(current).end();

                current = gameStates.indexOf(gameState);

                for (int i = Math.min(current + 1, oldState + 1); i < Math.max(current, oldState); i++) {
                    gameStates.get(i).onStatePreparation();
                    gameStates.get(i).onStateCleanup();
                }

                gameState.onStatePreparation();
                gameState.start();
            } else {
                throw new IllegalArgumentException("\"" + gameState.getInternalName() + "\" is not a gamestate within the series.");
            }
        }
    }

    public void skipGameState() {
        if ((current + 1) >= gameStates.size()) {
            end();
        } else {
            setCurrentState(gameStates.get(current + 1));
        }
    }

    public void start() {
        if (gameStates.isEmpty()) {
            end();
            return;
        }

        if (started) return;

        started = true;
        gameStates.get(current).start();
    }

    public void tick() {
        gameStates.get(current).tick();

        if (gameStates.get(current).canEnd() && !gameStates.get(current).isPaused()) {
            if ((current + 1) >= gameStates.size()) {
                current++;
                end();
                return;
            }

            setCurrentState(gameStates.get(current + 1));
        }
    }

    public void end() {
        if (current < gameStates.size()) {
            gameStates.get(current).onStateCleanup();
            gameStates.get(current).end();
        } else if (gameStates.size() >= current) {
            Anchor.getPlugin(Anchor.class).getLogger().info("GAME HAS ENDED! RESTARTING...");
            Bukkit.shutdown();
        }
    }

}