package games.sunken.anchor.game.state;

import games.sunken.anchor.util.XMaterial;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import java.time.Duration;
import java.time.Instant;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

public abstract class GameState {

    public enum JoinPermission {

        ALLOWED, SPECTATE, DENIED

    }

    private final JavaPlugin plugin;

    private final String name;
    private final String internalName;

    private Instant startTime;
    private final Duration duration;

    private boolean started = false;
    private boolean ended = false;
    private boolean paused = false;

    private final JoinPermission joinPermission;

    private final XMaterial icon;

    private final Set<Listener> listeners = new HashSet<>();
    private final Set<BukkitTask> tasks = new HashSet<>();

    private static final Map<Integer, Supplier<Boolean>> runUntil = new LinkedHashMap<>();

    public GameState(JavaPlugin plugin, String name, String internalName, Duration duration, JoinPermission joinPermission, XMaterial icon) {
        this.plugin = plugin;
        this.name = name;
        this.internalName = internalName;
        this.duration = duration;
        this.joinPermission = joinPermission;
        this.icon = icon;

        repeatAsync(() -> runUntil.entrySet().removeIf(entry -> {
            if (entry.getValue().get()) {
                Bukkit.getScheduler().cancelTask(entry.getKey());
                return true;
            }

            return false;
        }), 0L, 1L);
    }

    final void start() {
        if (started || ended || paused) return;

        started = true;
        startTime = Instant.now();

        try {
            onStart();
        } catch (Exception ex) {
            plugin.getLogger().severe("** Error Starting GameState: " + internalName + "! **");
            ex.printStackTrace();
        }
    }

    final void tick() {
        if (!started || ended || paused) return;

        if (canEnd() && !paused) {
            end();
            return;
        }

        try {
            onTick();
        } catch (Exception ex) {
            plugin.getLogger().severe("** Error Ticking GameState: " + internalName + "! **");
            ex.printStackTrace();
        }
    }

    public final void end() {
        if (!canEnd() || paused) return;

        ended = true;

        try {
            onEnd();
        } catch (Exception ex) {
            plugin.getLogger().severe("** Error Ending GameState: " + internalName + "! **");
            ex.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }

    public String getInternalName() {
        return internalName;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public Duration getDuration() {
        return duration;
    }

    public boolean isStarted() {
        return started;
    }

    public boolean isEnded() {
        return ended;
    }

    public JoinPermission getJoinPermission() {
        return joinPermission;
    }

    public boolean isPaused() {
        return paused;
    }

    public void setPaused(boolean paused) {
        this.paused = paused;
    }

    public XMaterial getIcon() { return icon; }

    public Duration getRemainingDuration() {
        return duration.minus(Duration.between(startTime, Instant.now()));
    }

    public boolean canEnd() {
        return getRemainingDuration().isNegative() || getRemainingDuration().isZero();
    }

    public void register(Listener listener) {
        listeners.add(listener);
        plugin.getServer().getPluginManager().registerEvents(listener, plugin);
    }

    public BukkitTask runLater(Runnable runnable, long delay) {
        BukkitTask task = plugin.getServer().getScheduler().runTaskLater(plugin, runnable, delay);
        tasks.add(task);

        return task;
    }

    public BukkitTask repeat(Runnable runnable, long delay, long interval) {
        BukkitTask task = plugin.getServer().getScheduler().runTaskTimer(plugin, runnable, delay, interval);
        tasks.add(task);

        return task;
    }

    public void runUntil(Runnable runnable, long delay, long interval, Supplier<Boolean> until) {
        final BukkitTask task = repeat(runnable, delay, interval);
        runUntil.put(task.getTaskId(), until);
    }

    public BukkitTask runLaterAsync(Runnable runnable, long delay) {
        BukkitTask task = plugin.getServer().getScheduler().runTaskLaterAsynchronously(plugin, runnable, delay);
        tasks.add(task);

        return task;
    }

    public BukkitTask repeatAsync(Runnable runnable, long delay, long interval) {
        BukkitTask task = plugin.getServer().getScheduler().runTaskTimerAsynchronously(plugin, runnable, delay, interval);
        tasks.add(task);

        return task;
    }

    public void runUntilAsync(Runnable runnable, long delay, long interval, Supplier<Boolean> until) {
        final BukkitTask task = repeatAsync(runnable, delay, interval);
        runUntil.put(task.getTaskId(), until);
    }

    public abstract void onStatePreparation();
    public abstract void onStart();
    public abstract void onTick();

    public void onStateCleanup() {
        listeners.forEach(HandlerList::unregisterAll);
        tasks.forEach(BukkitTask::cancel);
        listeners.clear();
        tasks.clear();
    }

    public abstract void onEnd();

    public abstract void sendScoreboard();

}