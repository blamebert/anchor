package games.sunken.anchor.game.map;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import games.sunken.anchor.Anchor;
import games.sunken.anchor.database.Mongo;
import games.sunken.anchor.event.MapDeleteEvent;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MapManager {

    private final List<Map> waitingForWorld;
    private final Set<MapInfo> mapCache;
    private final Set<MapSettings> settingsCache;
    private final File mapDownloadsFolder;

    public MapManager() {
        this.waitingForWorld = new ArrayList<>();
        this.mapCache = new HashSet<>();
        this.mapDownloadsFolder = new File(Anchor.getPlugin(Anchor.class).getDataFolder() + File.separator + "map-downloads");
        this.settingsCache = new HashSet<>();

        if (mapDownloadsFolder.exists()) {
            for (File file : mapDownloadsFolder.listFiles()) {
                if (file.getName().endsWith(".zip")) {
                    File worldFolder = new File(file.getName().substring(0, file.getName().length() - 4));

                    if (worldFolder.exists()) {
                        try {
                            FileUtils.deleteDirectory(worldFolder);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    public void refreshMaps(String game) {
        mapCache.clear();
        settingsCache.clear();

        BasicDBObject query = new BasicDBObject();
        query.put("game", game);

        DBCursor cursor = Mongo.getClient().getDB("sunken").getCollection("maps").find(query);

        while (cursor.hasNext()) {
            DBObject obj = cursor.next();

            MapInfo mapInfo = Anchor.getPlugin(Anchor.class).getGSON().fromJson(obj.toString(), MapInfo.class);

            DBObject settingsObj = Mongo.getClient().getDB("sunken").getCollection("map_settings").findOne(mapInfo.getUUID());
            MapSettings mapSettings;

            if (settingsObj != null) {
                mapSettings = Anchor.getPlugin(Anchor.class).getGSON().fromJson(settingsObj.toString(), MapSettings.class);
            } else {
                mapSettings = new MapSettings(mapInfo.getUUID());
                Mongo.getMapSettingsDAO().save(mapSettings);
            }

            mapCache.add(mapInfo);
            settingsCache.add(mapSettings);
        }
    }

    public List<Map> getWaitingForWorld() {
        return waitingForWorld;
    }

    public Set<MapInfo> getMapCache() {
        return mapCache;
    }

    public Set<MapSettings> getSettingsCache() {
        return settingsCache;
    }

    public File getMapDownloadsFolder() { return mapDownloadsFolder; }

    public MapSettings getSettings(MapInfo map) {
        return getSettingsCache().stream().filter(mapSettings -> mapSettings.getMap().equals(map.getUUID())).findFirst().orElse(null);
    }

    public void deleteMap(MapInfo mapInfo) {
        BasicDBObject query = new BasicDBObject();
        query.put("game", mapInfo.getGame());
        query.put("_id", mapInfo.getName());
        query.put("description", mapInfo.getDescription());
        query.put("version", mapInfo.getVersion());
        query.put("authors", mapInfo.getAuthors());
        query.put("url", mapInfo.getUrl());
        query.put("uuid", mapInfo.getUUID());

        Bukkit.getPluginManager().callEvent(new MapDeleteEvent(mapInfo));

        Mongo.getDatastore().getCollection(MapInfo.class).findAndRemove(query);

        query = new BasicDBObject();
        query.put("_id", mapInfo.getUUID());

        Mongo.getDatastore().getCollection(MapSettings.class).findAndRemove(query);
    }
}