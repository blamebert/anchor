package games.sunken.anchor.game.map;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.database.Mongo;
import org.bukkit.Bukkit;
import org.bukkit.World;

import javax.annotation.Nonnull;
import java.util.UUID;

public class Map {

    private MapInfo info;
    private MapSettings settings;
    private UUID uuid;
    private World world;

    public Map(@Nonnull MapInfo info, @Nonnull UUID uuid) {
        this.info = info;
        this.uuid = uuid;

        if (Anchor.getPlugin(Anchor.class).getMapManager().getSettings(info) == null) {
            this.settings = new MapSettings(info.getUUID());
        } else {
            this.settings = Anchor.getPlugin(Anchor.class).getMapManager().getSettings(info);
        }
    }

    public MapInfo getInfo() {
        return info;
    }

    public UUID getUuid() {
        return uuid;
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public MapSettings getSettings() {
        return settings;
    }

    public void save() {
        Bukkit.getScheduler().runTaskAsynchronously(Anchor.getPlugin(Anchor.class), () -> {
            Mongo.getMapInfoDAO().save(info);
            Mongo.getMapSettingsDAO().save(settings);
        });
    }

}