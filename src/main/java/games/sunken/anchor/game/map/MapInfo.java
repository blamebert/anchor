package games.sunken.anchor.game.map;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.event.MapCreateEvent;
import games.sunken.spigot.AsyncWorldLoader;
import org.apache.commons.io.IOUtils;
import org.bukkit.Bukkit;
import org.bukkit.WorldCreator;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import javax.annotation.Nonnull;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Enumeration;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

@Entity("maps")
public class MapInfo {

    private String game;
    @Id
    private String _id; // map name
    private String description;
    private String version;
    private String authors;
    private String url;
    private String uuid;

    public MapInfo() {
    }

    public MapInfo(@Nonnull String game, @Nonnull String name, @Nonnull String description, @Nonnull String version, @Nonnull String authors, @Nonnull String url, @Nonnull String uuid) {
        this.game = game;
        this._id = name;
        this.description = description;
        this.version = version;
        this.authors = authors;
        this.url = url;
        this.uuid = uuid;
    }

    public String getGame() {
        return game;
    }

    public String getName() {
        return _id;
    }

    public String getDescription() {
        return description;
    }

    public String getVersion() {
        return version;
    }

    public String getAuthors() {
        return authors;
    }

    public String getUrl() {
        return url;
    }

    public String getUUID() {
        return uuid;
    }

    public Map downloadMap() {
        try {
            File file = new File(Anchor.getPlugin(Anchor.class).getMapManager().getMapDownloadsFolder(), uuid + ".zip");

            if (!file.exists() && file.mkdirs()) { // Don't download multiple times
                URL link = new URL(url);

                Files.copy(link.openStream(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }

            File dest = new File(Bukkit.getWorldContainer().getPath() + File.separator + uuid);

            if (!dest.exists()) { // Don't create world folder if it's already there
                unzip(file.getPath(), dest.getPath());
            }

            Map map = new Map(this, UUID.fromString(uuid));

            Anchor.getPlugin(Anchor.class).getMapManager().getWaitingForWorld().add(map);

            AsyncWorldLoader.createWorld(new WorldCreator(uuid), Bukkit.getConsoleSender(), (short) ((int) Double.parseDouble(map.getSettings().getSettings().getOrDefault("map-radius", "250"))));

            Bukkit.getPluginManager().callEvent(new MapCreateEvent(map));

            return map;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void unzip(@Nonnull String path, @Nonnull String destination) {

        try (ZipFile zipFile = new ZipFile(new File(path))) {
            Enumeration<? extends ZipEntry> entries = zipFile.entries();

            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                File entryDestination = new File(destination, entry.getName());

                if (entry.isDirectory()) {
                    entryDestination.mkdirs();
                } else {
                    entryDestination.getParentFile().mkdirs();
                    InputStream in = zipFile.getInputStream(entry);
                    OutputStream out = new FileOutputStream(entryDestination);

                    IOUtils.copy(in, out);
                    IOUtils.closeQuietly(in);

                    out.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}