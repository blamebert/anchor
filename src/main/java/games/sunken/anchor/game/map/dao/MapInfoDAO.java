package games.sunken.anchor.game.map.dao;

import games.sunken.anchor.game.map.MapInfo;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class MapInfoDAO  extends BasicDAO<MapInfo, String> {

    public MapInfoDAO(Class<MapInfo> entityClass, Datastore datastore) {
        super(entityClass, datastore);
    }

}