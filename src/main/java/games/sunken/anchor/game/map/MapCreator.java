package games.sunken.anchor.game.map;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.chat.Prompt;
import games.sunken.anchor.player.SunkenPlayer;
import games.sunken.anchor.util.XSound;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class MapCreator {

    private SunkenPlayer sunkenPlayer;
    private java.util.Map<Integer, Prompt> prompts;
    private int currentPrompt;

    private String name;
    private String description;
    private String version;
    private String authors;
    private String url;
    private UUID uuid;
    private MapInfo mapInfo;

    public MapCreator(SunkenPlayer sunkenPlayer) {
        this.sunkenPlayer = sunkenPlayer;
        this.prompts = new HashMap<>();
        this.currentPrompt = 0;
        this.uuid = UUID.randomUUID();

        prompts.put(0, new Prompt(MsgType.SUCCESS, "Please enter a name for this map...", XSound.BLOCK_NOTE_BLOCK_SNARE,
                "Type your response in chat. Type \"cancel\" to cancel map creation.") {
            @Override
            public boolean onSubmit(Player player, String submitted) {
                name = submitted;

                return true;
            }

            @Override
            public void onCancel(Player player) {

            }
        });

        prompts.put(1, new Prompt(MsgType.SUCCESS, "Please enter a description for this map...", XSound.BLOCK_NOTE_BLOCK_SNARE,
                "Type your response in chat. Type \"cancel\" to cancel map creation.") {
            @Override
            public boolean onSubmit(Player player, String submitted) {
                description = submitted;

                return true;
            }

            @Override
            public void onCancel(Player player) {

            }
        });

        prompts.put(2, new Prompt(MsgType.SUCCESS, "Please enter a version for this map...", XSound.BLOCK_NOTE_BLOCK_SNARE,
                "Type your response in chat. Type \"cancel\" to cancel map creation.") {
            @Override
            public boolean onSubmit(Player player, String submitted) {
                version = submitted;

                return true;
            }

            @Override
            public void onCancel(Player player) {

            }
        });

        prompts.put(3, new Prompt(MsgType.SUCCESS, "Please enter the authors of this map...", XSound.BLOCK_NOTE_BLOCK_SNARE,
                "Type your response in chat. Type \"cancel\" to cancel map creation.") {
            @Override
            public boolean onSubmit(Player player, String submitted) {
                authors = submitted;

                return true;
            }

            @Override
            public void onCancel(Player player) {

            }
        });

        prompts.put(4, new Prompt(MsgType.SUCCESS, "Please enter a valid URL for a download of this map...", XSound.BLOCK_NOTE_BLOCK_SNARE,
                "Type your response in chat. Type \"cancel\" to cancel map creation.") {
            @Override
            public boolean onSubmit(Player player, String submitted) {
                url = submitted;
                String game = Anchor.getPlugin(Anchor.class).getGame().getName();

                mapInfo = new MapInfo(game, name, description, version, authors, url, uuid.toString());

                sunkenPlayer.sendMessage(MsgType.WARNING, XSound.BLOCK_NOTE_BLOCK_PLING, new TextComponent("Downloading map & loading world..."));

                Bukkit.getScheduler().runTask(Anchor.getPlugin(Anchor.class), mapInfo::downloadMap);

                return true;
            }

            @Override
            public void onCancel(Player player) {

            }
        });

        sunkenPlayer.setPrompt(prompts.get(currentPrompt));
    }

    public java.util.Map<Integer, Prompt> getPrompts() {
        return prompts;
    }

    public int getCurrentPrompt() {
        return currentPrompt;
    }

    public void setCurrentPrompt(int currentPrompt) {
        this.currentPrompt = currentPrompt;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getVersion() {
        return version;
    }

    public String getAuthors() {
        return authors;
    }

    public String getUrl() {
        return url;
    }

    public UUID getUUID() {
        return uuid;
    }

    public MapInfo getMapInfo() {
        return mapInfo;
    }

}