package games.sunken.anchor.game.map.dao;

import games.sunken.anchor.game.map.MapSettings;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class MapSettingsDAO extends BasicDAO<MapSettings, String> {

    public MapSettingsDAO(Class<MapSettings> entityClass, Datastore datastore) {
        super(entityClass, datastore);
    }

}