package games.sunken.anchor.game.map;

import games.sunken.anchor.util.Position;
import org.bukkit.Location;
import org.bukkit.World;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Entity("map_settings")
public class MapSettings {

    @Id private String _id;

    private Map<String, List<String>> locations;
    private Map<String, String> settings;

    public MapSettings() {}

    public MapSettings(String mapUuid) {
        this._id = mapUuid;
        this.locations = new HashMap<>();
        this.settings = new HashMap<>();
    }

    public String getMap() {
        return _id;
    }

    public Map<String, List<String>> getLocations() {
        return locations;
    }

    public Map<String, String> getSettings() {
        return settings;
    }

    public void saveLocation(String key, Location location) {
        String position = new Position(location).toString();

        if (!locations.containsKey(key) || locations.get(key) == null) {
            locations.put(key, new ArrayList<>());
        }

        locations.get(key).add(position);
    }

    public void removeLocation(String key, Location location) {
        String position = new Position(location).toString();

        if (locations.containsKey(key) && locations.get(key) != null && locations.get(key).contains(position)) {
            locations.get(key).remove(position);
        }
    }

    public List<Location> getLocations(String key, World world) {
        return locations.isEmpty() || locations.get(key) == null || locations.get(key).isEmpty() ? new ArrayList<>() : locations.get(key).stream().map(loc -> new Position(loc).toLocation(world)).collect(Collectors.toList());
    }

}