package games.sunken.anchor.game;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.game.map.Map;
import games.sunken.anchor.game.state.GameState;
import games.sunken.anchor.game.state.GameStateSeries;
import games.sunken.anchor.game.teams.Team;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public abstract class Game {

    private GameStateSeries gameStates;
    private Map map;
    private List<Team> teams;
    private BukkitTask gameTask;

    public Game(@Nullable Map map, @Nonnull List<Team> teams) {
        this.map = map;
        this.teams = teams;
    }

    public GameStateSeries getGameStates() { return gameStates; }

    public void setGameStates(@Nonnull GameStateSeries gameStates) {
        if (this.gameStates != gameStates) {
            if (this.gameStates != null) {
                if (this.gameStates.getCurrentState() != null) {
                    this.gameStates.getCurrentState().end();
                }

                gameTask.cancel();
            }

            this.gameStates = gameStates;

            gameStates.start();

            this.gameTask = new BukkitRunnable() {
                @Override
                public void run() {
                    gameStates.tick();
                }
            }.runTaskTimer(Anchor.getPlugin(Anchor.class), 20L, 20L);
        }
    }

    public Map getMap() { return map; }

    public Set<UUID> getAllPlayers() {
        return getAllTeams().stream().flatMap(team -> team.getMembers().stream()).collect(Collectors.toSet());
    }

    public Set<UUID> getActivePlayers() {
        return getPlayableTeams().stream()
                .flatMap(team -> team.getMembers().stream())
                .collect(Collectors.toSet());
    }

    public Set<UUID> getSpectators() {
        return getTeam("Spectators") == null ? null : getTeam("Spectators").getMembers();
    }

    public List<Team> getAllTeams() {
        return teams;
    }

    public List<Team> getPlayableTeams() {
        return getAllTeams().stream()
                .filter(team -> !team.getName().equalsIgnoreCase("Spectators"))
                .collect(Collectors.toList());
    }

    public Team getTeam(@Nonnull Player player) {
        return getAllTeams().stream()
                .filter(team -> team.getMembers().contains(player.getUniqueId()))
                .findFirst()
                .orElse(null);
    }

    public Team getTeam(@Nonnull String name) {
        return getAllTeams().stream()
                .filter(team -> team.getName().replaceAll(" ", "_").equalsIgnoreCase(name.replaceAll(" ", "_")))
                .findFirst()
                .orElse(null);
    }

    public void assignToTeam(@Nonnull Player player) {
        if (getGameStates().getCurrentState().getJoinPermission() == GameState.JoinPermission.SPECTATE) {
            if (getTeam("Spectators") != null) {
                getTeam("Spectators").addMember(player);

                return;
            }
        }

        Team smallest = null;

        int min = Integer.MAX_VALUE;

        for (Team team : getPlayableTeams()) {
            if (team.getMembers().size() < min && team.getMembers().size() < team.getMaxPlayers()) {
                smallest = team;
                min = team.getMembers().size();
            }
        }

        if (smallest != null) {
            smallest.addMember(player);
        } else {
            if (getTeam("Spectators") != null) getTeam("Spectators").addMember(player);
        }
    }

    public void broadcastMessage(@Nonnull String message) {
        getAllPlayers().stream()
                .filter(uuid -> Bukkit.getPlayer(uuid) != null)
                .filter(uuid -> Bukkit.getPlayer(uuid).isOnline())
                .forEach(uuid -> Bukkit.getPlayer(uuid).sendMessage(message));
    }

    public abstract String getName();
    public abstract String getVersion();
    public abstract String getAuthors();
    public abstract String[] getDescription();

    public abstract int getMinPlayers();
    public abstract int getMaxPlayers();

}