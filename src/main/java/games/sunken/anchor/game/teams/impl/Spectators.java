package games.sunken.anchor.game.teams.impl;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.game.teams.Team;
import games.sunken.anchor.items.ItemBuilder;
import games.sunken.anchor.util.Color;
import games.sunken.anchor.util.Reflection;
import games.sunken.anchor.util.XMaterial;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nonnull;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class Spectators implements Team {

    private Set<UUID> members;
    private List<Location> spawns;

    private ItemStack teleporter;
    private ItemStack settings;
    private ItemStack lobby;

    public Spectators(@Nonnull List<Location> spawns) {
        this.members = new HashSet<>();
        this.spawns = spawns;

        this.teleporter = new ItemBuilder(XMaterial.COMPASS)
                .withDisplayName(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Teleporter " + ChatColor.GRAY + "" + ChatColor.ITALIC + "(Right-Click)")
                .withLoreLine(" ")
                .withLoreLine(ChatColor.WHITE + "Right-Click " + ChatColor.GRAY + "to view & teleport to players.")
                .build();

        this.settings = new ItemBuilder(XMaterial.ANVIL)
                .withDisplayName(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Spectator Settings " + ChatColor.GRAY + "" + ChatColor.ITALIC + "(Right-Click)")
                .withLoreLine(" ")
                .withLoreLine(ChatColor.WHITE + "Right-Click " + ChatColor.GRAY + "to edit your spectator settings.")
                .build();

        this.lobby = new ItemBuilder(XMaterial.OAK_BOAT)
                .withDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "Return to Lobby " + ChatColor.GRAY + "" + ChatColor.ITALIC + "(Right-Click)")
                .withLoreLine(" ")
                .withLoreLine(ChatColor.WHITE + "Right-Click " + ChatColor.GRAY + "to return to the lobby.")
                .build();
    }

    @Override
    public String getName() {
        return "Spectators";
    }

    @Override
    public Color getColor() {
        return Color.RED;
    }

    @Override
    public int getMaxPlayers() {
        return Bukkit.getMaxPlayers();
    }

    @Override
    public Set<UUID> getMembers() {
        return this.members;
    }

    @Override
    public void addMember(@Nonnull Player player) {
        getMembers().add(player.getUniqueId());

        Anchor.getPlugin(Anchor.class).getGame().getAllPlayers().forEach(uuid -> {
            Player pl = Bukkit.getPlayer(uuid);

            if (pl != null && pl.isOnline()) {
                pl.hidePlayer(player);
            }
        });

        player.teleport(getSpawnPoints().get(0));
        player.setGameMode(GameMode.SURVIVAL);
        player.setAllowFlight(true);
        player.setFlying(true);
        player.setCanPickupItems(false);
        player.setItemOnCursor(null);
        player.setLeashHolder(null);
        player.setExp(0F);
        player.setLevel(0);
        player.setFireTicks(0);
        player.setHealth(player.getMaxHealth());
        player.setFoodLevel(20);
        player.setSaturation(20);
        player.setFallDistance(0f);
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.setLastDamageCause(null);

        player.spigot().setCollidesWithEntities(false);

        if (Reflection.VERSION_NUMBER >= 190) {
            try {
                player.getClass().getMethod("setInvulnerable", Player.class).invoke(player, true);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        player.getInventory().setItem(0, this.teleporter);
        player.getInventory().setItem(4, this.settings);
        player.getInventory().setItem(8, this.lobby);
    }

    @Override
    public void removeMember(@Nonnull Player player) {
        getMembers().remove(player.getUniqueId());

        player.getInventory().clear();
        player.getInventory().setArmorContents(null);

        if (player.getGameMode() == GameMode.SPECTATOR) {
            player.setSpectatorTarget(null);
        }
    }

    @Override
    public void broadcastMessage(@Nonnull String message) {
        for (UUID uuid : getMembers()) {
            if (Bukkit.getPlayer(uuid) != null && Bukkit.getPlayer(uuid).isOnline()) {
                Bukkit.getPlayer(uuid).sendMessage(message);
            }
        }
    }

    @Override
    public List<Location> getSpawnPoints() {
        return spawns;
    }

    public ItemStack getTeleporter() {
        return teleporter;
    }

    public ItemStack getSettingsItem() {
        return settings;
    }

    public ItemStack getLobbyItem() {
        return lobby;
    }

}