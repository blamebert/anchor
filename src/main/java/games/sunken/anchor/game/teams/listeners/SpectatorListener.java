package games.sunken.anchor.game.teams.listeners;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.game.Game;
import games.sunken.anchor.game.teams.impl.Spectators;
import games.sunken.anchor.gui.impl.SpectatorGUI;
import games.sunken.anchor.player.SunkenPlayer;
import games.sunken.anchor.util.XMaterial;
import games.sunken.anchor.util.XSound;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.enchantment.PrepareItemEnchantEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.vehicle.VehicleDamageEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.event.vehicle.VehicleEntityCollisionEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.Objects;

public class SpectatorListener implements Listener {

    private Game game;
    private Spectators spectators;

    public SpectatorListener(Game game, Spectators spectators) {
        this.game = game;
        this.spectators = spectators;
    }

    @EventHandler
    public void onBedEnter(PlayerBedEnterEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockDamage(BlockDamageEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockIgnite(BlockIgniteEvent event) {
        if (event.getCause() == BlockIgniteEvent.IgniteCause.FLINT_AND_STEEL || event.getCause() == BlockIgniteEvent.IgniteCause.FIREBALL) {
            if (event.getIgnitingEntity() != null) {
                if (event.getIgnitingEntity() instanceof Player && spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
            event.getPlayer().updateInventory();
        }
    }

    @EventHandler
    public void onBucketEmpty(PlayerBucketEmptyEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onBucketFill(PlayerBucketFillEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onDropItem(PlayerDropItemEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
            event.getPlayer().updateInventory();
        }
    }

    @EventHandler
    public void onEditBook(PlayerEditBookEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onEnchantItem(EnchantItemEvent event) {
        if (spectators.getMembers().contains(event.getEnchanter().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof Player && spectators.getMembers().contains(event.getEntity().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (spectators.getMembers().contains(event.getEntity().getUniqueId()) || spectators.getMembers().contains(event.getDamager().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntityShootBow(EntityShootBowEvent event) {
        if (event.getEntity() instanceof Player && spectators.getMembers().contains(event.getEntity().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntityTame(EntityTameEvent event) {
        if (event.getOwner() instanceof Player && spectators.getMembers().contains(event.getOwner().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntityTargetLivingEntity(EntityTargetLivingEntityEvent event) {
        if (event.getTarget() instanceof Player && spectators.getMembers().contains(event.getTarget().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent event) {
        if (event.getEntity() instanceof Player && spectators.getMembers().contains(event.getEntity().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onFish(PlayerFishEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onHangingBreakByEntity(HangingBreakByEntityEvent event) {
        if (event.getRemover() instanceof Player && spectators.getMembers().contains(event.getRemover().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onHangingPlace(HangingPlaceEvent event) {
        if (spectators.getMembers().contains(Objects.requireNonNull(event.getPlayer()).getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (spectators.getMembers().contains(event.getWhoClicked().getUniqueId())) {
            if (event.getClickedInventory() instanceof PlayerInventory) {
                event.setResult(Event.Result.DENY);
                event.setCancelled(true);
                event.setCurrentItem(null);
            }
        }
    }

    @EventHandler
    public void onInventoryDrag(InventoryDragEvent event) {
        if (spectators.getMembers().contains(event.getWhoClicked().getUniqueId())) {
            if (event.getInventory() instanceof PlayerInventory) {
                event.setResult(Event.Result.DENY);
                event.setCancelled(true);
                event.setCursor(null);
            }
        }
    }

    @EventHandler
    public void onPlayerArmorStandManipulate(PlayerArmorStandManipulateEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerBedEnter(PlayerBedEnterEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerBucketEmpty(PlayerBucketEmptyEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerBucketFill(PlayerBucketFillEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            Player player = event.getPlayer();

            if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
                this.handleClick(player);
            }

            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEntityEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            if (event.getRightClicked().getType() == EntityType.PLAYER) {
                if (!event.isCancelled()) {
                    Player player = event.getPlayer();
                    Player target = (Player) event.getRightClicked();

                    SunkenPlayer sunkenPlayer = Anchor.getPlugin(Anchor.class).getPlayerManager().getPlayer(player.getUniqueId());

                    player.setGameMode(GameMode.SPECTATOR);
                    player.teleport(target);
                    player.setSpectatorTarget(target);
                    sunkenPlayer.sendBars(ChatColor.DARK_AQUA + "Spectating: " + ChatColor.WHITE + target.getDisplayName(),
                            ChatColor.AQUA + "Press SNEAK to exit spectator mode.", " ", 0, 40, 20);
                    sunkenPlayer.sendMessage(MsgType.SUCCESS, XSound.BLOCK_WOODEN_PRESSURE_PLATE_CLICK_ON,
                            new TextComponent("Now spectating <p>. Press SNEAK to exit spectator mode."), target.getDisplayName());
                }
            } else {
                this.handleClick(event.getPlayer());
            }

            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerToggleSneak(PlayerToggleSneakEvent event) {
        if (event.getPlayer().isSneaking() && spectators.getMembers().contains(event.getPlayer().getUniqueId()) && event.getPlayer().getGameMode() == GameMode.SPECTATOR) {
            event.getPlayer().setGameMode(GameMode.SURVIVAL);
            event.getPlayer().setAllowFlight(true);
            event.getPlayer().setFlying(true);
        }
    }

    @EventHandler
    public void onPlayerItemConsume(PlayerItemConsumeEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerItemDamage(PlayerItemDamageEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
            event.getPlayer().updateInventory();
        }
    }

    @EventHandler
    public void onPlayerLeashEntity(PlayerLeashEntityEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerPickupItem(PlayerPickupItemEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerPortal(PlayerPortalEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerShearEntity(PlayerShearEntityEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerUnleashEntity(PlayerUnleashEntityEvent event) {
        if (spectators.getMembers().contains(event.getPlayer().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPrepareItemEnchant(PrepareItemEnchantEvent event) {
        if (spectators.getMembers().contains(event.getEnchanter().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onProjectileLaunch(ProjectileLaunchEvent event) {
        if (event.getEntity().getShooter() instanceof Player && spectators.getMembers().contains(((Player) event.getEntity().getShooter()).getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onVehicleDamage(VehicleDamageEvent event) {
        if (event.getAttacker() instanceof Player && spectators.getMembers().contains(event.getAttacker().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onVehicleDestroy(VehicleDestroyEvent event) {
        if (event.getAttacker() instanceof Player && spectators.getMembers().contains(event.getAttacker().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onVehicleEnter(VehicleEnterEvent event) {
        if (event.getEntered() instanceof Player && spectators.getMembers().contains(event.getEntered().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onVehicleEntityCollision(VehicleEntityCollisionEvent event) {
        if (event.getEntity() instanceof Player && spectators.getMembers().contains(event.getEntity().getUniqueId())) {
            event.setCancelled(true);
        }
    }

    private void handleClick(Player player) {
        if (player.getInventory().getItemInHand().getType() != XMaterial.AIR.parseMaterial()) {
            ItemStack item = player.getInventory().getItemInHand();
            SunkenPlayer sunkenPlayer = Anchor.getPlugin(Anchor.class).getPlayerManager().getPlayer(player.getUniqueId());

            if (item.isSimilar(spectators.getTeleporter())) {
                new SpectatorGUI(game).open(player);
                player.playSound(player.getLocation(), Objects.requireNonNull(XSound.ENTITY_HORSE_ARMOR.parseSound()), 1f, 1f);
            } else if (item.isSimilar(spectators.getSettingsItem())) {
                sunkenPlayer.sendMessage(MsgType.DANGER, XSound.BLOCK_NOTE_BLOCK_BASS, new TextComponent("This feature is coming soon..."));
            } else if (item.isSimilar(spectators.getLobbyItem())) {
                sunkenPlayer.sendMessage(MsgType.INFO, XSound.BLOCK_NOTE_BLOCK_PLING, new TextComponent("Returning to the lobby..."));
                sunkenPlayer.sendToServer("lobby");
            }
        }
    }

}