package games.sunken.anchor.game.teams;

import games.sunken.anchor.util.Color;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface Team {

    /*
     *
     *   This is meant to be implemented on a per-game
     *   basis through an abstract implementation of
     *   this interface, as most minigames will have
     *   to put their own spin on teams. This is just
     *   for organization & spectating.
     *
     */

    String getName();

    Color getColor();

    int getMaxPlayers();

    Set<UUID> getMembers();

    void addMember(Player player);

    void removeMember(Player player);

    void broadcastMessage(String message);

    List<Location> getSpawnPoints();

}