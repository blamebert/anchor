package games.sunken.anchor.perms;

import games.sunken.anchor.Anchor;
import org.bukkit.ChatColor;
import org.mongodb.morphia.annotations.*;

import java.util.ArrayList;
import java.util.List;

@Entity("ranks")
public class Rank {

    @Id
    private String _id;

    @Indexed(options = @IndexOptions(unique = true))
    private int priority; // Lower number = lower priority. MUST BE UNIQUE

    private String prefix;
    private String suffix;
    private ChatColor chatColor;

    private List<String> permissions;

    private boolean defaultRank = false;

    private List<String> inherits;

    public Rank() {}

    public Rank(String name, int priority) {
        this._id = name;
        this.priority = priority;
        this.prefix = ChatColor.GRAY + "";
        this.suffix = "";
        this.chatColor = ChatColor.GRAY;
        this.permissions = new ArrayList<>();
        this.inherits = new ArrayList<>();
    }

    public String getName() {
        return _id;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public ChatColor getChatColor() {
        return chatColor;
    }

    public void setChatColor(ChatColor chatColor) {
        this.chatColor = chatColor;
    }

    public boolean isDefaultRank() {
        return defaultRank;
    }

    public void setDefaultRank(boolean defaultRank) {
        this.defaultRank = defaultRank;
    }

    public List<String> getPermissions() {
        if (permissions == null) this.permissions = new ArrayList<>();

        return permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }

    public void setInherits(List<String> inherits) {
        this.inherits = inherits;
    }

    public List<Rank> getInherits() {
        List<Rank> in = new ArrayList<>();

        if (inherits != null && !inherits.isEmpty()) {
            inherits.stream()
                    .filter(inheritance -> Anchor.getPlugin(Anchor.class).getRankManager().getRank(inheritance) != null)
                    .forEach(inheritance -> in.add(Anchor.getPlugin(Anchor.class).getRankManager().getRank(inheritance)));
        }

        return in;
    }

    public List<String> getInheritsStrings() {
        return this.inherits;
    }

    public void addInheritance(Rank rank) {
        if (rank.equals(this)) throw new IllegalArgumentException("A rank can't inherit itself!");
        if (inherits == null) this.inherits = new ArrayList<>();

        inherits.add(rank.getName());
    }

    public void removeInheritance(Rank rank) {
        if (rank.equals(this)) throw new IllegalArgumentException("A rank can't inherit itself!");
        if (inherits == null) this.inherits = new ArrayList<>();

        inherits.remove(rank.getName());
    }

}