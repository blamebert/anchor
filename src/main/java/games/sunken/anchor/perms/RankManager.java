package games.sunken.anchor.perms;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import games.sunken.anchor.Anchor;
import games.sunken.anchor.database.Mongo;
import games.sunken.anchor.database.redis.Redis;
import org.bukkit.Bukkit;
import redis.clients.jedis.Jedis;

import java.util.HashSet;
import java.util.Set;

public class RankManager {

    private Set<Rank> ranks;

    public RankManager() {
        this.ranks = new HashSet<>();

        this.reload();
    }

    public void reload() {
        ranks.clear();

        Bukkit.getScheduler().runTaskAsynchronously(Anchor.getPlugin(Anchor.class), () -> {
            DBCursor cursor = Mongo.getClient().getDB("sunken").getCollection("ranks").find();

            while (cursor.hasNext()) {
                DBObject obj = cursor.next();

                ranks.add(Anchor.getPlugin(Anchor.class).getGSON().fromJson(obj.toString(), Rank.class));
            }
        });
    }

    public void deleteRank(Rank rank) {
        Bukkit.getScheduler().runTaskAsynchronously(Anchor.getPlugin(Anchor.class), () -> {
            BasicDBObject query = new BasicDBObject();
            query.put("_id", rank.getName());

            Mongo.getDatastore().getCollection(Rank.class).findAndRemove(query);
            ranks.remove(rank);

            if (Anchor.getPlugin(Anchor.class).isRedis()) {
                try (Jedis jedis = Redis.getJedisPool().getResource()) {
                    jedis.publish(Anchor.RANK_UPDATE_CHANNEL, Anchor.getPlugin(Anchor.class).getGSON().toJson(rank));
                }
            } else {
                reload();
            }
        });
    }

    public Rank createRank(String name, int priority) {
        if (getRank(name) != null)
            throw new IllegalArgumentException("A rank with name \"" + name + "\" already exists!");
        if (getRank(priority) != null)
            throw new IllegalArgumentException("A rank with priority \"" + priority + "\" already exists!");

        Rank rank = new Rank(name, priority);

        Bukkit.getScheduler().runTaskAsynchronously(Anchor.getPlugin(Anchor.class), () -> Mongo.getRankDAO().save(rank));
        ranks.add(rank);

        updateRank(rank);

        return rank;
    }

    public void updateRank(Rank rank) {
        Bukkit.getScheduler().runTaskAsynchronously(Anchor.getPlugin(Anchor.class), () -> {
            if (getRank(rank.getName()) != null) {
                Rank toUpdate = getRank(rank.getName());
                toUpdate.setPermissions(rank.getPermissions());
                toUpdate.setInherits(rank.getInheritsStrings());
                toUpdate.setChatColor(rank.getChatColor());
                toUpdate.setPrefix(rank.getPrefix());
                toUpdate.setSuffix(rank.getSuffix());
                toUpdate.setDefaultRank(rank.isDefaultRank());

                Mongo.getRankDAO().save(toUpdate);

                if (Anchor.getPlugin(Anchor.class).isRedis()) {
                    try (Jedis jedis = Redis.getJedisPool().getResource()) {
                        jedis.publish(Anchor.RANK_UPDATE_CHANNEL, Anchor.getPlugin(Anchor.class).getGSON().toJson(toUpdate));
                    }
                }
            } else {
                Mongo.getRankDAO().save(rank);

                if (Anchor.getPlugin(Anchor.class).isRedis()) {
                    try (Jedis jedis = Redis.getJedisPool().getResource()) {
                        jedis.publish(Anchor.RANK_UPDATE_CHANNEL, Anchor.getPlugin(Anchor.class).getGSON().toJson(rank));
                    }
                }
            }

            reload();
        });
    }

    public Set<Rank> getRanks() {
        return ranks;
    }

    public Rank getDefaultRank() {
        return ranks.stream().filter(Rank::isDefaultRank).findFirst().orElse(null);
    }

    public void setDefaultRank(Rank rank) {
        if (getDefaultRank() != null) getDefaultRank().setDefaultRank(false);

        rank.setDefaultRank(true);

        updateRank(rank);
    }

    public Rank getRank(String name) {
        return ranks.stream().filter(rank -> rank.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

    public Rank getRank(int priority) {
        return ranks.stream().filter(rank -> rank.getPriority() == priority).findFirst().orElse(null);
    }

    public Rank getLowestRank() {
        Rank lowest = null;

        for (Rank rank : getRanks()) {
            if (lowest == null || lowest.getPriority() > rank.getPriority()) lowest = rank;
        }

        return lowest;
    }

    public Rank getHighestRank() {
        Rank highest = null;

        for (Rank rank : getRanks()) {
            if (highest == null || highest.getPriority() < rank.getPriority()) highest = rank;
        }

        return highest;
    }

    public Rank getRankAbove(Rank rank) {
        if (rank.equals(getHighestRank())) return null;

        int priority = rank.getPriority();
        Rank higherRank;

        do {
            higherRank = getRank(priority++);
        } while (higherRank == null);

        return higherRank;
    }

    public Rank getRankBelow(Rank rank) {
        if (rank.equals(getLowestRank())) return null;

        int priority = rank.getPriority();
        Rank lowerRank;

        do {
            lowerRank = getRank(priority--);
        } while (lowerRank == null);

        return lowerRank;
    }

}