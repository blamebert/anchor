package games.sunken.anchor.perms;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class RankDAO extends BasicDAO<Rank, String> {

    public RankDAO(Class<Rank> entityClass, Datastore datastore) {
        super(entityClass, datastore);
    }

}