package games.sunken.anchor.perms;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.player.SunkenPlayer;
import org.bukkit.ChatColor;

import java.util.Set;
import java.util.UUID;

public class UserUpdateData {

    private String uuid;
    private String prefix;
    private String suffix;
    private ChatColor chatColor;
    private String rank;
    private Set<String> perms;

    public UserUpdateData(String uuid, String prefix, String suffix, ChatColor chatColor, String rank, Set<String> perms) {
        this.uuid = uuid;
        this.prefix = prefix;
        this.suffix = suffix;
        this.chatColor = chatColor;
        this.rank = rank;
        this.perms = perms;
    }

    public String getUUID() {
        return uuid;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public ChatColor getChatColor() {
        return chatColor;
    }

    public String getRank() {
        return rank;
    }

    public Set<String> getPerms() {
        return perms;
    }

    public void apply() {
        SunkenPlayer sunkenPlayer = Anchor.getPlugin(Anchor.class).getPlayerManager().getPlayer(UUID.fromString(uuid));

        if (sunkenPlayer != null) {
            Rank newRank = Anchor.getPlugin(Anchor.class).getRankManager().getRank(this.rank);
            Rank oldRank = sunkenPlayer.getRank();
            String newPrefix = (sunkenPlayer.getPrefix() == null || sunkenPlayer.getPrefix().equals(oldRank.getPrefix()) ? null : sunkenPlayer.getPrefix());
            String newSuffix = (sunkenPlayer.getSuffix() == null || sunkenPlayer.getSuffix().equals(oldRank.getSuffix()) ? null : sunkenPlayer.getSuffix());
            ChatColor newChatColor = (sunkenPlayer.getChatColor() == null || sunkenPlayer.getChatColor().equals(oldRank.getChatColor()) ? null : sunkenPlayer.getChatColor());

            sunkenPlayer.setPrefix(newPrefix);
            sunkenPlayer.setSuffix(newSuffix);
            sunkenPlayer.setChatColor(newChatColor);
            sunkenPlayer.setRank(newRank);
            sunkenPlayer.setPlayerPermissions(this.perms);
            sunkenPlayer.recalculatePermissions(false);
        }
    }

}