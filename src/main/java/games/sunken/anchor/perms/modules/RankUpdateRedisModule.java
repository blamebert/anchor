package games.sunken.anchor.perms.modules;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.database.Mongo;
import games.sunken.anchor.database.redis.RedisModule;
import games.sunken.anchor.perms.Rank;
import games.sunken.anchor.player.SunkenPlayer;
import org.bukkit.ChatColor;

import javax.annotation.Nonnull;
import java.util.function.BiConsumer;

public class RankUpdateRedisModule implements RedisModule {

    @Nonnull
    @Override
    public String getName() {
        return "Rank Update Receiver";
    }

    @Nonnull
    @Override
    public String getChannel() {
        return Anchor.RANK_UPDATE_CHANNEL;
    }

    @Override
    public boolean isDaemonThread() {
        return true;
    }

    @Override
    public BiConsumer<String, String> onMessage() {
        return (channel, message) -> {
            Rank data = Anchor.getPlugin(Anchor.class).getGSON().fromJson(message, Rank.class);

            Anchor.getPlugin(Anchor.class).getRankManager().reload();

            for (SunkenPlayer player : Anchor.getPlugin(Anchor.class).getPlayerManager().getOnlinePlayers()) {
                if (player.getRank() != null) {
                    if (player.getRank().getName().equals(data.getName())) {
                        Rank oldRank = player.getRank();
                        String newPrefix = (player.getPrefix() == null || player.getPrefix().equals(oldRank.getPrefix()) ? null : player.getPrefix());
                        String newSuffix = (player.getSuffix() == null || player.getSuffix().equals(oldRank.getSuffix()) ? null : player.getSuffix());
                        ChatColor newChatColor = (player.getChatColor() == null || player.getChatColor().equals(oldRank.getChatColor()) ? null : player.getChatColor());

                        player.setRank(data);
                        player.setPrefix(newPrefix);
                        player.setSuffix(newSuffix);
                        player.setChatColor(newChatColor);
                        player.recalculatePermissions(false);
                    } else if (player.getRank().getInherits().stream().filter(rank -> rank.getName().equals(data.getName())).findAny().orElse(null) != null) {
                        player.recalculatePermissions(true);
                    } else {
                        continue;
                    }

                    Mongo.getPlayerDAO().save(player); // onMessage is always in its own thread, so this is fine
                }
            }
        };
    }

    @Override
    public BiConsumer<String, Integer> onSubscribe() {
        return (s, integer) -> Anchor.getPlugin(Anchor.class).getLogger().info("*** Rank Synchronization Module Started! ***");
    }

    @Override
    public BiConsumer<String, Integer> onUnsubscribe() {
        return (s, integer) -> Anchor.getPlugin(Anchor.class).getLogger().info("*** Rank Synchronization Module Stopped! ***");
    }

    @Override
    public Runnable onDisable() {
        return null;
    }

}