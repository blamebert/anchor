package games.sunken.anchor.perms.modules;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.database.redis.RedisModule;
import games.sunken.anchor.perms.Rank;
import games.sunken.anchor.perms.UserUpdateData;

import javax.annotation.Nonnull;
import java.util.function.BiConsumer;

public class UserUpdateRedisModule implements RedisModule {

    @Nonnull
    @Override
    public String getName() {
        return "User Data Update Receiver";
    }

    @Nonnull
    @Override
    public String getChannel() {
        return Anchor.USER_UPDATE_CHANNEL;
    }

    @Override
    public boolean isDaemonThread() {
        return true;
    }

    @Override
    public BiConsumer<String, String> onMessage() {
        return (channel, message) -> {
            UserUpdateData data = Anchor.getPlugin(Anchor.class).getGSON().fromJson(message, UserUpdateData.class);

            if (data.getRank() != null && !data.getRank().isEmpty()) {
                Rank rank = Anchor.getPlugin(Anchor.class).getRankManager().getRank(data.getRank());

                if (rank == null) throw new IllegalArgumentException("Rank not found!");

                data.apply();
            }
        };
    }

    @Override
    public BiConsumer<String, Integer> onSubscribe() {
        return (s, integer) -> Anchor.getPlugin(Anchor.class).getLogger().info("*** User Data Synchronization Module Started! ***");
    }

    @Override
    public BiConsumer<String, Integer> onUnsubscribe() {
        return (s, integer) -> Anchor.getPlugin(Anchor.class).getLogger().info("*** User Data Synchronization Module Stopped! ***");
    }

    @Override
    public Runnable onDisable() {
        return null;
    }

}