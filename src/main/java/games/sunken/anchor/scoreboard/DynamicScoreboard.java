package games.sunken.anchor.scoreboard;

import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import games.sunken.anchor.util.packetwrapper.WrapperPlayServerScoreboardDisplayObjective;
import games.sunken.anchor.util.packetwrapper.WrapperPlayServerScoreboardObjective;
import games.sunken.anchor.util.packetwrapper.WrapperPlayServerScoreboardScore;
import games.sunken.anchor.util.packetwrapper.WrapperPlayServerScoreboardTeam;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DynamicScoreboard {

    private final Line[] lines = new Line[16];
    private final Player player;
    private boolean created = false;
    private String titleName;

    public DynamicScoreboard(Player player, String titleName) {
        this.player = player;
        this.titleName = titleName;
    }

    public DynamicScoreboard(Player player, String titleName, List<String> lineList) {
        this.player = player;
        this.titleName = titleName;

        this.setLines(lineList);
    }

    public void create() {
        if (this.created) return;

        this.createObjectivePacket(0, this.titleName).sendPacket(this.player);
        this.setObjectiveSlot().sendPacket(this.player);

        this.created = true;

        for (int i = 0; i < this.lines.length; i++) {
            if (this.lines[i] == null) continue;

            this.sendLine(i);
        }
    }

    public void destroy() {
        if (!this.created) {
            return;
        }

        this.createObjectivePacket(1, null).sendPacket(this.player);

        for (Line team : this.lines) {
            if (team != null) {
                team.removeTeam().sendPacket(this.player);
            }
        }

        this.created = false;
    }

    public boolean isActive() {
        return created && this.player.isOnline();
    }

    public void setObjectiveName(String name) {
        this.titleName = name;

        if (this.created) {
            this.createObjectivePacket(2, name).sendPacket(this.player);
        }
    }

    public void setLines(List<String> lineList) {
        for (int i = 0; i < this.lines.length; i++) {
            if (i < lineList.size()) {
                this.setLine(i, lineList.get(i));
            } else {
                this.removeLine(i);
            }
        }
    }

    public void setLine(int line, String value) {
        Line team = this.getOrCreateTeam(line);
        String old = team.getCurrentPlayer();

        if (ChatColor.stripColor(value).isEmpty() || ChatColor.stripColor(value).startsWith(" ")) value = ChatColor.values()[line] + value;

        team.setValue(value);

        if (!team.playerChanged && !team.prefixChanged && !team.suffixChanged) {
            return;
        }

        if (old != null && this.created) {
            this.removeLine(old).sendPacket(this.player);
        }

        this.sendLine(line);
    }

    public void removeAllLines() {
        for (int i = 0; i < this.lines.length; i++) {
            if (this.lines[i] != null) {
                this.removeLine(i);
            }
        }
    }

    public void removeLine(int line) {
        Line team = this.getOrCreateTeam(line);
        String old = team.getCurrentPlayer();

        if (old != null && this.created) {
            this.removeLine(old).sendPacket(this.player);
            team.removeTeam().sendPacket(this.player);
        }

        this.lines[line] = null;
    }

    public String getLine(int line) {
        if (line > 14) return null;
        if (line < 0) return null;

        return this.getOrCreateTeam(line).getValue();
    }

    public Line getTeam(int line) {
        if (line > 14) return null;
        if (line < 0) return null;

        return this.getOrCreateTeam(line);
    }

    private void sendLine(int line) {
        if (line > 14) return;
        if (line < 0) return;

        if (!this.created) return;

        Line val = this.getOrCreateTeam(line);

        for (WrapperPlayServerScoreboardTeam wrapperPlayServerScoreboardTeam : val.sendLine()) {
            wrapperPlayServerScoreboardTeam.sendPacket(this.player);
        }

        sendScore(val.getCurrentPlayer(), line).sendPacket(this.player);
        val.reset();
    }

    private Line getOrCreateTeam(int line) {
        if (lines[line] == null) {
            lines[line] = new Line("__fakeScore" + line);
        }

        return lines[line];
    }

    private WrapperPlayServerScoreboardObjective createObjectivePacket(int mode, String displayName) {
        WrapperPlayServerScoreboardObjective wrapperPlayServerScoreboardObjective = new WrapperPlayServerScoreboardObjective();

        //Name of objective
        wrapperPlayServerScoreboardObjective.setName(this.player.getName());

        //Mode
        //0 : Create Scoreboard
        //1 : Remove Scoreboard
        //2 : Update Display Text
        wrapperPlayServerScoreboardObjective.setMode(mode);

        if (mode == 0 || mode == 2) {
            wrapperPlayServerScoreboardObjective.setDisplayName(WrappedChatComponent.fromText(displayName));
            wrapperPlayServerScoreboardObjective.setHealthDisplay(WrapperPlayServerScoreboardObjective.HealthDisplay.INTEGER);
        }

        return wrapperPlayServerScoreboardObjective;
    }

    private WrapperPlayServerScoreboardDisplayObjective setObjectiveSlot() {
        return new WrapperPlayServerScoreboardDisplayObjective(1, this.player.getName());
    }

    private WrapperPlayServerScoreboardScore sendScore(String line, int score) {
        return new WrapperPlayServerScoreboardScore(line, player.getName(), score, EnumWrappers.ScoreboardAction.CHANGE);
    }

    private WrapperPlayServerScoreboardScore removeLine(String line) {
        return new WrapperPlayServerScoreboardScore(line, player.getName(), 0, EnumWrappers.ScoreboardAction.REMOVE);
    }

    public class Line {

        private final String name;
        private String prefix;
        private String suffix;
        private String currentPlayer;

        private boolean prefixChanged, suffixChanged, playerChanged = false;
        private boolean first = true;

        private Line(String name, String prefix, String suffix) {
            this.name = name;
            this.prefix = prefix;
            this.suffix = suffix;
        }

        private Line(String name) {
            this.name = name;
            this.prefix = "";
            this.suffix = "";
        }

        public String getName() {
            return this.name;
        }

        public String getPrefix() {
            return this.prefix;
        }

        public void setPrefix(String prefix) {
            if (this.prefix == null || !this.prefix.equals(prefix)) {
                this.prefixChanged = true;
            }
            this.prefix = prefix;
        }

        public String getSuffix() {
            return this.suffix;
        }

        public void setSuffix(String suffix) {
            if (this.suffix == null || !this.suffix.equals(suffix)) {
                this.suffixChanged = true;
            }
            this.suffix = suffix;
        }

        private WrapperPlayServerScoreboardTeam createPacket(int mode) {
            return new WrapperPlayServerScoreboardTeam(this.name, WrappedChatComponent.fromText(this.name), WrappedChatComponent.fromText(this.prefix), WrappedChatComponent.fromText(this.suffix), "always", null, "always", new ArrayList<>(), mode, 0);
        }

        public WrapperPlayServerScoreboardTeam createTeam() {
            return this.createPacket(0);
        }

        public WrapperPlayServerScoreboardTeam updateTeam() {
            return this.createPacket(2);
        }

        public WrapperPlayServerScoreboardTeam removeTeam() {
            this.first = true;

            return new WrapperPlayServerScoreboardTeam(this.name, null, null, null, null, null, null, null, 1, 0);
        }

        public void setPlayer(String name) {
            if (name.length() <= 14) {
                for (Line line : lines) {
                    if (line == null || line.getName().equals(this.getName())) {
                        continue;
                    }

                    if (line.getCurrentPlayer().equals(name)) {
                        name += "§f";
                        break;
                    }
                }
            }

            if (this.currentPlayer == null || !this.currentPlayer.equals(name)) {
                this.playerChanged = true;
            }

            this.currentPlayer = name;
        }

        public Iterable<WrapperPlayServerScoreboardTeam> sendLine() {
            List<WrapperPlayServerScoreboardTeam> packets = new ArrayList<>();

            if (this.first) {
                packets.add(this.createTeam());
            } else if (this.prefixChanged || this.suffixChanged) {
                packets.add(this.updateTeam());
            }

            if (this.first || this.playerChanged) {
                packets.add(this.changePlayer());
            }

            if (this.first) {
                this.first = false;
            }

            return packets;
        }

        public void reset() {
            this.prefixChanged = false;
            this.suffixChanged = false;
            this.playerChanged = false;
        }

        public WrapperPlayServerScoreboardTeam changePlayer() {
            return this.addOrRemovePlayer(3, currentPlayer);
        }

        public WrapperPlayServerScoreboardTeam addOrRemovePlayer(int mode, String playerName) {
            return new WrapperPlayServerScoreboardTeam(this.name, null, null, null, null, null, null, new ArrayList<>(Collections.singleton(playerName)), mode, 0);
        }

        public String getCurrentPlayer() {
            return this.currentPlayer;
        }

        public String getValue() {
            return this.prefix + this.currentPlayer + this.suffix;
        }

        public void setValue(String value) {
            if (value.length() <= 16) {
                this.setPrefix("");
                this.setSuffix("");
                this.setPlayer(value);
            } else if (value.length() <= 32) {
                this.setPrefix(value.substring(0, 16));
                this.setPlayer(value.substring(16));
                this.setSuffix("");
            } else if (value.length() <= 48) {
                this.setPrefix(value.substring(0, 16));
                this.setPlayer(value.substring(16, 32));
                this.setSuffix(value.substring(32));
            } else {
                throw new IllegalArgumentException("Value too long! Max 48 characters, value was " + value.length() + " !");
            }
        }
    }

}