package games.sunken.anchor.scoreboard.animation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FlashingString extends FrameAnimatedString {

    public enum FlashType {
        LOOP,
        BOOMERANG
    }

    private String context;
    private FlashType flashType;
    private String[] formats;

    public FlashingString(String context, FlashType flashType, String... formats) {
        this.context = context;
        this.flashType = flashType;
        this.formats = formats;

        if (flashType == FlashType.LOOP) {
            for (String format : formats) {
                addFrame(format + context);
            }
        } else if (flashType == FlashType.BOOMERANG) {
            List<String> f = new ArrayList<>(Arrays.asList(formats));

            f.forEach(format -> addFrame(format + context));

            f.remove(0);
            f.remove(f.size() - 1);

            Collections.reverse(f);

            f.forEach(format -> addFrame(format + context));
        }
    }

    public String getContext() {
        return context;
    }

    public String[] getFormats() {
        return formats;
    }

}