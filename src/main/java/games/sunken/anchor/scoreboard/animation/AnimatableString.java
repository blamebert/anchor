package games.sunken.anchor.scoreboard.animation;

public interface AnimatableString {

    String current();

    String next();

    String previous();

}