package games.sunken.anchor.npc;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.npc.type.CreatureNPCType;
import games.sunken.anchor.npc.type.PlayerNPCType;
import games.sunken.anchor.util.Hologram;
import games.sunken.anchor.util.packetwrapper.WrapperPlayServerEntityDestroy;
import games.sunken.anchor.util.packetwrapper.WrapperPlayServerEntityHeadRotation;
import games.sunken.anchor.util.packetwrapper.WrapperPlayServerEntityLook;
import games.sunken.spigot.event.player.PlayerUseUnknownEntityEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Creature;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.*;
import java.util.function.Consumer;

public abstract class NPCType {

    private List<NPC> registeredNPCs = new ArrayList<>();

    public void despawnAll() {
        this.registeredNPCs.forEach(NPC::despawn);
    }

    private EntityType entityType;

    private Scoreboard scoreboard;
    private Team team;

    // Constructor
    public NPCType(EntityType entityType) {
        if (this instanceof CreatureNPCType && !Creature.class.isAssignableFrom(entityType.getEntityClass())) {
            throw new IllegalArgumentException("Entity does not extend Creature!");
        } else if (this instanceof PlayerNPCType && entityType != EntityType.PLAYER) {
            throw new IllegalArgumentException("Only player entities can be passed as a PlayerNPCType!");
        }

        this.entityType = entityType;

        String teamName = ("cnpc_" + this.getClass().getName().replace("games.sunken.anchor", "")).length() > 16 ? ("cnpc_" + this.getClass().getName().replace("games.sunken.anchor", "")).substring(0, 16) : "cnpc_" + this.getClass().getName().replace("games.sunken.anchor", "");

        this.scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();
        this.team = scoreboard.getTeam(teamName) == null ? scoreboard.registerNewTeam(teamName) : scoreboard.getTeam(teamName);
        this.team.setNameTagVisibility(NameTagVisibility.NEVER);
    }

    public EntityType getEntityType() {
        return this.entityType;
    }

    public List<NPC> getRegisteredNPCs() {
        return this.registeredNPCs;
    }

    public Scoreboard getScoreboard() {
        return this.scoreboard;
    }

    public Team getTeam() {
        return this.team;
    }

    public void registerNPC(NPC nonPlayableCharacter) {
        this.getRegisteredNPCs().add(nonPlayableCharacter);
    }

    public void unregisterNPC(NPC nonPlayableCharacter) {
        this.getRegisteredNPCs().remove(nonPlayableCharacter);
        nonPlayableCharacter.despawn();
    }

    public enum NPCVisibility { ALWAYS, HIDDEN }

    public static abstract class NPC implements Listener {

        private int entityID;
        private boolean lookClose;
        private Location location;

        private HashMap<Player, NPCVisibility> playerNPCVisibilityHashMap;
        private NPCVisibility npcVisibility;

        private Hologram title;

        private Consumer<Player> onClick;

        public NPC(Location location, NPCVisibility npcVisibility, Consumer<Player> onClick, String... title) {
            Objects.requireNonNull(npcVisibility);

            this.entityID = new Random().nextInt(Integer.MAX_VALUE);

            this.location = location;

            this.playerNPCVisibilityHashMap = new HashMap<>();
            this.npcVisibility = npcVisibility;

            this.onClick = onClick;

            // Head height will need changing eventually as we implement other movement functions to NPCs, and other NPCs will want to override the head height.
            this.title = new Hologram(location.add(0, 1.62F, 0), title);

            Bukkit.getPluginManager().registerEvents(this, Anchor.getPlugin(Anchor.class));
        }

        public int getEntityID() {
            return this.entityID;
        }

        public boolean doesLookClose() {
            return this.lookClose;
        }

        public void setLookClose(boolean lookClose) {
            this.lookClose = lookClose;
        }

        public Location getLocation() {
            return this.location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }

        public void addPlayerVisibility(Player player, NPCVisibility npcVisibility) {
            Objects.requireNonNull(player);
            Objects.requireNonNull(npcVisibility);

            this.playerNPCVisibilityHashMap.put(player, npcVisibility);
        }

        public NPCVisibility getNPCVisibilityFor(Player player) {
            return this.playerNPCVisibilityHashMap.get(player);
        }

        public void removePlayerVisibility(Player player) {
            this.playerNPCVisibilityHashMap.remove(player);
        }

        public NPCVisibility getNPCVisibility() {
            return this.npcVisibility;
        }

        public void setNPCVisibility(NPCVisibility npcVisibility) {
            this.npcVisibility = npcVisibility;
        }

        public Hologram getTitle() {
            return this.title;
        }

        public void setTitle(Hologram title) {
            this.title = title;
        }

        public boolean canBeSeenBy(Player player) {
            return (this.npcVisibility == NPCVisibility.ALWAYS && this.playerNPCVisibilityHashMap.get(player) != NPCVisibility.HIDDEN) || this.playerNPCVisibilityHashMap.get(player) != NPCVisibility.ALWAYS;
        }

        public Consumer<Player> getClickFunction() {
            return onClick;
        }

        public void onClick(Player player) {
            this.onClick.accept(player);
        }

        public void despawn() {
            Bukkit.getOnlinePlayers().forEach(this::hide);

            this.title = null;
        }

        public void display(Player player) {
            this.title.display(player);
        }

        public byte getFixRotation(float yaw) {
            return (byte) ((int) (yaw * 256.0F / 360.0F));
        }

        public void hide(Player player) {
            new WrapperPlayServerEntityDestroy(new int[]{ this.entityID }).sendPacket(player);

            this.title.hide(player);
        }

        public void lookAt(Location lookAt) {
            location.setDirection(lookAt.subtract(location).toVector());

            for (Player player : Bukkit.getOnlinePlayers()) {
                if (lookAt instanceof Player && !player.canSee((Player) lookAt)) {
                    continue;
                }

                new WrapperPlayServerEntityHeadRotation(this.entityID, getFixRotation(location.getYaw())).sendPacket(player);
                new WrapperPlayServerEntityLook(this.entityID, getFixRotation(location.getYaw()), getFixRotation(location.getPitch()), true).sendPacket(player);
            }
        }

        public void spawn() {
            if (this.npcVisibility == NPCVisibility.ALWAYS) {
                Bukkit.getOnlinePlayers().stream().filter(player -> this.playerNPCVisibilityHashMap.get(player) != NPCVisibility.HIDDEN).forEach(this::display);
            } else {
                this.playerNPCVisibilityHashMap.forEach((player, npcVisibility1) -> {
                    if (this.canBeSeenBy(player)) {
                        this.display(player);
                    }
                });
            }
        }

        @EventHandler
        public void onPlayerUseUnknownEntity(PlayerUseUnknownEntityEvent event) {
            if (event.getEntityID() == getEntityID()) {
                this.onClick(event.getPlayer());
            }
        }

    }

}