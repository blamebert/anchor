package games.sunken.anchor.npc.type;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.*;
import com.mojang.authlib.GameProfile;
import games.sunken.anchor.Anchor;
import games.sunken.anchor.npc.NPCType;
import games.sunken.anchor.util.Reflection;
import games.sunken.anchor.util.XSound;
import games.sunken.anchor.util.packetwrapper.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import javax.annotation.Nullable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

public class PlayerNPCType extends NPCType {

    public PlayerNPCType() {
        super(EntityType.PLAYER);
    }

    public static class PlayerNPC extends NPC {

        private GameProfile gameProfile;
        private PlayerInfoData playerInfoData;

        private boolean sneaking = false;
        private boolean sprinting = false;
        private boolean blocking = false;

        private ItemStack helmet;
        private ItemStack chestplate;
        private ItemStack leggings;
        private ItemStack boots;
        private ItemStack heldItem;

        public PlayerNPC(Location location, NPCVisibility npcVisibility, UUID uuid, Consumer<Player> onClick, String... title) {
            super(location, npcVisibility, onClick, title);

            this.gameProfile = Anchor.getPlugin(Anchor.class).getSkinUtil().makeProfile(title[0], uuid, Anchor.getPlugin(Anchor.class).getSkinUtil().getGameProfile(uuid, title[0]));
            this.playerInfoData = new PlayerInfoData(WrappedGameProfile.fromHandle(this.gameProfile), 0, EnumWrappers.NativeGameMode.SURVIVAL, WrappedChatComponent.fromText(this.gameProfile.getName()));

            Anchor.getPlugin(Anchor.class).getNPCManager().getNPCType(EntityType.PLAYER).getTeam().addEntry(this.gameProfile.getName());
        }

        private WrapperPlayServerPlayerInfo constructPacketWrapper(EnumWrappers.PlayerInfoAction playerInfoAction) {
            return new WrapperPlayServerPlayerInfo(playerInfoAction, Collections.singletonList(this.playerInfoData));
        }

        public void display(Player player) {
            constructPacketWrapper(EnumWrappers.PlayerInfoAction.ADD_PLAYER).sendPacket(player);

            WrapperPlayServerNamedEntitySpawn wrapperPlayServerNamedEntitySpawn = new WrapperPlayServerNamedEntitySpawn();

            wrapperPlayServerNamedEntitySpawn.setEntityID(getEntityID());
            wrapperPlayServerNamedEntitySpawn.setPlayerUUID(this.gameProfile.getId());
            wrapperPlayServerNamedEntitySpawn.setX(getLocation().getX());
            wrapperPlayServerNamedEntitySpawn.setY(getLocation().getY());
            wrapperPlayServerNamedEntitySpawn.setZ(getLocation().getZ());
            wrapperPlayServerNamedEntitySpawn.setYaw(getLocation().getYaw());
            wrapperPlayServerNamedEntitySpawn.setPitch(getLocation().getPitch());

            WrappedDataWatcher wrappedDataWatcher = new WrappedDataWatcher();

            if (Reflection.VERSION_NUMBER > 189) {
                wrappedDataWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(3, WrappedDataWatcher.Registry.get(Boolean.class)), false);
                wrappedDataWatcher.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(13, WrappedDataWatcher.Registry.get(Byte.class)), (byte) 0xFF);
            } else {
                wrappedDataWatcher.setObject(3, (byte) 0);
                wrappedDataWatcher.setObject(10, (byte) 0xFF);
            }

            wrapperPlayServerNamedEntitySpawn.setMetadata(wrappedDataWatcher);
            wrapperPlayServerNamedEntitySpawn.sendPacket(player);

            new BukkitRunnable() {
                @Override
                public void run() {
                    constructPacketWrapper(EnumWrappers.PlayerInfoAction.REMOVE_PLAYER).sendPacket(player);
                }
            }.runTaskLater(Anchor.getPlugin(Anchor.class), 50);

            super.getTitle().display(player);
            super.getTitle().mountOn(super.getEntityID());
        }

        @Override
        public void hide(Player player) {
            constructPacketWrapper(EnumWrappers.PlayerInfoAction.REMOVE_PLAYER).sendPacket(player);

            super.hide(player);
        }

        public void sneak() {
            this.sneaking = true;

            WrappedDataWatcher w = new WrappedDataWatcher();
            w.setObject(0, (byte) 2);
            w.setObject(1, (short) 0);
            w.setObject(8, (byte) 0);

            WrapperPlayServerEntityMetadata wrapper = new WrapperPlayServerEntityMetadata();
            wrapper.getHandle().getIntegers().write(0, getEntityID());

            List<WrappedWatchableObject> watchableObjects = new ArrayList<>();
            watchableObjects.add(new WrappedWatchableObject(0, (byte) 2));
            watchableObjects.add(new WrappedWatchableObject(1, (short) 0));
            watchableObjects.add(new WrappedWatchableObject(8, (byte) 0));

            wrapper.getHandle().getWatchableCollectionModifier().write(0, watchableObjects);

            sendPacket(wrapper.getHandle(), null);
        }

        public void sprint() {
            this.sprinting = true;

            WrapperPlayServerEntityMetadata wrapper = new WrapperPlayServerEntityMetadata();
            wrapper.getHandle().getIntegers().write(0, getEntityID());

            List<WrappedWatchableObject> watchableObjects = new ArrayList<>();
            watchableObjects.add(new WrappedWatchableObject(0, (byte) 8));
            watchableObjects.add(new WrappedWatchableObject(1, (short) 0));
            watchableObjects.add(new WrappedWatchableObject(8, (byte) 0));

            wrapper.getHandle().getWatchableCollectionModifier().write(0, watchableObjects);

            sendPacket(wrapper.getHandle(), null);
        }

        public void block() {
            this.blocking = true;

            WrapperPlayServerEntityMetadata wrapper = new WrapperPlayServerEntityMetadata();
            wrapper.getHandle().getIntegers().write(0, getEntityID());

            List<WrappedWatchableObject> watchableObjects = new ArrayList<>();
            watchableObjects.add(new WrappedWatchableObject(0, (byte) 0x10));

            wrapper.getHandle().getWatchableCollectionModifier().write(0, watchableObjects);

            sendPacket(wrapper.getHandle(), null);
        }

        public void resetMovement() {
            this.blocking = false;
            this.sneaking = false;
            this.sprinting = false;

            WrapperPlayServerEntityMetadata wrapper = new WrapperPlayServerEntityMetadata();
            wrapper.getHandle().getIntegers().write(0, getEntityID());

            List<WrappedWatchableObject> watchableObjects = new ArrayList<>();
            watchableObjects.add(new WrappedWatchableObject(0, (byte) 0));
            watchableObjects.add(new WrappedWatchableObject(1, (short) 0));
            watchableObjects.add(new WrappedWatchableObject(8, (byte) 0));

            wrapper.getHandle().getWatchableCollectionModifier().write(0, watchableObjects);

            sendPacket(wrapper.getHandle(), null);
        }

        public void swingArm() {
            WrapperPlayServerAnimation wrapper = new WrapperPlayServerAnimation();
            wrapper.getHandle().getIntegers().write(0, getEntityID());
            wrapper.getHandle().getIntegers().write(1, 0);

            sendPacket(wrapper.getHandle(), null);
        }

        public void setItemInHand(ItemStack item) {
            this.heldItem = item;

            WrapperPlayServerEntityEquipment wrapper = new WrapperPlayServerEntityEquipment();
            wrapper.getHandle().getIntegers().write(0, getEntityID());
            wrapper.getHandle().getIntegers().write(1, 0);
            wrapper.getHandle().getItemModifier().write(0, item);

            sendPacket(wrapper.getHandle(), null);
        }

        public void setHelmet(ItemStack helmet) {
            this.helmet = helmet;

            WrapperPlayServerEntityEquipment wrapper = new WrapperPlayServerEntityEquipment();
            wrapper.getHandle().getIntegers().write(0, getEntityID());
            wrapper.getHandle().getIntegers().write(1, 4);
            wrapper.getHandle().getItemModifier().write(0, helmet);

            sendPacket(wrapper.getHandle(), null);
        }

        public void setChestplate(ItemStack chestplate) {
            this.chestplate = chestplate;

            WrapperPlayServerEntityEquipment wrapper = new WrapperPlayServerEntityEquipment();
            wrapper.getHandle().getIntegers().write(0, getEntityID());
            wrapper.getHandle().getIntegers().write(1, 3);
            wrapper.getHandle().getItemModifier().write(0, chestplate);

            sendPacket(wrapper.getHandle(), null);
        }

        public void setLeggings(ItemStack leggings) {
            this.leggings = leggings;

            WrapperPlayServerEntityEquipment wrapper = new WrapperPlayServerEntityEquipment();
            wrapper.getHandle().getIntegers().write(0, getEntityID());
            wrapper.getHandle().getIntegers().write(1, 2);
            wrapper.getHandle().getItemModifier().write(0, leggings);

            sendPacket(wrapper.getHandle(), null);
        }

        public void setBoots(ItemStack boots) {
            this.boots = boots;

            WrapperPlayServerEntityEquipment wrapper = new WrapperPlayServerEntityEquipment();
            wrapper.getHandle().getIntegers().write(0, getEntityID());
            wrapper.getHandle().getIntegers().write(1, 1);
            wrapper.getHandle().getItemModifier().write(0, boots);

            sendPacket(wrapper.getHandle(), null);
        }

        public void damageAnimation(Player player) {
            WrapperPlayServerAnimation wrapper = new WrapperPlayServerAnimation();
            wrapper.getHandle().getIntegers().write(0, getEntityID());
            wrapper.getHandle().getIntegers().write(1, 1);

            sendPacket(wrapper.getHandle(), player);

            if (player != null) {
                player.playSound(getLocation(), XSound.ENTITY_PLAYER_HURT.parseSound(), 1, 1);
            } else {
                for (Player p : Bukkit.getOnlinePlayers()) {
                    p.playSound(getLocation(), XSound.ENTITY_PLAYER_HURT.parseSound(), 1, 1);
                }
            }
        }

        // Teleport to location
        public void teleport(Location location) {
            this.setLocation(location);

            PacketContainer wrapper = new PacketContainer(PacketType.Play.Server.ENTITY_TELEPORT);
            wrapper.getIntegers().write(0, getEntityID());
            wrapper.getIntegers().write(1, (int) (this.getLocation().getX() * 32D));
            wrapper.getIntegers().write(2, (int) (this.getLocation().getY() * 32D));
            wrapper.getIntegers().write(3, (int) (this.getLocation().getZ() * 32D));
            wrapper.getBytes().write(0, (byte) (this.getLocation().getYaw() * 256F / 260F));
            wrapper.getBytes().write(0, (byte) (this.getLocation().getPitch() * 256F / 260F));
            
            sendPacket(wrapper, null);
        }
        
        public void move(Location location) {
            move(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
        }

        // Walk to location
        public void move(double x, double y, double z, float yaw, float pitch) {
            WrapperPlayServerRelEntityMoveLook wrapper = new WrapperPlayServerRelEntityMoveLook();
            wrapper.getHandle().getIntegers().write(0, getEntityID());
            wrapper.getHandle().getBytes().write(0, (byte) toFxdPnt(x));
            wrapper.getHandle().getBytes().write(1, (byte) toFxdPnt(y));
            wrapper.getHandle().getBytes().write(2, (byte) toFxdPnt(z));
            wrapper.getHandle().getBytes().write(3, (byte) toAngle(yaw));
            wrapper.getHandle().getBytes().write(4, (byte) toAngle(pitch));
            wrapper.getHandle().getBooleans().write(0, true);

            sendPacket(wrapper.getHandle(), null);

            this.getLocation().add(toFxdPnt(x) / 32D, toFxdPnt(y) / 32D, toFxdPnt(z) / 32D);
            this.getLocation().setYaw(yaw);
            this.getLocation().setPitch(pitch);
        }

        public void move(double x, double y, double z) {
            WrapperPlayServerRelEntityMoveLook wrapper = new WrapperPlayServerRelEntityMoveLook();
            wrapper.getHandle().getIntegers().write(0, getEntityID());
            wrapper.getHandle().getBytes().write(0, (byte) toFxdPnt(x));
            wrapper.getHandle().getBytes().write(1, (byte) toFxdPnt(y));
            wrapper.getHandle().getBytes().write(2, (byte) toFxdPnt(z));
            wrapper.getHandle().getBooleans().write(0, true);

            sendPacket(wrapper.getHandle(), null);

            this.getLocation().add(toFxdPnt(x) / 32D, toFxdPnt(y) / 32D, toFxdPnt(z) / 32D);
        }

        private int toFxdPnt(double value) {
            return (int) Math.floor(value * 32.0D);
        }

        private byte toAngle(float value) {
            return (byte) ((int) (value * 256.0F / 360.0F));
        }

        public boolean isSneaking() {
            return sneaking;
        }

        public boolean isSprinting() {
            return sprinting;
        }

        public boolean isBlocking() {
            return blocking;
        }

        public ItemStack getItemInHand() {
            return heldItem;
        }

        public ItemStack[] getArmorContents() {
            return new ItemStack[]{helmet, chestplate, leggings, boots};
        }

    }

    private static void sendPacket(PacketContainer wrapper, @Nullable Player player) {
        if (player != null) {
            try {
                ProtocolLibrary.getProtocolManager().sendServerPacket(player, wrapper);
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        } else {
            for (Player p : Bukkit.getOnlinePlayers()) {
                try {
                    ProtocolLibrary.getProtocolManager().sendServerPacket(p, wrapper);
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}