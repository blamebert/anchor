package games.sunken.anchor.npc.type;

import games.sunken.anchor.npc.NPCType;
import games.sunken.anchor.util.packetwrapper.WrapperPlayServerSpawnEntityLiving;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.Random;
import java.util.UUID;
import java.util.function.Consumer;

public class CreatureNPCType extends NPCType {

    public CreatureNPCType(EntityType entityType) {
        super(entityType);
    }

    public static class CreatureNPC extends NPC {

        private EntityType entityType;

        public CreatureNPC(EntityType entityType, Location location, NPCVisibility npcVisibility, Consumer<Player> onClick, String... title) {
            super(location, npcVisibility, onClick, title);

            this.entityType = entityType;
        }

        @Override
        public void display(Player player) {
            WrapperPlayServerSpawnEntityLiving wrapperPlayServerSpawnEntityLiving = new WrapperPlayServerSpawnEntityLiving();

            wrapperPlayServerSpawnEntityLiving.setEntityID(new Random().nextInt(Integer.MAX_VALUE));
            wrapperPlayServerSpawnEntityLiving.setUniqueId(UUID.randomUUID());
            wrapperPlayServerSpawnEntityLiving.setType(entityType);
            wrapperPlayServerSpawnEntityLiving.setX(getLocation().getX());
            wrapperPlayServerSpawnEntityLiving.setY(getLocation().getY());
            wrapperPlayServerSpawnEntityLiving.setZ(getLocation().getZ());
            wrapperPlayServerSpawnEntityLiving.setYaw(getLocation().getYaw());
            wrapperPlayServerSpawnEntityLiving.setPitch(getLocation().getPitch());

            wrapperPlayServerSpawnEntityLiving.sendPacket(player);

            super.getTitle().display(player);
            super.getTitle().mountOn(super.getEntityID());
        }

        @Override
        public void hide(Player player) {
            super.hide(player);
        }

    }

}
