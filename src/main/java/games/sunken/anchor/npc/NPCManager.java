package games.sunken.anchor.npc;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.npc.type.CreatureNPCType;
import games.sunken.anchor.npc.type.PlayerNPCType;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class NPCManager {

    private List<NPCType> npcTypes = new ArrayList<>();

    public NPCManager(Anchor anchor) {
        this.registerNPCTypes();

        new BukkitRunnable() {
            @Override
            public void run() {
                for (NPCType npcType : npcTypes) {
                    for (NPCType.NPC registeredNPC : npcType.getRegisteredNPCs()) {
                        Player closest = null;

                        for (Entity entity : registeredNPC.getLocation().getWorld().getNearbyEntities(registeredNPC.getLocation(), 5, 5, 5)) {
                            if (entity instanceof Player) {
                                if (closest != null) {
                                    if (closest.getEyeLocation().distance(entity.getLocation()) < ((Player) entity).getEyeLocation().distance(registeredNPC.getLocation())) {
                                        closest = (Player) entity;
                                    }
                                } else {
                                    closest = (Player) entity;
                                }
                            }
                        }

                        if (closest != null) {
                            registeredNPC.lookAt(closest.getEyeLocation());
                        }
                    }
                }
            }
        }.runTaskTimer(anchor, 0, 1);
    }

    public void addNPCType(NPCType npcType) {
        this.npcTypes.add(npcType);
    }

    public void removeNPCType(NPCType npcType) {
        this.npcTypes.remove(npcType);
    }

    public List<NPCType> getNPCTypes() {
        return this.npcTypes;
    }

    public NPCType getNPCType(EntityType entityType) {
        return getNPCTypes().stream().filter(npcType -> npcType.getEntityType() == entityType).findFirst().orElse(null);
    }

    public void registerNPCTypes() {
        this.addNPCType(new PlayerNPCType());

        for (EntityType entityType : EntityType.values()) {
            if (entityType.getEntityClass() != null && Creature.class.isAssignableFrom(entityType.getEntityClass())) {
                this.addNPCType(new CreatureNPCType(entityType));
            }
        }
    }

    public void despawnAllNPCs() {
        this.npcTypes.forEach(NPCType::despawnAll);
    }

}