package games.sunken.anchor.cmd;

import com.google.common.collect.ImmutableList;
import games.sunken.anchor.util.Reflection;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.SimpleCommandMap;

import java.lang.reflect.InvocationTargetException;

public class CommandManager {

    private SimpleCommandMap getCommandManager() {
        try {
            Object craftServer = Reflection.getCraftClass("CraftServer").cast(Bukkit.getServer());
            return (SimpleCommandMap) craftServer.getClass().getMethod("getCommandMap").invoke(craftServer);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }

        return null;
    }

    public ImmutableList<Command> searchCommands(String query) {
        ImmutableList.Builder<Command> results = new ImmutableList.Builder<>();

        for (Command command : getCommandManager().getCommands()) {
            if (command.getName().toLowerCase().startsWith(query.toLowerCase())) {
                results.add(command);
            } else {
                command.getDescription();

                if (command.getDescription().toLowerCase().contains(query.toLowerCase())) {
                    results.add(command);
                } else {
                    for (String alias : command.getAliases()) {
                        if (alias.toLowerCase().startsWith(query.toLowerCase())) {
                            results.add(command);
                        }
                    }
                }
            }
        }

        return results.build();
    }

    public ImmutableList<SunkenCommand> getCommands() {
        ImmutableList.Builder<SunkenCommand> temp = new ImmutableList.Builder<>();

        for (Command command : getCommandManager().getCommands()) {
            if ((command instanceof SunkenCommand)) {
                temp.add((SunkenCommand) command);
            }
        }

        return temp.build();
    }

    public SunkenCommand getCommandByLabel(String label) {
        Validate.notNull(label);
        Command cmd = getCommandManager().getCommand(label);
        Validate.isTrue((cmd instanceof SunkenCommand), "This command isn't a SunkenCommand");

        return (SunkenCommand) cmd;
    }

    public void registerCommand(SunkenCommand command) {
        getCommandManager().register(command.getLabel().toLowerCase(), command);
    }

}