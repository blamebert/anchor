package games.sunken.anchor.cmd;

import org.apache.commons.lang3.Validate;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public abstract class CommandArgument<T> {

    private int id;
    private String label;
    private boolean required;

    public CommandArgument(String label, boolean required) {
        Validate.notNull(label);
        this.id = 0;
        this.label = label;
        this.required = required;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getTabCompletionResults(CommandSender sender, String lastWord) {
        List<String> results = new ArrayList<>();

        if ((sender instanceof Player)) {
            Player pl = (Player) sender;

            for (Player target : Bukkit.getOnlinePlayers()) {
                if (pl.canSee(target) && target.getDisplayName().toLowerCase().startsWith(lastWord.toLowerCase())) {
                    results.add(ChatColor.stripColor(target.getDisplayName()));
                }
            }
        } else {
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (player.getName().toLowerCase().startsWith(lastWord.toLowerCase())) {
                    results.add(player.getName());
                }
            }
        }

        return results;
    }


    public T getValue(String input) {
        return parse(input);
    }

    protected abstract T parse(String input);

    public String getLabel() {
        return label;
    }

    public boolean isRequired() {
        return required;
    }

}