package games.sunken.anchor.cmd.args;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.chat.channel.ChannelManager;
import games.sunken.anchor.chat.channel.ChatChannel;
import games.sunken.anchor.cmd.CommandArgument;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

public class ChannelArgument extends CommandArgument {

    private ChannelManager channelManager;

    public ChannelArgument(String label, boolean required) {
        super(label, required);

        this.channelManager = Anchor.getPlugin(Anchor.class).getChannelManager();
    }

    @Override
    public List<String> getTabCompletionResults(CommandSender sender, String lastWord) {
        List<String> suggestions = new ArrayList<>();

        for (ChatChannel chatChannel : channelManager.getChannels()) {
            if (chatChannel.getName().toLowerCase().startsWith(lastWord.toLowerCase())) {
                suggestions.add(chatChannel.getName());
            }
        }

        return suggestions;
    }

    @Override
    protected ChatChannel parse(String input) {
        ChatChannel chatChannel = channelManager.getByName(input);

        if (chatChannel == null) {
            chatChannel = channelManager.getByShortcut(input);

            if (chatChannel == null) {
                throw new IllegalArgumentException("Invalid chat channel!");
            }
        }

        return chatChannel;
    }
}
