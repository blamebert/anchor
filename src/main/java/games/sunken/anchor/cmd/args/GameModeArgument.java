package games.sunken.anchor.cmd.args;

import games.sunken.anchor.cmd.CommandArgument;
import org.apache.commons.lang.math.NumberUtils;
import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

public class GameModeArgument extends CommandArgument {

    public GameModeArgument(String label, boolean required) {
        super(label, required);
    }

    @Override
    public List<String> getTabCompletionResults(CommandSender sender, String lastWord) {
        List<String> strings = new ArrayList<>();

        for (GameMode gameMode : GameMode.values()) {
            if (gameMode.name().toLowerCase().startsWith(lastWord.toLowerCase())) {
                strings.add(gameMode.name());
            }
        }

        return strings;
    }

    @Override
    protected GameMode parse(String input) {
        if (NumberUtils.isNumber(input)) {
            switch (Integer.parseInt(input)) {
                case 0:
                    return GameMode.SURVIVAL;
                case 1:
                    return GameMode.CREATIVE;
                case 2:
                    return GameMode.ADVENTURE;
                case 3:
                    return GameMode.SPECTATOR;
            }
        } else if (input.length() == 1) {
            switch (input.toLowerCase().charAt(0)) {
                case 's':
                    return GameMode.SURVIVAL;
                case 'c':
                    return GameMode.CREATIVE;
                case 'a':
                    return GameMode.ADVENTURE;
                case 'p':
                    return GameMode.SPECTATOR;
            }
        } else {
            try {
                return GameMode.valueOf(input.toUpperCase());
            } catch (Exception ignored) {
            }
        }

        throw new IllegalArgumentException("Invalid gamemode!");
    }
}
