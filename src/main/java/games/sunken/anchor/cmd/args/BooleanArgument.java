package games.sunken.anchor.cmd.args;

import games.sunken.anchor.cmd.CommandArgument;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.List;

public class BooleanArgument extends CommandArgument {

    public BooleanArgument(String label, boolean required) {
        super(label, required);
    }

    @Override
    public List<String> getTabCompletionResults(CommandSender sender, String lastWord) {
        return Arrays.asList("on", "off");
    }

    @Override
    protected Boolean parse(String input) {
        switch (input.toLowerCase()) {
            case "yes":
            case "true":
            case "on":
                return true;

            case "no":
            case "false":
            case "off":
                return false;
        }

        throw new IllegalArgumentException("Invalid arguments! Input must be true or false!");
    }

}