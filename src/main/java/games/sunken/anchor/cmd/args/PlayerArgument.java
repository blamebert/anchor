package games.sunken.anchor.cmd.args;

import games.sunken.anchor.cmd.CommandArgument;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PlayerArgument extends CommandArgument {

    private boolean exactMatch;

    public PlayerArgument(String label, boolean required, boolean exactMatch) {
        super(label, required);
        this.exactMatch = exactMatch;
    }

    @Override
    public List<String> getTabCompletionResults(CommandSender sender, String lastWord) {
        List<String> suggestions = new ArrayList<>();

        for (Player player : Bukkit.getOnlinePlayers()) {
            if ((sender instanceof Player)) {
                Player p = (Player) sender;

                if (!p.canSee(player)) {
                    continue;
                }
            }

            if (lastWord.startsWith("u:")) {
                String testId = lastWord.replace("u:", "");

                if (player.getUniqueId().toString().startsWith(testId)) {
                    suggestions.add("u:" + player.getUniqueId().toString());
                }

                continue;
            }

            boolean isExact = false;
            if (lastWord.startsWith("e:")) {
                lastWord = lastWord.replace("e:", "");
                isExact = true;
            }

            if (player.getName().toLowerCase().startsWith(lastWord.toLowerCase())) {
                suggestions.add((isExact ? "e:" : "") + player.getName());
            }
        }

        return suggestions;
    }

    public boolean isExactMatch() {
        return exactMatch;
    }

    @Override
    protected Player parse(String input) {
        Player player;

        if (input.startsWith("u:")) {
            try {
                input = input.replace("u:", "");
                UUID uuid = UUID.fromString(input);
                player = Bukkit.getPlayer(uuid);
            } catch (Exception ex) {
                throw new IllegalArgumentException("Invalid UUID!");
            }
        } else if (isExactMatch() || input.startsWith("e:")) {
            if (input.startsWith("e:")) {
                input = input.replace("e:", "");
            }

            player = Bukkit.getPlayerExact(input);
        } else {
            player = Bukkit.getPlayer(input);
        }

        if (player != null) {
            return player;
        } else {
            throw new IllegalArgumentException("Player not found!");
        }
    }
}
