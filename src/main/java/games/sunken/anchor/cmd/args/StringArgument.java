package games.sunken.anchor.cmd.args;

import games.sunken.anchor.cmd.CommandArgument;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;

public class StringArgument extends CommandArgument {

    private int minLength = 0;
    private int maxLength = 256;
    private boolean alphabetic = false;
    private boolean alphanumeric = false;

    public StringArgument(String label, boolean required) {
        super(label, required);
    }

    public StringArgument(String label, boolean required, int minLength, int maxLength, boolean alphabetic, boolean alphanumeric) {
        super(label, required);
        this.minLength = minLength;
        this.maxLength = maxLength;
        this.alphabetic = alphabetic;
        this.alphanumeric = alphanumeric;
    }

    public int getMinLength() {
        return minLength;
    }

    public int getMaxLength() {
        return maxLength;
    }

    public boolean isAlphabetic() {
        return alphabetic;
    }

    public boolean isAlphanumeric() {
        return alphanumeric;
    }

    @Override
    protected String parse(String input) {
        if (input.length() < getMinLength()) {
            throw new IllegalArgumentException(getLabel() + " is too short! Minimum length is " + ChatColor.WHITE + getMinLength() + ChatColor.RED + ".");
        }

        if (input.length() > getMaxLength()) {
            throw new IllegalArgumentException(getLabel() + " is too long1 Maximum length is " + ChatColor.WHITE + getMaxLength() + ChatColor.RED + ".");
        }

        if (isAlphabetic() && !StringUtils.isAlpha(input)) {
            throw new IllegalArgumentException(getLabel() + " can only contain letters.");
        }

        if (isAlphanumeric() && !StringUtils.isAlphanumeric(input)) {
            throw new IllegalArgumentException(getLabel() + " can only contain letters and numbers.");
        }

        return input;
    }
}
