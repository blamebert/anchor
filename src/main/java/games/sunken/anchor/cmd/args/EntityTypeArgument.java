package games.sunken.anchor.cmd.args;

import games.sunken.anchor.cmd.CommandArgument;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;

import java.util.ArrayList;
import java.util.List;

public class EntityTypeArgument extends CommandArgument {

    private boolean aliveOnly;

    public EntityTypeArgument(String label, boolean required, boolean aliveOnly) {
        super(label, required);
        this.aliveOnly = aliveOnly;
    }

    @Override
    public List<String> getTabCompletionResults(CommandSender sender, String lastWord) {
        List<String> suggestions = new ArrayList<>();

        for (EntityType entityType : EntityType.values()) {
            if (!entityType.isSpawnable()) continue;
            if (!entityType.isAlive() && isAliveOnly()) continue;

            suggestions.add(entityType.name());
        }

        return suggestions;
    }

    public boolean isAliveOnly() {
        return aliveOnly;
    }

    @Override
    protected EntityType parse(String input) {
        try {
            EntityType entityType = EntityType.valueOf(input.toUpperCase());

            if (!entityType.isSpawnable()) {
                throw new IllegalArgumentException("Entity is not spawnable!");
            }

            if (isAliveOnly() && !entityType.isAlive()) {
                throw new IllegalArgumentException("Entity must be alive!");
            }

            return entityType;
        } catch (Exception ex) {
            throw new IllegalArgumentException("Unknown entity type!");
        }
    }
}
