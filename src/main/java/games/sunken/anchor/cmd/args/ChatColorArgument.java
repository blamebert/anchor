package games.sunken.anchor.cmd.args;

import games.sunken.anchor.cmd.CommandArgument;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

public class ChatColorArgument extends CommandArgument {

    public ChatColorArgument(String label, boolean required) {
        super(label, required);
    }

    @Override
    public List<String> getTabCompletionResults(CommandSender sender, String lastWord) {
        List<String> strings = new ArrayList<>();

        for (ChatColor chatColor : ChatColor.values()) {
            if (chatColor.name().toLowerCase().startsWith(lastWord.toLowerCase())) {
                strings.add(chatColor.name());
            }
        }

        return strings;
    }

    @Override
    protected ChatColor parse(String input) {
        try {
            return ChatColor.valueOf(input.toUpperCase());
        } catch (Exception ignored) {
        }

        throw new IllegalArgumentException("Invalid chat color!");
    }
}
