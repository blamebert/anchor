package games.sunken.anchor.cmd.args;

import games.sunken.anchor.cmd.CommandArgument;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.Collections;
import java.util.List;

public class DoubleArgument extends CommandArgument {

    private double minimum = Double.MIN_VALUE;
    private double maximum = Double.MAX_VALUE;

    public DoubleArgument(String label, boolean required) {
        super(label, required);
    }

    public DoubleArgument(String label, boolean required, double minimum, double maximum) {
        super(label, required);
        this.minimum = minimum;
        this.maximum = maximum;
    }

    public double getMinimum() {
        return minimum;
    }

    public double getMaximum() {
        return maximum;
    }

    @Override
    public List<String> getTabCompletionResults(CommandSender sender, String lastWord) {
        return Collections.singletonList("0.0");
    }

    @Override
    protected Double parse(String input) {
        try {
            double value = Double.parseDouble(input);

            if (value < getMinimum()) {
                throw new IllegalArgumentException(getLabel() + " can't be below " + ChatColor.WHITE + getMinimum() + ChatColor.RED + "!");
            }

            if (value > getMaximum()) {
                throw new IllegalArgumentException(getLabel() + " can't be above " + ChatColor.WHITE + getMaximum() + ChatColor.RED + "!");
            }

            return value;
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Invalid arguments! Input must be a decimal or a whole number!");
        }
    }
}
