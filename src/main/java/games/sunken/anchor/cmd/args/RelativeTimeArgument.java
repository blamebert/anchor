package games.sunken.anchor.cmd.args;

import games.sunken.anchor.chat.TimeFormatter;
import games.sunken.anchor.cmd.CommandArgument;
import org.bukkit.command.CommandSender;

import java.util.Collections;
import java.util.List;

public class RelativeTimeArgument extends CommandArgument {

    private boolean future;

    public RelativeTimeArgument(String label, boolean required, boolean future) {
        super(label, required);
        this.future = future;
    }

    @Override
    public List<String> getTabCompletionResults(CommandSender sender, String lastWord) {
        return Collections.singletonList("0m0s");
    }

    public boolean isFuture() {
        return future;
    }

    @Override
    protected Long parse(String input) {
        try {
            return TimeFormatter.parseDateDiff(input, isFuture());
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid time format!");
        }
    }
}
