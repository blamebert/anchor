package games.sunken.anchor.cmd.args;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.cmd.CommandArgument;
import games.sunken.anchor.perms.Rank;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

public class RankArgument extends CommandArgument {

    public RankArgument(String label, boolean required) {
        super(label, required);
    }

    @Override
    public List<String> getTabCompletionResults(CommandSender sender, String lastWord) {
        List<String> strings = new ArrayList<>();

        for (Rank rank : Anchor.getPlugin(Anchor.class).getRankManager().getRanks()) {
            if (rank.getName().toLowerCase().startsWith(lastWord.toLowerCase())) {
                strings.add(rank.getName());
            }
        }

        return strings;
    }

    @Override
    protected Rank parse(String input) {
        if (Anchor.getPlugin(Anchor.class).getRankManager().getRank(input) != null)
            return Anchor.getPlugin(Anchor.class).getRankManager().getRank(input);

        throw new IllegalArgumentException("That rank doesn't exist!");
    }
}
