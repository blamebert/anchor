package games.sunken.anchor.cmd.args;

import games.sunken.anchor.cmd.CommandArgument;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.List;

public class IntegerArgument extends CommandArgument {

    private int minimum = Integer.MIN_VALUE;
    private int maximum = Integer.MAX_VALUE;

    public IntegerArgument(String label, boolean required) {
        super(label, required);
    }

    public IntegerArgument(String label, boolean required, int minimum, int maximum) {
        super(label, required);
        this.minimum = minimum;
        this.maximum = maximum;
    }

    public int getMinimum() {
        return minimum;
    }

    public int getMaximum() {
        return maximum;
    }

    @Override
    public List<String> getTabCompletionResults(CommandSender sender, String lastWord) {
        return Arrays.asList("0");
    }

    @Override
    protected Integer parse(String input) {
        try {
            int value = Integer.parseInt(input);

            if (value < getMinimum()) {
                throw new IllegalArgumentException(getLabel() + " can't be below " + ChatColor.WHITE + getMinimum() + ChatColor.RED + "!");
            }

            if (value > getMaximum()) {
                throw new IllegalArgumentException(getLabel() + " can't be above " + ChatColor.WHITE + getMaximum() + ChatColor.RED + "!");
            }

            return value;
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Invalid arguments! Input must be a whole number!");
        }
    }
}
