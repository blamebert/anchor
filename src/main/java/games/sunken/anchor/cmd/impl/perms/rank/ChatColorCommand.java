package games.sunken.anchor.cmd.impl.perms.rank;

import com.google.common.collect.ImmutableMap;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.cmd.AllowedSenderType;
import games.sunken.anchor.cmd.SunkenCommand;
import games.sunken.anchor.cmd.args.ChatColorArgument;
import games.sunken.anchor.cmd.args.RankArgument;
import games.sunken.anchor.perms.Rank;
import games.sunken.anchor.perms.RankManager;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.Arrays;

public class ChatColorCommand extends SunkenCommand {

    private RankManager rankManager;

    public ChatColorCommand(RankManager rankManager) {
        super("chatcolor", "Manage a rank's chat color", "sunken.rank.chatcolor", AllowedSenderType.PLAYER, new ArrayList<>());

        this.rankManager = rankManager;

        this.registerSubCommands(Arrays.asList(
                new SetCommand(),
                new DeleteCommand()
        ));
    }

    @Override
    protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
        this.sendUsage(sender);
    }

    protected class SetCommand extends SunkenCommand {

        public SetCommand() {
            super("set", "Change a rank's chat color", "sunken.rank.chatcolor.set", AllowedSenderType.PLAYER, new ArrayList<>());

            this.registerAlias("change");

            this.registerArgument(new RankArgument("rank", true));
            this.registerArgument(new ChatColorArgument("chatcolor", true));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            Rank rank = (Rank) args.get("rank");
            ChatColor chatColor = (ChatColor) args.get("chatcolor");

            rank.setChatColor(chatColor);
            rankManager.updateRank(rank);

            sender.sendMessage(MsgType.SUCCESS.format("Rank chat color set to <p>!", chatColor + chatColor.name()));
        }

    }

    protected class DeleteCommand extends SunkenCommand {

        public DeleteCommand() {
            super("delete", "Delete a rank's chat color", "sunken.rank.chatcolor.delete", AllowedSenderType.PLAYER, new ArrayList<>());

            this.registerAlias("del");
            this.registerAlias("remove");
            this.registerAlias("reset");

            this.registerArgument(new RankArgument("rank", true));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            Rank rank = (Rank) args.get("rank");

            rank.setChatColor(ChatColor.GRAY);
            rankManager.updateRank(rank);

            sender.sendMessage(MsgType.SUCCESS.format("Rank chat color reset!"));
        }

    }

}