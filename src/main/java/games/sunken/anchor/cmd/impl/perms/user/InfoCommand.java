package games.sunken.anchor.cmd.impl.perms.user;

import com.google.common.collect.ImmutableMap;
import games.sunken.anchor.Anchor;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.cmd.AllowedSenderType;
import games.sunken.anchor.cmd.SunkenCommand;
import games.sunken.anchor.cmd.args.StringArgument;
import games.sunken.anchor.database.Mongo;
import games.sunken.anchor.player.PlayerManager;
import games.sunken.anchor.player.SunkenPlayer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class InfoCommand extends SunkenCommand {

    private PlayerManager playerManager;

    public InfoCommand(PlayerManager playerManager) {
        super("info", "Display information about a user", "sunken.user.info", AllowedSenderType.PLAYER, new ArrayList<>());

        this.registerArgument(new StringArgument("player", true));

        this.playerManager = playerManager;
    }

    @Override
    protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
        String playerName = (String) args.get("player");
        SunkenPlayer sunkenPlayer = null;

        if (Bukkit.getPlayer(playerName) != null && playerManager.getPlayer(Bukkit.getPlayer(playerName).getUniqueId()) != null) {
            sunkenPlayer = playerManager.getPlayer(Bukkit.getPlayer(playerName).getUniqueId());
        } else {
            if (playerManager.getPlayer(playerName) != null) {
                sunkenPlayer = playerManager.getPlayer(playerName);
            } else {
                sender.sendMessage(MsgType.WARNING.format("Loading data for <p>...", playerName));

                Bukkit.getScheduler().runTaskAsynchronously(Anchor.getPlugin(Anchor.class), () -> {
                    SunkenPlayer sp = Mongo.getPlayerDAO().findOne("username", playerName);

                    if (sp != null) {
                        playerManager.cachePlayer(sp, 60 * 5); // Cache for 5 minutes

                        sendInfo(sender, sp);
                    } else {
                        sender.sendMessage(MsgType.DANGER.format("Error loading data for <p>! Have they logged in before?", playerName));
                    }
                });
            }
        }

        if (sunkenPlayer != null) {
            sendInfo(sender, sunkenPlayer);
        }
    }

    private void sendInfo(CommandSender sender, SunkenPlayer sunkenPlayer) {
        String bullet = ChatColor.DARK_GRAY + "" + ChatColor.BOLD + " * " + ChatColor.DARK_AQUA;

        sender.sendMessage(" ");
        sender.sendMessage(MsgType.CENTERED.format(ChatColor.AQUA + "" + ChatColor.BOLD + "Showing information about <p>...", sunkenPlayer.getUsername()));
        sender.sendMessage(" ");
        sender.sendMessage(bullet + "Name: " + ChatColor.WHITE + sunkenPlayer.getUsername());
        sender.sendMessage(bullet + "Rank: " + ChatColor.WHITE + (sunkenPlayer.getRank() == null ? ChatColor.RED + "None" : sunkenPlayer.getRank().getName()));
        sender.sendMessage(bullet + "Prefix: " + sunkenPlayer.getPrefix());
        sender.sendMessage(bullet + "Suffix: " + sunkenPlayer.getSuffix());
        sender.sendMessage(bullet + "Chat Color: " + sunkenPlayer.getChatColor() + sunkenPlayer.getChatColor().name());

        TextComponent component = new TextComponent(bullet + "Permissions: " + ChatColor.AQUA + "" + ChatColor.UNDERLINE + "Click to view");

        component.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Click " + ChatColor.GRAY + "to view permissions").create()));
        component.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/user perms list " + sunkenPlayer.getUsername()));
        ((Player) sender).sendMessage(component);
        sender.sendMessage(" ");
    }

}