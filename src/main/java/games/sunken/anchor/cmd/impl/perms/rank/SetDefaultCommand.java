package games.sunken.anchor.cmd.impl.perms.rank;

import com.google.common.collect.ImmutableMap;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.cmd.AllowedSenderType;
import games.sunken.anchor.cmd.SunkenCommand;
import games.sunken.anchor.cmd.args.RankArgument;
import games.sunken.anchor.perms.Rank;
import games.sunken.anchor.perms.RankManager;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;

public class SetDefaultCommand extends SunkenCommand {

    private RankManager rankManager;

    public SetDefaultCommand(RankManager rankManager) {
        super("default", "Sets a rank as the default rank to be applied to new users", "sunken.rank.setdefault", AllowedSenderType.PLAYER, new ArrayList<>());

        this.registerAlias("setdefault");

        this.registerArgument(new RankArgument("rank", true));

        this.rankManager = rankManager;
    }

    @Override
    protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
        Rank rank = (Rank) args.get("rank");

        rankManager.setDefaultRank(rank);
        sender.sendMessage(MsgType.SUCCESS.format("Rank <p> set as the default rank!", rank.getName()));
    }

}