package games.sunken.anchor.cmd.impl.perms.rank;

import com.google.common.collect.ImmutableMap;
import games.sunken.anchor.chat.ChatPaginator;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.cmd.AllowedSenderType;
import games.sunken.anchor.cmd.SunkenCommand;
import games.sunken.anchor.cmd.args.BooleanArgument;
import games.sunken.anchor.cmd.args.RankArgument;
import games.sunken.anchor.cmd.args.StringArgument;
import games.sunken.anchor.perms.Rank;
import games.sunken.anchor.perms.RankManager;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PermissionsCommand extends SunkenCommand {

    private RankManager rankManager;

    public PermissionsCommand(RankManager rankManager) {
        super("perms", "Manage rank permissions", "sunken.rank.perms", AllowedSenderType.PLAYER, new ArrayList<>());
        this.registerAlias("perm");
        this.registerAlias("permission");
        this.registerAlias("permissions");

        this.rankManager = rankManager;

        this.registerSubCommands(Arrays.asList(
                new SetCommand(),
                new DeleteCommand(),
                new ListCommand()
        ));
    }

    @Override
    protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
        this.sendUsage(sender);
    }

    protected class SetCommand extends SunkenCommand {

        public SetCommand() {
            super("set", "Set a rank's permission to true or false", "sunken.rank.perms.set", AllowedSenderType.PLAYER, new ArrayList<>());

            this.registerAlias("change");
            this.registerAlias("add");

            this.registerArgument(new RankArgument("rank", true));
            this.registerArgument(new StringArgument("perm", true));
            this.registerArgument(new BooleanArgument("value", false));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            Rank rank = (Rank) args.get("rank");
            String perm = (String) args.get("perm");
            boolean value = true;

            if (args.containsKey("value")) value = (boolean) args.get("value");

            if (perm.startsWith("-")) {
                sender.sendMessage(MsgType.DANGER.format("Permissions must be set using the original permission node (no \"-\")!"));
                return;
            }

            if (rank.getPermissions().contains("-" + perm)) {
                if (value) {
                    rank.getPermissions().remove("-" + perm);
                    rank.getPermissions().add(perm);
                }
            } else if (rank.getPermissions().contains(perm)) {
                if (!value) {
                    rank.getPermissions().remove(perm);
                    rank.getPermissions().add("-" + perm);
                }
            } else {
                rank.getPermissions().add((value ? perm : "-" + perm));
            }

            rankManager.updateRank(rank);

            sender.sendMessage(MsgType.SUCCESS.format("Rank permission <p> set to <p>!", perm, (value ? ChatColor.GREEN : ChatColor.RED) + "" + ChatColor.BOLD + value));
        }

    }

    protected class DeleteCommand extends SunkenCommand {

        public DeleteCommand() {
            super("delete", "Delete a rank's permission", "sunken.rank.perms.delete", AllowedSenderType.PLAYER, new ArrayList<>());
            this.registerAlias("del");
            this.registerAlias("remove");

            this.registerArgument(new RankArgument("rank", true));
            this.registerArgument(new StringArgument("perm", true));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            Rank rank = (Rank) args.get("rank");
            String perm = (String) args.get("perm");

            if (perm.startsWith("-")) {
                sender.sendMessage(MsgType.DANGER.format("Permissions must be removed using the original permission node (no \"-\")!"));
                return;
            }

            if (rank.getPermissions().contains(perm)) {
                rank.getPermissions().remove(perm);
            } else if (rank.getPermissions().contains("-" + perm)) {
                rank.getPermissions().remove("-" + perm);
            } else {
                sender.sendMessage(MsgType.DANGER.format("That rank doesn't have the permission <p>!", perm));
                return;
            }

            rankManager.updateRank(rank);

            sender.sendMessage(MsgType.SUCCESS.format("Rank permission <p> removed!", perm));
        }

    }

    protected class ListCommand extends SunkenCommand {

        public ListCommand() {
            super("list", "Show a list of a rank's permissions", "sunken.rank.perms.list", AllowedSenderType.PLAYER, new ArrayList<>());

            this.registerAlias("show");
            this.registerAlias("view");

            this.registerArgument(new RankArgument("rank", true));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            Rank rank = (Rank) args.get("rank");

            if (rank.getPermissions().isEmpty()) {
                sender.sendMessage(MsgType.DANGER.format("This rank currently doesn't have any permissions! Add some with <p>!", "/rank perm add <rank> <perm>"));
                return;
            }

            List<TextComponent> msgs = new ArrayList<>();

            sender.sendMessage(" ");
            sender.sendMessage(MsgType.CENTERED.format(ChatColor.AQUA + "" + ChatColor.BOLD + "Showing a list of all <p>'s permissions...", rank.getName()));
            sender.sendMessage(" ");

            for (String perm : rank.getPermissions()) {
                if (perm.startsWith("-")) {
                    msgs.add(new TextComponent(ChatColor.DARK_GRAY + "" + ChatColor.BOLD + " * " + ChatColor.RED + perm.substring(1)));
                } else {
                    msgs.add(new TextComponent(ChatColor.DARK_GRAY + "" + ChatColor.BOLD + " * " + ChatColor.WHITE + perm));
                }
            }

            new ChatPaginator((Player) sender, "/rank perm list " + rank.getName(), 7, msgs);
        }

    }

}