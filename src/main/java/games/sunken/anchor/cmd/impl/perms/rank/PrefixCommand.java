package games.sunken.anchor.cmd.impl.perms.rank;

import com.google.common.collect.ImmutableMap;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.cmd.AllowedSenderType;
import games.sunken.anchor.cmd.SunkenCommand;
import games.sunken.anchor.cmd.args.RankArgument;
import games.sunken.anchor.cmd.args.StringArgument;
import games.sunken.anchor.perms.Rank;
import games.sunken.anchor.perms.RankManager;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.Arrays;

public class PrefixCommand extends SunkenCommand {

    private RankManager rankManager;

    public PrefixCommand(RankManager rankManager) {
        super("prefix", "Manage a rank's prefix", "sunken.rank.prefix", AllowedSenderType.PLAYER, new ArrayList<>());

        this.rankManager = rankManager;

        this.registerSubCommands(Arrays.asList(
                new SetCommand(),
                new DeleteCommand()
        ));
    }

    @Override
    protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
        this.sendUsage(sender);
    }

    protected class SetCommand extends SunkenCommand {

        public SetCommand() {
            super("set", "Change a rank's prefix", "sunken.rank.prefix.set", AllowedSenderType.PLAYER, new ArrayList<>());

            this.registerAlias("change");

            this.registerArgument(new RankArgument("rank", true));
            this.setExtraMessageArgument(new StringArgument("prefix", true));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            Rank rank = (Rank) args.get("rank");
            String prefix = (String) args.get("prefix");

            rank.setPrefix(prefix);
            rankManager.updateRank(rank);

            sender.sendMessage(MsgType.SUCCESS.format("Rank prefix set to <p>!", ChatColor.translateAlternateColorCodes('&', prefix)));
        }

    }

    protected class DeleteCommand extends SunkenCommand {

        public DeleteCommand() {
            super("delete", "Delete a rank's prefix", "sunken.rank.prefix.delete", AllowedSenderType.PLAYER, new ArrayList<>());

            this.registerAlias("del");
            this.registerAlias("remove");
            this.registerAlias("reset");

            this.registerArgument(new RankArgument("rank", true));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            Rank rank = (Rank) args.get("rank");

            rank.setPrefix(ChatColor.GRAY + "");
            rankManager.updateRank(rank);

            sender.sendMessage(MsgType.SUCCESS.format("Rank prefix reset!"));
        }

    }

}