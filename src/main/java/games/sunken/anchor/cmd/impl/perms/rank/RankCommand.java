package games.sunken.anchor.cmd.impl.perms.rank;

import com.google.common.collect.ImmutableMap;
import games.sunken.anchor.cmd.AllowedSenderType;
import games.sunken.anchor.cmd.SunkenCommand;
import games.sunken.anchor.perms.RankManager;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.Arrays;

public class RankCommand extends SunkenCommand {

    private RankManager rankManager;

    public RankCommand(RankManager rankManager) {
        super("rank", "View & modify ranks", "sunken.rank", AllowedSenderType.PLAYER, new ArrayList<>());
        this.registerAlias("ranks");

        this.registerSubCommands(Arrays.asList(
                new InfoCommand(),
                new ListCommand(rankManager),
                new CreateCommand(rankManager),
                new DeleteCommand(rankManager),
                new SetDefaultCommand(rankManager),
                new PrefixCommand(rankManager),
                new SuffixCommand(rankManager),
                new ChatColorCommand(rankManager),
                new PermissionsCommand(rankManager),
                new InheritanceCommand(rankManager)
        ));

        this.rankManager = rankManager;
    }

    @Override
    protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
        this.sendUsage(sender);
    }

}