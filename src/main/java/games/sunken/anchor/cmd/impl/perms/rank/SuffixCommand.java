package games.sunken.anchor.cmd.impl.perms.rank;

import com.google.common.collect.ImmutableMap;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.cmd.AllowedSenderType;
import games.sunken.anchor.cmd.SunkenCommand;
import games.sunken.anchor.cmd.args.RankArgument;
import games.sunken.anchor.cmd.args.StringArgument;
import games.sunken.anchor.perms.Rank;
import games.sunken.anchor.perms.RankManager;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.Arrays;

public class SuffixCommand extends SunkenCommand {

    private RankManager rankManager;

    public SuffixCommand(RankManager rankManager) {
        super("suffix", "Manage a rank's suffix", "sunken.rank.suffix", AllowedSenderType.PLAYER, new ArrayList<>());

        this.rankManager = rankManager;

        this.registerSubCommands(Arrays.asList(
                new SetCommand(),
                new DeleteCommand()
        ));
    }

    @Override
    protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
        this.sendUsage(sender);
    }

    protected class SetCommand extends SunkenCommand {

        public SetCommand() {
            super("set", "Change a rank's suffix", "sunken.rank.suffix.set", AllowedSenderType.PLAYER, new ArrayList<>());

            this.registerAlias("change");

            this.registerArgument(new RankArgument("rank", true));
            this.setExtraMessageArgument(new StringArgument("suffix", true));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            Rank rank = (Rank) args.get("rank");
            String suffix = (String) args.get("suffix");

            rank.setSuffix(suffix);
            rankManager.updateRank(rank);

            sender.sendMessage(MsgType.SUCCESS.format("Rank suffix set to <p>!", ChatColor.translateAlternateColorCodes('&', suffix)));
        }

    }

    protected class DeleteCommand extends SunkenCommand {

        public DeleteCommand() {
            super("delete", "Delete a rank's suffix", "sunken.rank.suffix.delete", AllowedSenderType.PLAYER, new ArrayList<>());

            this.registerAlias("del");
            this.registerAlias("remove");
            this.registerAlias("reset");

            this.registerArgument(new RankArgument("rank", true));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            Rank rank = (Rank) args.get("rank");

            rank.setSuffix("");
            rankManager.updateRank(rank);

            sender.sendMessage(MsgType.SUCCESS.format("Rank suffix reset!"));
        }

    }

}