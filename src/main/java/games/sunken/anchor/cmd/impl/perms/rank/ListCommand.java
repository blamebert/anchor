package games.sunken.anchor.cmd.impl.perms.rank;

import com.google.common.collect.ImmutableMap;
import games.sunken.anchor.chat.ChatPaginator;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.cmd.AllowedSenderType;
import games.sunken.anchor.cmd.SunkenCommand;
import games.sunken.anchor.perms.Rank;
import games.sunken.anchor.perms.RankManager;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class ListCommand extends SunkenCommand {

    private RankManager rankManager;

    public ListCommand(RankManager rankManager) {
        super("list", "Show a list of all ranks", "sunken.rank.list", AllowedSenderType.PLAYER, new ArrayList<>());

        this.registerAlias("show");
        this.registerAlias("view");

        this.rankManager = rankManager;
    }

    @Override
    protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
        if (rankManager.getRanks().isEmpty()) {
            sender.sendMessage(MsgType.DANGER.format("There are no ranks registered at the moment! Add some with <p>!", "/rank create <name> <priority>"));
            return;
        }

        List<TextComponent> msgs = new ArrayList<>();

        sender.sendMessage(" ");
        sender.sendMessage(MsgType.CENTERED.format(ChatColor.AQUA + "" + ChatColor.BOLD + "Showing a list of all ranks..."));
        sender.sendMessage(" ");

        for (Rank rank : rankManager.getRanks()) {
            TextComponent component = new TextComponent(ChatColor.DARK_GRAY + "" + ChatColor.BOLD + " * " + ChatColor.WHITE + rank.getName());

            component.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Click " + ChatColor.GRAY + "to view rank info").create()));
            component.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/rank info " + rank.getName()));

            msgs.add(component);
        }

        new ChatPaginator((Player) sender, "/rank list", 7, msgs);
    }

}