package games.sunken.anchor.cmd.impl.perms.user;

import com.google.common.collect.ImmutableMap;
import games.sunken.anchor.Anchor;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.cmd.AllowedSenderType;
import games.sunken.anchor.cmd.SunkenCommand;
import games.sunken.anchor.cmd.args.RankArgument;
import games.sunken.anchor.cmd.args.StringArgument;
import games.sunken.anchor.database.Mongo;
import games.sunken.anchor.database.redis.Redis;
import games.sunken.anchor.perms.Rank;
import games.sunken.anchor.perms.UserUpdateData;
import games.sunken.anchor.player.PlayerManager;
import games.sunken.anchor.player.SunkenPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.Arrays;

public class RankCommand extends SunkenCommand {

    private PlayerManager playerManager;

    public RankCommand(PlayerManager playerManager) {
        super("rank", "Manage a user's rank", "sunken.user.rank", AllowedSenderType.PLAYER, new ArrayList<>());

        this.playerManager = playerManager;

        this.registerSubCommands(Arrays.asList(
                new SetCommand(),
                new PromoteCommand(),
                new DemoteCommand()
        ));
    }

    @Override
    protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
        this.sendUsage(sender);
    }

    protected class SetCommand extends SunkenCommand {

        public SetCommand() {
            super("set", "Change a user's rank", "sunken.user.rank.set", AllowedSenderType.PLAYER, new ArrayList<>());

            this.registerAlias("change");

            this.registerArgument(new StringArgument("player", true));
            this.registerArgument(new RankArgument("rank", true));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            String playerName = (String) args.get("player");
            Rank rank = (Rank) args.get("rank");
            SunkenPlayer sunkenPlayer = null;

            if (Bukkit.getPlayer(playerName) != null && playerManager.getPlayer(Bukkit.getPlayer(playerName).getUniqueId()) != null) {
                sunkenPlayer = playerManager.getPlayer(Bukkit.getPlayer(playerName).getUniqueId());
            } else {
                if (playerManager.getPlayer(playerName) != null) {
                    sunkenPlayer = playerManager.getPlayer(playerName);
                } else {
                    sender.sendMessage(MsgType.WARNING.format("Loading data for <p>...", playerName));

                    Bukkit.getScheduler().runTaskAsynchronously(Anchor.getPlugin(Anchor.class), () -> {
                        SunkenPlayer sp = Mongo.getPlayerDAO().findOne("username", playerName);

                        if (sp != null) {
                            playerManager.cachePlayer(sp, 60 * 5); // Cache for 5 minutes

                            setRank(sender, sp, rank);
                        } else {
                            sender.sendMessage(MsgType.DANGER.format("Error loading data for <p>! Have they logged in before?", playerName));
                        }
                    });
                }
            }

            if (sunkenPlayer != null) {
                setRank(sender, sunkenPlayer, rank);
            }
        }

    }

    protected class PromoteCommand extends SunkenCommand {

        public PromoteCommand() {
            super("promote", "Set a user's rank to the next highest rank", "sunken.user.rank.promote", AllowedSenderType.PLAYER, new ArrayList<>());

            this.registerArgument(new StringArgument("player", true));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            String playerName = (String) args.get("player");
            SunkenPlayer sunkenPlayer = null;

            if (Bukkit.getPlayer(playerName) != null && playerManager.getPlayer(Bukkit.getPlayer(playerName).getUniqueId()) != null) {
                sunkenPlayer = playerManager.getPlayer(Bukkit.getPlayer(playerName).getUniqueId());
            } else {
                if (playerManager.getPlayer(playerName) != null) {
                    sunkenPlayer = playerManager.getPlayer(playerName);
                } else {
                    sender.sendMessage(MsgType.WARNING.format("Loading data for <p>...", playerName));

                    Bukkit.getScheduler().runTaskAsynchronously(Anchor.getPlugin(Anchor.class), () -> {
                        SunkenPlayer sp = Mongo.getPlayerDAO().findOne("username", playerName);

                        if (sp != null) {
                            playerManager.cachePlayer(sp, 60 * 5); // Cache for 5 minutes

                            setRank(sender, sp, Anchor.getPlugin(Anchor.class).getRankManager().getRankAbove(sp.getRank()));
                        } else {
                            sender.sendMessage(MsgType.DANGER.format("Error loading data for <p>! Have they logged in before?", playerName));
                        }
                    });
                }
            }

            if (sunkenPlayer != null) {
                setRank(sender, sunkenPlayer, Anchor.getPlugin(Anchor.class).getRankManager().getRankAbove(sunkenPlayer.getRank()));
            }
        }

    }

    protected class DemoteCommand extends SunkenCommand {

        public DemoteCommand() {
            super("demote", "Set a user's rank to the next lowest rank", "sunken.user.rank.demote", AllowedSenderType.PLAYER, new ArrayList<>());

            this.registerArgument(new StringArgument("player", true));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            String playerName = (String) args.get("player");
            SunkenPlayer sunkenPlayer = null;

            if (Bukkit.getPlayer(playerName) != null && playerManager.getPlayer(Bukkit.getPlayer(playerName).getUniqueId()) != null) {
                sunkenPlayer = playerManager.getPlayer(Bukkit.getPlayer(playerName).getUniqueId());
            } else {
                if (playerManager.getPlayer(playerName) != null) {
                    sunkenPlayer = playerManager.getPlayer(playerName);
                } else {
                    sender.sendMessage(MsgType.WARNING.format("Loading data for <p>...", playerName));

                    Bukkit.getScheduler().runTaskAsynchronously(Anchor.getPlugin(Anchor.class), () -> {
                        SunkenPlayer sp = Mongo.getPlayerDAO().findOne("username", playerName);

                        if (sp != null) {
                            playerManager.cachePlayer(sp, 60 * 5); // Cache for 5 minutes

                            setRank(sender, sp, Anchor.getPlugin(Anchor.class).getRankManager().getRankBelow(sp.getRank()));
                        } else {
                            sender.sendMessage(MsgType.DANGER.format("Error loading data for <p>! Have they logged in before?", playerName));
                        }
                    });
                }
            }

            if (sunkenPlayer != null) {
                setRank(sender, sunkenPlayer, Anchor.getPlugin(Anchor.class).getRankManager().getRankBelow(sunkenPlayer.getRank()));
            }
        }

    }

    private void setRank(CommandSender sender, SunkenPlayer player, Rank rank) {
        if (rank == null) {
            sender.sendMessage(MsgType.DANGER.format("That rank does not exist!"));
            return;
        }

        Rank oldRank = player.getRank();
        String prefix = (player.getPrefix().equals(oldRank.getPrefix()) ? null : player.getPrefix());
        String suffix = (player.getSuffix().equals(oldRank.getSuffix()) ? null : player.getSuffix());
        ChatColor chatColor = (player.getChatColor().equals(oldRank.getChatColor()) ? null : player.getChatColor());

        UserUpdateData data = new UserUpdateData(player.getUUID().toString(), prefix, suffix, chatColor, rank.getName(), player.getPlayerPermissions());

        Bukkit.getScheduler().runTaskAsynchronously(Anchor.getPlugin(Anchor.class), () -> {
            if (Anchor.getPlugin(Anchor.class).isRedis()) {
                try (Jedis jedis = Redis.getJedisPool().getResource()) {
                    jedis.publish(Anchor.USER_UPDATE_CHANNEL, Anchor.getPlugin(Anchor.class).getGSON().toJson(data));
                }
            } else {
                data.apply();
            }

            Mongo.getPlayerDAO().save(player);

            sender.sendMessage(MsgType.SUCCESS.format("<p>'s rank has been set to <p>!", player.getUsername(), rank.getName()));
        });
    }

}