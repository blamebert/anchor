package games.sunken.anchor.cmd.impl.perms.user;

import com.google.common.collect.ImmutableMap;
import games.sunken.anchor.Anchor;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.cmd.AllowedSenderType;
import games.sunken.anchor.cmd.SunkenCommand;
import games.sunken.anchor.cmd.args.ChatColorArgument;
import games.sunken.anchor.cmd.args.StringArgument;
import games.sunken.anchor.database.Mongo;
import games.sunken.anchor.database.redis.Redis;
import games.sunken.anchor.perms.Rank;
import games.sunken.anchor.perms.UserUpdateData;
import games.sunken.anchor.player.PlayerManager;
import games.sunken.anchor.player.SunkenPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.Arrays;

public class ChatColorCommand extends SunkenCommand {

    private PlayerManager playerManager;

    public ChatColorCommand(PlayerManager playerManager) {
        super("chatcolor", "Manage a user's chat color", "sunken.user.chatcolor", AllowedSenderType.PLAYER, new ArrayList<>());

        this.playerManager = playerManager;

        this.registerSubCommands(Arrays.asList(
                new SetCommand(),
                new DeleteCommand()
        ));
    }

    @Override
    protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
        this.sendUsage(sender);
    }

    protected class SetCommand extends SunkenCommand {

        public SetCommand() {
            super("set", "Change a user's chat color", "sunken.user.chatcolor.set", AllowedSenderType.PLAYER, new ArrayList<>());

            this.registerAlias("change");

            this.registerArgument(new StringArgument("player", true));
            this.registerArgument(new ChatColorArgument("color", true));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            String playerName = (String) args.get("player");
            ChatColor color = (ChatColor) args.get("color");
            SunkenPlayer sunkenPlayer = null;

            if (Bukkit.getPlayer(playerName) != null && playerManager.getPlayer(Bukkit.getPlayer(playerName).getUniqueId()) != null) {
                sunkenPlayer = playerManager.getPlayer(Bukkit.getPlayer(playerName).getUniqueId());
            } else {
                if (playerManager.getPlayer(playerName) != null) {
                    sunkenPlayer = playerManager.getPlayer(playerName);
                } else {
                    sender.sendMessage(MsgType.WARNING.format("Loading data for <p>...", playerName));

                    Bukkit.getScheduler().runTaskAsynchronously(Anchor.getPlugin(Anchor.class), () -> {
                        SunkenPlayer sp = Mongo.getPlayerDAO().findOne("username", playerName);

                        if (sp != null) {
                            playerManager.cachePlayer(sp, 60 * 5); // Cache for 5 minutes

                            setChatColor(sender, sp, color);
                        } else {
                            sender.sendMessage(MsgType.DANGER.format("Error loading data for <p>! Have they logged in before?", playerName));
                        }
                    });
                }
            }

            if (sunkenPlayer != null) {
                setChatColor(sender, sunkenPlayer, color);
            }
        }

    }

    protected class DeleteCommand extends SunkenCommand {

        public DeleteCommand() {
            super("delete", "Delete a user's chat color", "sunken.user.chatcolor.delete", AllowedSenderType.PLAYER, new ArrayList<>());

            this.registerAlias("del");
            this.registerAlias("remove");
            this.registerAlias("reset");

            this.registerArgument(new StringArgument("player", true));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            String playerName = (String) args.get("player");
            SunkenPlayer sunkenPlayer = null;

            if (Bukkit.getPlayer(playerName) != null && playerManager.getPlayer(Bukkit.getPlayer(playerName).getUniqueId()) != null) {
                sunkenPlayer = playerManager.getPlayer(Bukkit.getPlayer(playerName).getUniqueId());
            } else {
                if (playerManager.getPlayer(playerName) != null) {
                    sunkenPlayer = playerManager.getPlayer(playerName);
                } else {
                    sender.sendMessage(MsgType.WARNING.format("Loading data for <p>...", playerName));

                    Bukkit.getScheduler().runTaskAsynchronously(Anchor.getPlugin(Anchor.class), () -> {
                        SunkenPlayer sp = Mongo.getPlayerDAO().findOne("username", playerName);

                        if (sp != null) {
                            playerManager.cachePlayer(sp, 60 * 5); // Cache for 5 minutes

                            setChatColor(sender, sp, null);
                        } else {
                            sender.sendMessage(MsgType.DANGER.format("Error loading data for <p>! Have they logged in before?", playerName));
                        }
                    });
                }
            }

            if (sunkenPlayer != null) {
                setChatColor(sender, sunkenPlayer, null);
            }
        }

    }

    private void setChatColor(CommandSender sender, SunkenPlayer player, ChatColor color) {
        Rank rank = player.getRank();
        String prefix = (player.getPrefix() == null || player.getPrefix().equals(rank.getPrefix()) ? null : player.getPrefix());
        String suffix = (player.getSuffix() == null || player.getSuffix().equals(rank.getSuffix()) ? null : player.getSuffix());

        UserUpdateData data = new UserUpdateData(player.getUUID().toString(), prefix, suffix, color, rank.getName(), player.getPlayerPermissions());

        Bukkit.getScheduler().runTaskAsynchronously(Anchor.getPlugin(Anchor.class), () -> {
            if (Anchor.getPlugin(Anchor.class).isRedis()) {
                try (Jedis jedis = Redis.getJedisPool().getResource()) {
                    jedis.publish(Anchor.USER_UPDATE_CHANNEL, Anchor.getPlugin(Anchor.class).getGSON().toJson(data));
                }
            } else {
                data.apply();
            }

            Mongo.getPlayerDAO().save(player);

            if (color == null) {
                sender.sendMessage(MsgType.SUCCESS.format("<p>'s chat color has been removed!", player.getUsername()));
            } else {
                sender.sendMessage(MsgType.SUCCESS.format("<p>'s chat color has been set to <p>!", player.getUsername(), color + color.name()));
            }
        });
    }

}