package games.sunken.anchor.cmd.impl;

import com.google.common.collect.ImmutableMap;
import games.sunken.anchor.Anchor;
import games.sunken.anchor.chat.channel.ChatChannel;
import games.sunken.anchor.cmd.AllowedSenderType;
import games.sunken.anchor.cmd.SunkenCommand;
import games.sunken.anchor.cmd.args.StringArgument;
import games.sunken.anchor.player.SunkenPlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class ChatShortcutCommand extends SunkenCommand {

    private ChatChannel chatChannel;

    public ChatShortcutCommand(ChatChannel chatChannel) {
        super(chatChannel.getName().toLowerCase(), "Send a chat message to " + chatChannel.getName(), "sunken.chat." + chatChannel.getName().toLowerCase().replaceAll(" ", "_"), AllowedSenderType.PLAYER, new ArrayList<>());
        this.chatChannel = chatChannel;

        registerAlias(chatChannel.getNickname().toLowerCase());

        setExtraMessageArgument(new StringArgument("message", true));
    }

    @Override
    protected void runCommand(CommandSender sender, ImmutableMap<String, Object> arguments) {
        Player player = (Player) sender;
        SunkenPlayer sunkenPlayer = Anchor.getPlugin(Anchor.class).getPlayerManager().getPlayer(player.getUniqueId());

        String msg = (String) arguments.get("message");
        chatChannel.sendMessage(sunkenPlayer, msg);
    }
}
