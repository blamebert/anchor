package games.sunken.anchor.cmd.impl.perms.rank;

import com.google.common.collect.ImmutableMap;
import games.sunken.anchor.Anchor;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.chat.Prompt;
import games.sunken.anchor.cmd.AllowedSenderType;
import games.sunken.anchor.cmd.SunkenCommand;
import games.sunken.anchor.cmd.args.RankArgument;
import games.sunken.anchor.perms.Rank;
import games.sunken.anchor.perms.RankManager;
import games.sunken.anchor.player.SunkenPlayer;
import games.sunken.anchor.util.XSound;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class DeleteCommand extends SunkenCommand {

    private RankManager rankManager;

    public DeleteCommand(RankManager rankManager) {
        super("delete", "Delete a rank", "sunken.rank.delete", AllowedSenderType.PLAYER, new ArrayList<>());

        this.registerAlias("del");
        this.registerAlias("remove");

        this.registerArgument(new RankArgument("rank", true));

        this.rankManager = rankManager;
    }

    @Override
    protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
        Rank rank = (Rank) args.get("rank");
        String name = rank.getName();

        SunkenPlayer sunkenPlayer = Anchor.getPlugin(Anchor.class).getPlayerManager().getPlayer(((Player) sender).getUniqueId());

        sunkenPlayer.setPrompt(new Prompt(MsgType.DANGER, ChatColor.RED + "" + ChatColor.BOLD + "Are you sure you want to delete this rank?", XSound.BLOCK_NOTE_BLOCK_BASS,
                ChatColor.RED + "This action will reset the ranks of all players who have this rank and cannot be undone!",
                "To CONFIRM rank deletion, please re-enter the name of this rank in chat.",
                "To CANCEL rank deletion, type \"cancel\" in chat!") {
            @Override
            public boolean onSubmit(Player player, String submittedText) {
                if (submittedText.equalsIgnoreCase(name)) {
                    try {
                        rankManager.deleteRank(rank);
                    } catch (IllegalArgumentException ex) {
                        sender.sendMessage(MsgType.DANGER.format(ex.getMessage()));
                    } catch (Exception ex) {
                        sender.sendMessage(MsgType.DANGER.format("An internal error occurred while attempting to delete a rank!"));
                        ex.printStackTrace();
                    }

                    sender.sendMessage(MsgType.SUCCESS.format("Rank <p> successfully deleted!", name));

                    return true;
                }

                return false;
            }

            @Override
            public void onCancel(Player player) {

            }
        });
    }

}