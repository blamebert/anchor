package games.sunken.anchor.cmd.impl.perms.rank;

import com.google.common.collect.ImmutableMap;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.cmd.AllowedSenderType;
import games.sunken.anchor.cmd.SunkenCommand;
import games.sunken.anchor.cmd.args.RankArgument;
import games.sunken.anchor.perms.Rank;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class InfoCommand extends SunkenCommand {

    public InfoCommand() {
        super("info", "Display information about a rank", "sunken.rank.info", AllowedSenderType.PLAYER, new ArrayList<>());

        this.registerArgument(new RankArgument("rank", true));
    }

    @Override
    protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
        Rank rank = (Rank) args.get("rank");
        String bullet = ChatColor.DARK_GRAY + "" + ChatColor.BOLD + " * " + ChatColor.DARK_AQUA;

        sender.sendMessage(" ");
        sender.sendMessage(MsgType.CENTERED.format(ChatColor.AQUA + "" + ChatColor.BOLD + "Showing information about <p>...", rank.getName()));
        sender.sendMessage(" ");
        sender.sendMessage(bullet + "Name: " + ChatColor.WHITE + rank.getName());
        sender.sendMessage(bullet + "Priority: " + ChatColor.WHITE + rank.getPriority());
        sender.sendMessage(bullet + "Default: " + (rank.isDefaultRank() ? ChatColor.GREEN + "Yes" : ChatColor.RED + "No"));
        sender.sendMessage(bullet + "Prefix: " + ChatColor.translateAlternateColorCodes('&', rank.getPrefix()));
        sender.sendMessage(bullet + "Suffix: " + ChatColor.translateAlternateColorCodes('&', rank.getSuffix()));
        sender.sendMessage(bullet + "Chat Color: " + rank.getChatColor() + rank.getChatColor().name());

        List<String> inherits = new ArrayList<>();
        if (rank.getInherits() != null && !rank.getInherits().isEmpty()) rank.getInherits().forEach(r -> inherits.add(r.getName()));

        sender.sendMessage(bullet + "Inheritances: " + (inherits.isEmpty() ? ChatColor.RED + "None" : ChatColor.WHITE + String.join(ChatColor.GRAY + ", " + ChatColor.WHITE, inherits)));

        TextComponent component = new TextComponent(bullet + "Permissions: " + ChatColor.AQUA + "" + ChatColor.UNDERLINE + "Click to view");

        component.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Click " + ChatColor.GRAY + "to view permissions").create()));
        component.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/rank perms list " + rank.getName()));
        ((Player) sender).sendMessage(component);
        sender.sendMessage(" ");
    }

}