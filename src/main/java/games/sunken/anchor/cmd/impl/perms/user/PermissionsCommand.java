package games.sunken.anchor.cmd.impl.perms.user;

import com.google.common.collect.ImmutableMap;
import games.sunken.anchor.Anchor;
import games.sunken.anchor.chat.ChatPaginator;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.cmd.AllowedSenderType;
import games.sunken.anchor.cmd.SunkenCommand;
import games.sunken.anchor.cmd.args.BooleanArgument;
import games.sunken.anchor.cmd.args.StringArgument;
import games.sunken.anchor.database.Mongo;
import games.sunken.anchor.database.redis.Redis;
import games.sunken.anchor.perms.Rank;
import games.sunken.anchor.perms.UserUpdateData;
import games.sunken.anchor.player.PlayerManager;
import games.sunken.anchor.player.SunkenPlayer;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import redis.clients.jedis.Jedis;

import java.util.*;

public class PermissionsCommand extends SunkenCommand {

    private PlayerManager playerManager;

    public PermissionsCommand(PlayerManager playerManager) {
        super("perms", "Manage user permissions", "sunken.user.perms", AllowedSenderType.PLAYER, new ArrayList<>());
        this.registerAlias("perm");
        this.registerAlias("permission");
        this.registerAlias("permissions");

        this.playerManager = playerManager;

        this.registerSubCommands(Arrays.asList(
                new SetCommand(),
                new DeleteCommand(),
                new ListCommand()
        ));
    }

    @Override
    protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
        this.sendUsage(sender);
    }

    protected class SetCommand extends SunkenCommand {

        public SetCommand() {
            super("set", "Set a users's permission to true or false", "sunken.user.perms.set", AllowedSenderType.PLAYER, new ArrayList<>());

            this.registerAlias("change");
            this.registerAlias("add");

            this.registerArgument(new StringArgument("player", true));
            this.registerArgument(new StringArgument("perm", true));
            this.registerArgument(new BooleanArgument("value", false));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            String playerName = (String) args.get("player");
            String perm = (String) args.get("perm");
            boolean value = (!args.containsKey("value") || (boolean) args.get("value"));

            SunkenPlayer sunkenPlayer = null;

            if (Bukkit.getPlayer(playerName) != null && playerManager.getPlayer(Bukkit.getPlayer(playerName).getUniqueId()) != null) {
                sunkenPlayer = playerManager.getPlayer(Bukkit.getPlayer(playerName).getUniqueId());
            } else {
                if (playerManager.getPlayer(playerName) != null) {
                    sunkenPlayer = playerManager.getPlayer(playerName);
                } else {
                    sender.sendMessage(MsgType.WARNING.format("Loading data for <p>...", playerName));

                    Bukkit.getScheduler().runTaskAsynchronously(Anchor.getPlugin(Anchor.class), () -> {
                        SunkenPlayer sp = Mongo.getPlayerDAO().findOne("username", playerName);

                        if (sp != null) {
                            playerManager.cachePlayer(sp, 60 * 5); // Cache for 5 minutes

                            updatePermission(sender, sp, perm, value, false);
                        } else {
                            sender.sendMessage(MsgType.DANGER.format("Error loading data for <p>! Have they logged in before?", playerName));
                        }
                    });
                }
            }

            if (sunkenPlayer != null) {
                updatePermission(sender, sunkenPlayer, perm, value, false);
            }
        }

    }

    protected class DeleteCommand extends SunkenCommand {

        public DeleteCommand() {
            super("delete", "Delete a rank's permission", "sunken.rank.perms.delete", AllowedSenderType.PLAYER, new ArrayList<>());
            this.registerAlias("del");
            this.registerAlias("remove");

            this.registerArgument(new StringArgument("player", true));
            this.registerArgument(new StringArgument("perm", true));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            String playerName = (String) args.get("player");
            String perm = (String) args.get("perm");

            SunkenPlayer sunkenPlayer = null;

            if (Bukkit.getPlayer(playerName) != null && playerManager.getPlayer(Bukkit.getPlayer(playerName).getUniqueId()) != null) {
                sunkenPlayer = playerManager.getPlayer(Bukkit.getPlayer(playerName).getUniqueId());
            } else {
                if (playerManager.getPlayer(playerName) != null) {
                    sunkenPlayer = playerManager.getPlayer(playerName);
                } else {
                    sender.sendMessage(MsgType.WARNING.format("Loading data for <p>...", playerName));

                    Bukkit.getScheduler().runTaskAsynchronously(Anchor.getPlugin(Anchor.class), () -> {
                        SunkenPlayer sp = Mongo.getPlayerDAO().findOne("username", playerName);

                        if (sp != null) {
                            playerManager.cachePlayer(sp, 60 * 5); // Cache for 5 minutes

                            updatePermission(sender, sp, perm, true, true);
                        } else {
                            sender.sendMessage(MsgType.DANGER.format("Error loading data for <p>! Have they logged in before?", playerName));
                        }
                    });
                }
            }

            if (sunkenPlayer != null) {
                updatePermission(sender, sunkenPlayer, perm, true, true);
            }
        }

    }

    protected class ListCommand extends SunkenCommand {

        public ListCommand() {
            super("list", "Show a list of a user's permissions", "sunken.user.perms.list", AllowedSenderType.PLAYER, new ArrayList<>());

            this.registerAlias("show");
            this.registerAlias("view");

            this.registerArgument(new StringArgument("player", true));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            String playerName = (String) args.get("player");

            SunkenPlayer sunkenPlayer = null;

            if (Bukkit.getPlayer(playerName) != null && playerManager.getPlayer(Bukkit.getPlayer(playerName).getUniqueId()) != null) {
                sunkenPlayer = playerManager.getPlayer(Bukkit.getPlayer(playerName).getUniqueId());
            } else {
                if (playerManager.getPlayer(playerName) != null) {
                    sunkenPlayer = playerManager.getPlayer(playerName);
                } else {
                    sender.sendMessage(MsgType.WARNING.format("Loading data for <p>...", playerName));

                    Bukkit.getScheduler().runTaskAsynchronously(Anchor.getPlugin(Anchor.class), () -> {
                        SunkenPlayer sp = Mongo.getPlayerDAO().findOne("username", playerName);

                        if (sp != null) {
                            playerManager.cachePlayer(sp, 60 * 5); // Cache for 5 minutes

                            listPerms(sender, sp);
                        } else {
                            sender.sendMessage(MsgType.DANGER.format("Error loading data for <p>! Have they logged in before?", playerName));
                        }
                    });
                }
            }

            if (sunkenPlayer != null) {
                listPerms(sender, sunkenPlayer);
            }
        }

    }

    private void updatePermission(CommandSender sender, SunkenPlayer sunkenPlayer, String perm, boolean value, boolean delete) {
        if (perm.startsWith("-")) {
            sender.sendMessage(MsgType.DANGER.format("Permissions must be set using the original permission node (no \"-\")!"));
            return;
        }

        Set<String> permissions = new HashSet<>(sunkenPlayer.getPlayerPermissions());
        Rank rank = sunkenPlayer.getRank();
        String prefix = (sunkenPlayer.getPrefix().equals(rank.getPrefix()) ? null : sunkenPlayer.getPrefix());
        String suffix = (sunkenPlayer.getSuffix().equals(rank.getSuffix()) ? null : sunkenPlayer.getSuffix());
        ChatColor chatColor = (sunkenPlayer.getChatColor().equals(rank.getChatColor()) ? null : sunkenPlayer.getChatColor());

        if (delete) {
            if (permissions.contains(perm)) {
                permissions.remove(perm);
            } else if (permissions.contains("-" + perm)) {
                permissions.remove("-" + perm);
            } else {
                sender.sendMessage(MsgType.DANGER.format("That user doesn't have the permission <p>!", perm));
                return;
            }
        } else {
            if (permissions.contains("-" + perm)) {
                if (value) {
                    permissions.remove("-" + perm);
                    permissions.add(perm);
                }
            } else if (permissions.contains(perm)) {
                if (!value) {
                    permissions.remove(perm);
                    permissions.add("-" + perm);
                }
            } else {
                permissions.add((value ? perm : "-" + perm));
            }
        }

        UserUpdateData data = new UserUpdateData(sunkenPlayer.getUUID().toString(), prefix, suffix, chatColor, rank.getName(), permissions);

        Bukkit.getScheduler().runTaskAsynchronously(Anchor.getPlugin(Anchor.class), () -> {
            if (Anchor.getPlugin(Anchor.class).isRedis()) {
                try (Jedis jedis = Redis.getJedisPool().getResource()) {
                    jedis.publish(Anchor.USER_UPDATE_CHANNEL, Anchor.getPlugin(Anchor.class).getGSON().toJson(data));
                }
            } else {
                data.apply();
            }

            Mongo.getPlayerDAO().save(sunkenPlayer);

            if (delete) {
                sender.sendMessage(MsgType.SUCCESS.format("<p> has been removed from <p>!", perm, sunkenPlayer.getUsername()));
            } else {
                sender.sendMessage(MsgType.SUCCESS.format("<p> has been set to <p> for <p>!", perm, (value ? ChatColor.GREEN : ChatColor.RED) + "" + ChatColor.BOLD + value, sunkenPlayer.getUsername()));
            }
        });
    }

    private void listPerms(CommandSender sender, SunkenPlayer player) {
        if (player.getPlayerPermissions().isEmpty()) {
            sender.sendMessage(MsgType.DANGER.format("This user currently doesn't have any custom permissions! Add some with <p>!", "/user perm add <player> <perm>"));
            return;
        }

        List<TextComponent> msgs = new ArrayList<>();

        sender.sendMessage(" ");
        sender.sendMessage(MsgType.CENTERED.format(ChatColor.AQUA + "" + ChatColor.BOLD + "Showing a list of all <p>'s permissions...", player.getUsername()));
        sender.sendMessage(" ");

        for (String perm : player.getPlayerPermissions()) {
            if (perm.startsWith("-")) {
                msgs.add(new TextComponent(ChatColor.DARK_GRAY + "" + ChatColor.BOLD + " * " + ChatColor.RED + perm.substring(1)));
            } else {
                msgs.add(new TextComponent(ChatColor.DARK_GRAY + "" + ChatColor.BOLD + " * " + ChatColor.WHITE + perm));
            }
        }

        new ChatPaginator((Player) sender, "/user perms list " + player.getUsername(), 7, msgs);
    }

}