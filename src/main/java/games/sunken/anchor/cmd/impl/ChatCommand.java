package games.sunken.anchor.cmd.impl;

import com.google.common.collect.ImmutableMap;
import games.sunken.anchor.Anchor;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.chat.channel.ChatChannel;
import games.sunken.anchor.chat.channel.impl.StaffChannel;
import games.sunken.anchor.cmd.AllowedSenderType;
import games.sunken.anchor.cmd.SunkenCommand;
import games.sunken.anchor.cmd.args.ChannelArgument;
import games.sunken.anchor.player.SunkenPlayer;
import games.sunken.anchor.util.XSound;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collection;

public class ChatCommand extends SunkenCommand {

    public ChatCommand() {
        super("chat", "Change chat channels", "sunken.chat", AllowedSenderType.PLAYER, new ArrayList<>());

        registerArgument(new ChannelArgument("channel", false));

        registerAlias("ch");
        registerAlias("c");
    }

    @Override
    protected void runCommand(CommandSender sender, ImmutableMap<String, Object> arguments) {
        Player player = (Player) sender;
        SunkenPlayer sunkenPlayer = Anchor.getPlugin(Anchor.class).getPlayerManager().getPlayer(player.getUniqueId());

        if (arguments.containsKey("channel")) {
            ChatChannel chatChannel = (ChatChannel) arguments.get("channel");

            if (sunkenPlayer.getChatFocus() != null && chatChannel.getName().equals(sunkenPlayer.getChatFocus().getName())) {
                sunkenPlayer.sendMessage(MsgType.DANGER, XSound.BLOCK_NOTE_BLOCK_BASS, new TextComponent("<p> is already your chat focus!"), chatChannel.getChatColor() + chatChannel.getName());
                return;
            }

            if (chatChannel.canSeeChannel(sunkenPlayer)) {
                sunkenPlayer.setChatFocus(chatChannel);
            } else {
                sunkenPlayer.sendMessage(MsgType.DANGER, XSound.BLOCK_NOTE_BLOCK_BASS, new TextComponent("You are unable to set <p> as your chat focus!"), chatChannel.getChatColor() + chatChannel.getName());
            }
        } else {
            Collection<ChatChannel> channels = Anchor.getPlugin(Anchor.class).getChannelManager().getChannels();

            sunkenPlayer.sendMessage(MsgType.CENTERED, null, new TextComponent(" "));
            sunkenPlayer.sendMessage(MsgType.CENTERED, null, new TextComponent(ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "Available Chat Channels:"));
            sunkenPlayer.sendMessage(MsgType.CENTERED, null, new TextComponent(" "));

            for (ChatChannel chatChannel : channels) {
                if (chatChannel instanceof StaffChannel && !chatChannel.canSeeChannel(sunkenPlayer)) {
                    continue;
                }

                sender.sendMessage(ChatColor.DARK_GRAY + " - " + chatChannel.getChatColor() + chatChannel.getName() +
                        (sunkenPlayer.getChatFocus() != null && sunkenPlayer.getChatFocus().getName().equals(chatChannel.getName())
                                ? ChatColor.GREEN + "" + ChatColor.BOLD + " (FOCUS)" : ""));
            }

            sunkenPlayer.sendMessage(MsgType.CENTERED, null, new TextComponent(" "));
            sunkenPlayer.sendMessage(MsgType.INFO, null, new TextComponent("Type <p> to change your chat focus!"), "/chat <channel>");
        }
    }

}