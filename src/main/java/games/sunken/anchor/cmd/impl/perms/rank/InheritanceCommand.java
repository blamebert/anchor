package games.sunken.anchor.cmd.impl.perms.rank;

import com.google.common.collect.ImmutableMap;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.cmd.AllowedSenderType;
import games.sunken.anchor.cmd.SunkenCommand;
import games.sunken.anchor.cmd.args.RankArgument;
import games.sunken.anchor.perms.Rank;
import games.sunken.anchor.perms.RankManager;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.Arrays;

public class InheritanceCommand extends SunkenCommand {

    private RankManager rankManager;

    public InheritanceCommand(RankManager rankManager) {
        super("inherits", "Manage a rank's inheritances", "sunken.rank.inherits", AllowedSenderType.PLAYER, new ArrayList<>());

        this.rankManager = rankManager;

        this.registerSubCommands(Arrays.asList(
                new AddCommand(),
                new DeleteCommand()
        ));
    }

    @Override
    protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
        this.sendUsage(sender);
    }

    protected class AddCommand extends SunkenCommand {

        public AddCommand() {
            super("add", "Add an inheritance to a rank", "sunken.rank.inherits.add", AllowedSenderType.PLAYER, new ArrayList<>());

            this.registerArgument(new RankArgument("rank", true));
            this.registerArgument(new RankArgument("inheritance", true));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            Rank rank = (Rank) args.get("rank");
            Rank inherits = (Rank) args.get("inheritance");

            if (rank.getInherits() == null || rank.getInherits().isEmpty() || !rank.getInherits().contains(inherits)) {
                if (!rank.equals(inherits)) {
                    rank.addInheritance(inherits);
                } else {
                    sender.sendMessage(MsgType.DANGER.format("A rank can't inherit itself!"));
                    return;
                }
            } else {
                sender.sendMessage(MsgType.DANGER.format("That rank already inherits <p>!", inherits.getName()));
                return;
            }

            rankManager.updateRank(rank);

            sender.sendMessage(MsgType.SUCCESS.format("Rank inheritance <p> added to <p>!", inherits.getName(), rank.getName()));
        }

    }

    protected class DeleteCommand extends SunkenCommand {

        public DeleteCommand() {
            super("delete", "Delete a rank's inheritance", "sunken.rank.inherits.delete", AllowedSenderType.PLAYER, new ArrayList<>());
            this.registerAlias("del");
            this.registerAlias("remove");

            this.registerArgument(new RankArgument("rank", true));
            this.registerArgument(new RankArgument("inheritance", true));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            Rank rank = (Rank) args.get("rank");
            Rank inherits = (Rank) args.get("inheritance");

            if (rank.getInherits().contains(inherits)) {
                rank.addInheritance(inherits);
            } else {
                sender.sendMessage(MsgType.DANGER.format("That rank doesn't currently inherit <p>!", inherits.getName()));
                return;
            }

            rankManager.updateRank(rank);

            sender.sendMessage(MsgType.SUCCESS.format("Rank inheritance <p> removed from <p>!", inherits.getName(), rank.getName()));
        }

    }

}