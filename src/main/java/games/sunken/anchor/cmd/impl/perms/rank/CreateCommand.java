package games.sunken.anchor.cmd.impl.perms.rank;

import com.google.common.collect.ImmutableMap;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.cmd.AllowedSenderType;
import games.sunken.anchor.cmd.SunkenCommand;
import games.sunken.anchor.cmd.args.IntegerArgument;
import games.sunken.anchor.cmd.args.StringArgument;
import games.sunken.anchor.perms.RankManager;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;

public class CreateCommand extends SunkenCommand {

    private RankManager rankManager;

    public CreateCommand(RankManager rankManager) {
        super("create", "Create a new rank", "sunken.rank.create", AllowedSenderType.PLAYER, new ArrayList<>());

        this.registerAlias("add");
        this.registerAlias("new");

        this.registerArgument(new StringArgument("name", true));
        this.registerArgument(new IntegerArgument("priority", true, 0, Integer.MAX_VALUE));

        this.rankManager = rankManager;
    }

    @Override
    protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
        String name = (String) args.get("name");
        int priority = (int) args.get("priority");

        try {
            rankManager.createRank(name, priority);
        } catch (IllegalArgumentException ex) {
            sender.sendMessage(MsgType.DANGER.format(ex.getMessage()));
            return;
        } catch (Exception ex) {
            sender.sendMessage(MsgType.DANGER.format("An internal error occurred while attempting to create a rank!"));
            ex.printStackTrace();
            return;
        }

        sender.sendMessage(MsgType.SUCCESS.format("Rank <p> successfully created with priority <p>!", name, priority));
    }

}