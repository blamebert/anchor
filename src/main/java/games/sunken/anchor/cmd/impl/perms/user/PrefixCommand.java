package games.sunken.anchor.cmd.impl.perms.user;

import com.google.common.collect.ImmutableMap;
import games.sunken.anchor.Anchor;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.cmd.AllowedSenderType;
import games.sunken.anchor.cmd.SunkenCommand;
import games.sunken.anchor.cmd.args.StringArgument;
import games.sunken.anchor.database.Mongo;
import games.sunken.anchor.database.redis.Redis;
import games.sunken.anchor.perms.Rank;
import games.sunken.anchor.perms.UserUpdateData;
import games.sunken.anchor.player.PlayerManager;
import games.sunken.anchor.player.SunkenPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.Arrays;

public class PrefixCommand extends SunkenCommand {

    private PlayerManager playerManager;

    public PrefixCommand(PlayerManager playerManager) {
        super("prefix", "Manage a user's prefix", "sunken.user.prefix", AllowedSenderType.PLAYER, new ArrayList<>());

        this.playerManager = playerManager;

        this.registerSubCommands(Arrays.asList(
                new SetCommand(),
                new DeleteCommand()
        ));
    }

    @Override
    protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
        this.sendUsage(sender);
    }

    protected class SetCommand extends SunkenCommand {

        public SetCommand() {
            super("set", "Change a user's prefix", "sunken.user.prefix.set", AllowedSenderType.PLAYER, new ArrayList<>());

            this.registerAlias("change");

            this.registerArgument(new StringArgument("player", true));
            this.setExtraMessageArgument(new StringArgument("prefix", true));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            String playerName = (String) args.get("player");
            String prefix = (String) args.get("prefix");
            SunkenPlayer sunkenPlayer = null;

            if (Bukkit.getPlayer(playerName) != null && playerManager.getPlayer(Bukkit.getPlayer(playerName).getUniqueId()) != null) {
                sunkenPlayer = playerManager.getPlayer(Bukkit.getPlayer(playerName).getUniqueId());
            } else {
                if (playerManager.getPlayer(playerName) != null) {
                    sunkenPlayer = playerManager.getPlayer(playerName);
                } else {
                    sender.sendMessage(MsgType.WARNING.format("Loading data for <p>...", playerName));

                    Bukkit.getScheduler().runTaskAsynchronously(Anchor.getPlugin(Anchor.class), () -> {
                        SunkenPlayer sp = Mongo.getPlayerDAO().findOne("username", playerName);

                        if (sp != null) {
                            playerManager.cachePlayer(sp, 60 * 5); // Cache for 5 minutes

                            setPrefix(sender, sp, prefix);
                        } else {
                            sender.sendMessage(MsgType.DANGER.format("Error loading data for <p>! Have they logged in before?", playerName));
                        }
                    });
                }
            }

            if (sunkenPlayer != null) {
                setPrefix(sender, sunkenPlayer, prefix);
            }
        }

    }

    protected class DeleteCommand extends SunkenCommand {

        public DeleteCommand() {
            super("delete", "Delete a user's prefix", "sunken.user.prefix.delete", AllowedSenderType.PLAYER, new ArrayList<>());

            this.registerAlias("del");
            this.registerAlias("remove");
            this.registerAlias("reset");

            this.registerArgument(new StringArgument("player", true));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            String playerName = (String) args.get("player");
            SunkenPlayer sunkenPlayer = null;

            if (Bukkit.getPlayer(playerName) != null && playerManager.getPlayer(Bukkit.getPlayer(playerName).getUniqueId()) != null) {
                sunkenPlayer = playerManager.getPlayer(Bukkit.getPlayer(playerName).getUniqueId());
            } else {
                if (playerManager.getPlayer(playerName) != null) {
                    sunkenPlayer = playerManager.getPlayer(playerName);
                } else {
                    sender.sendMessage(MsgType.WARNING.format("Loading data for <p>...", playerName));

                    Bukkit.getScheduler().runTaskAsynchronously(Anchor.getPlugin(Anchor.class), () -> {
                        SunkenPlayer sp = Mongo.getPlayerDAO().findOne("username", playerName);

                        if (sp != null) {
                            playerManager.cachePlayer(sp, 60 * 5); // Cache for 5 minutes

                            setPrefix(sender, sp, null);
                        } else {
                            sender.sendMessage(MsgType.DANGER.format("Error loading data for <p>! Have they logged in before?", playerName));
                        }
                    });
                }
            }

            if (sunkenPlayer != null) {
                setPrefix(sender, sunkenPlayer, null);
            }
        }

    }

    private void setPrefix(CommandSender sender, SunkenPlayer player, String prefix) {
        Rank rank = player.getRank();
        String suffix = (player.getSuffix() == null || player.getSuffix().equals(rank.getSuffix()) ? null : player.getSuffix());
        ChatColor chatColor = (player.getChatColor() == null || player.getChatColor().equals(rank.getChatColor()) ? null : player.getChatColor());

        UserUpdateData data = new UserUpdateData(player.getUUID().toString(), prefix, suffix, chatColor, rank.getName(), player.getPlayerPermissions());

        Bukkit.getScheduler().runTaskAsynchronously(Anchor.getPlugin(Anchor.class), () -> {
            if (Anchor.getPlugin(Anchor.class).isRedis()) {
                try (Jedis jedis = Redis.getJedisPool().getResource()) {
                    jedis.publish(Anchor.USER_UPDATE_CHANNEL, Anchor.getPlugin(Anchor.class).getGSON().toJson(data));
                }
            } else {
                data.apply();
            }

            Mongo.getPlayerDAO().save(player);

            if (prefix == null) {
                sender.sendMessage(MsgType.SUCCESS.format("<p>'s prefix has been removed!", player.getUsername()));
            } else {
                sender.sendMessage(MsgType.SUCCESS.format("<p>'s prefix has been set to <p>!", player.getUsername(), prefix));
            }
        });
    }

}