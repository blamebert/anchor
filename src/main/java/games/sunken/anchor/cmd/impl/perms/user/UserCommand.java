package games.sunken.anchor.cmd.impl.perms.user;

import com.google.common.collect.ImmutableMap;
import games.sunken.anchor.cmd.AllowedSenderType;
import games.sunken.anchor.cmd.SunkenCommand;
import games.sunken.anchor.player.PlayerManager;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.Arrays;

public class UserCommand extends SunkenCommand {

    private PlayerManager playerManager;

    public UserCommand(PlayerManager playerManager) {
        super("user", "View & modify player data", "sunken.user", AllowedSenderType.PLAYER, new ArrayList<>());
        this.registerAlias("users");
        this.registerAlias("u");

        this.playerManager = playerManager;

        this.registerSubCommands(Arrays.asList(
                new InfoCommand(playerManager),
                new RankCommand(playerManager),
                new PrefixCommand(playerManager),
                new SuffixCommand(playerManager),
                new ChatColorCommand(playerManager),
                new PermissionsCommand(playerManager)
        ));
    }

    @Override
    protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
        this.sendUsage(sender);
    }

}