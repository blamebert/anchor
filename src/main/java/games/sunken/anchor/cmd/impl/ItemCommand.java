package games.sunken.anchor.cmd.impl;

import com.google.common.collect.ImmutableMap;
import games.sunken.anchor.Anchor;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.cmd.AllowedSenderType;
import games.sunken.anchor.cmd.SunkenCommand;
import games.sunken.anchor.cmd.args.StringArgument;
import games.sunken.anchor.player.SunkenPlayer;
import games.sunken.anchor.util.XSound;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class ItemCommand extends SunkenCommand {

    public ItemCommand() {
        super("item", "Customize the item in your hand", "sunken.item", AllowedSenderType.PLAYER, new ArrayList<>());

        registerAlias("i");

        registerSubCommand("settag", new SetTagCommand());
        registerSubCommand("gettag", new GetTagCommand());
    }

    @Override
    protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
        super.sendUsage(sender);
    }

    class SetTagCommand extends SunkenCommand {

        public SetTagCommand() {
            super("settag", "Set an NBT tag on an item", "sunken.item.settag", AllowedSenderType.PLAYER, new ArrayList<>());

            registerArgument(new StringArgument("tag", true, 0, 32, false, false));

            setExtraMessageArgument(new StringArgument("value", true));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            Player player = (Player) sender;
            SunkenPlayer sunkenPlayer = Anchor.getPlugin(Anchor.class).getPlayerManager().getPlayer(player.getUniqueId());

            if (player.getItemInHand() != null) {
                player.setItemInHand(Anchor.getPlugin(Anchor.class).getItemUtil().setTag(player.getItemInHand(), (String) args.get("tag"), args.get("value")));

                sunkenPlayer.sendMessage(MsgType.SUCCESS, XSound.BLOCK_NOTE_BLOCK_PLING, new TextComponent("Item tag set!"));
            } else {
                sunkenPlayer.sendMessage(MsgType.DANGER, XSound.BLOCK_NOTE_BLOCK_BASS, new TextComponent("You must be holding an item to sue this command!"));
            }
        }

    }

    class GetTagCommand extends SunkenCommand {

        public GetTagCommand() {
            super("gettag", "Get an NBT tag on an item", "sunken.item.gettag", AllowedSenderType.PLAYER, new ArrayList<>());

            registerArgument(new StringArgument("tag", true, 0, 32, false, false));
        }

        @Override
        protected void runCommand(CommandSender sender, ImmutableMap<String, Object> args) {
            Player player = (Player) sender;
            SunkenPlayer sunkenPlayer = Anchor.getPlugin(Anchor.class).getPlayerManager().getPlayer(player.getUniqueId());

            if (player.getItemInHand() != null) {
                if (Anchor.getPlugin(Anchor.class).getItemUtil().hasTag(player.getItemInHand(), (String) args.get("tag"))) {
                    Object tag = Anchor.getPlugin(Anchor.class).getItemUtil().getTag(player.getItemInHand(), (String) args.get("tag"));

                    sunkenPlayer.sendMessage(MsgType.SUCCESS, XSound.BLOCK_NOTE_BLOCK_PLING, new TextComponent("Tag Found! Value: <p>"), tag.toString());
                } else {
                    sunkenPlayer.sendMessage(MsgType.DANGER, XSound.BLOCK_NOTE_BLOCK_BASS, new TextComponent("There is no NBT tag by that name on this item!"));
                }
            } else {
                sunkenPlayer.sendMessage(MsgType.DANGER, XSound.BLOCK_NOTE_BLOCK_BASS, new TextComponent("You must be holding an item to sue this command!"));
            }
        }

    }

}