package games.sunken.anchor.cmd;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import games.sunken.anchor.chat.ChatPaginator;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.chat.TimeFormatter;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.concurrent.TimeUnit;

public abstract class SunkenCommand extends Command {

    private String name;
    private String description;
    private String permission;
    private AllowedSenderType senderType;
    private List<CommandArgument> argumentList = new ArrayList<>();
    private Map<String, SunkenCommand> subCommands = new HashMap<>();
    private Cache<UUID, Long> nextCommandUsage = CacheBuilder.newBuilder().concurrencyLevel(4).expireAfterWrite(1, TimeUnit.MINUTES).build();
    private long usageCooldown = 0;
    private CommandArgument extraMessageArgument = null;

    public SunkenCommand(String name, String description, String permission, AllowedSenderType senderType, List<CommandArgument> argumentList) {
        super(name.toLowerCase(), description, "", new ArrayList<>());
        this.name = name;
        this.description = description;
        this.permission = permission;
        this.senderType = senderType;

        this.registerArguments(argumentList);
    }

    public SunkenCommand(String name, String description, String permission, AllowedSenderType senderType, List<CommandArgument> argumentList, List<SunkenCommand> subCommands) {
        super(name.toLowerCase(), description, "", new ArrayList<>());
        this.name = name;
        this.description = description;
        this.permission = permission;
        this.senderType = senderType;

        this.registerArguments(argumentList);
        this.registerSubCommands(subCommands);
    }

    public void setNextAllowedUsage(UUID playerID, long time) {
        this.nextCommandUsage.put(playerID, time);
    }

    public long getNextAllowedUsage(UUID playerID) {
        Long time = nextCommandUsage.getIfPresent(playerID);
        return time != null ? time : 0;
    }

    public CommandArgument getExtraMessageArgument() {
        return extraMessageArgument;
    }

    public void setExtraMessageArgument(CommandArgument extraMessageArgument) {
        if (extraMessageArgument.isRequired()) {
            for (CommandArgument argument : getArgumentList()) {
                if (!argument.isRequired()) {
                    throw new IllegalArgumentException("You can't require a message to be provided if you have optional arguments before it.");
                }
            }
        }
        this.extraMessageArgument = extraMessageArgument;
    }

    public boolean hasUsageCooldown() {
        return usageCooldown > 0;
    }

    public long getUsageCooldown() {
        return usageCooldown;
    }

    public void setUsageCooldown(long duration, TimeUnit unit) {
        this.usageCooldown = unit.toMillis(duration);
        this.nextCommandUsage.invalidateAll();
        this.nextCommandUsage = CacheBuilder.newBuilder().concurrencyLevel(4).expireAfterWrite(getUsageCooldown() * 2, TimeUnit.MILLISECONDS).build();
    }

    public void registerAlias(String alias) {
        List<String> aliases = super.getAliases();
        aliases.add(alias);

        super.setAliases(aliases);
    }

    public void registerSubCommands(Collection<SunkenCommand> subcommands) {
        for (SunkenCommand cmd : subcommands) {
            registerSubCommand(cmd.getLabel(), cmd);
        }
    }

    public SunkenCommand getSubCommand(String cmd) {
        return subCommands.getOrDefault(cmd.toLowerCase(), null);
    }

    public void registerSubCommand(String cmd, SunkenCommand command) {
        subCommands.put(cmd.toLowerCase(), command);
    }

    private String getDisplayArgs() {
        StringBuilder sb = new StringBuilder();

        for (CommandArgument argument : argumentList) {
            sb.append(argument.isRequired() ? "<" : "[").append(argument.getLabel()).append(argument.isRequired() ? ">" : "]").append(" ");
        }

        if (getExtraMessageArgument() != null) {
            CommandArgument a = getExtraMessageArgument();
            sb.append(a.isRequired() ? "<" : "[").append(a.getLabel()).append(a.isRequired() ? ">" : "]").append(" ");
        }

        return sb.toString();
    }

    public void sendUsage(CommandSender sender) {
        this.sendUsage(sender, new ArrayList<>());
    }

    public void sendUsage(CommandSender sender, List<SunkenCommand> parentCommands) {
        StringBuilder prefix;

        if (parentCommands.isEmpty()) {
            prefix = new StringBuilder("/" + getLabel());
        } else {
            prefix = new StringBuilder("/");

            for (SunkenCommand cmd : parentCommands) {
                prefix.append(cmd.getLabel()).append(" ");
            }

            prefix.append(getLabel());
        }

        sender.sendMessage(" ");

        if (!getDisplayArgs().isEmpty()) {
            sender.sendMessage(MsgType.CENTERED.format(ChatColor.AQUA + "" + ChatColor.BOLD + "Showing help for <p>...", ChatColor.WHITE + prefix.toString() + " " + getDisplayArgs()));
        } else {
            sender.sendMessage(MsgType.CENTERED.format(ChatColor.AQUA + "" + ChatColor.BOLD + "Showing help for <p>...", ChatColor.WHITE + prefix.toString()));
        }

        sender.sendMessage(MsgType.CENTERED.format(ChatColor.GRAY + getDescription()));
        sender.sendMessage(" ");

        String bullet = ChatColor.DARK_GRAY + "" + ChatColor.BOLD + " * " + ChatColor.WHITE;
        if (sender instanceof Player) {
            List<TextComponent> components = new ArrayList<>();

            if (!subCommands.isEmpty()) {
                for (Map.Entry<String, SunkenCommand> entry : subCommands.entrySet()) {
                    SunkenCommand cmd = entry.getValue();

                    if (cmd.getPermission() != null && !sender.hasPermission(cmd.getPermission())) continue;

                    String cmdString = prefix.toString() + " " + entry.getKey() +
                            (!cmd.getDisplayArgs().isEmpty() ? ChatColor.ITALIC + " " + cmd.getDisplayArgs() : " ");

                    TextComponent textComponent = new TextComponent(bullet);

                    TextComponent cmdTextComponent = new TextComponent(cmdString);
                    cmdTextComponent.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, ChatColor.stripColor(cmdString)));
                    cmdTextComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Click " + ChatColor.GRAY + "to type command.").create()));

                    textComponent.addExtra(cmdTextComponent);

                    textComponent.addExtra(new TextComponent(ChatColor.DARK_GRAY + "- "));

                    TextComponent desc = new TextComponent(ChatColor.GRAY + "" + ChatColor.ITALIC + cmd.getDescription());
                    desc.setItalic(true);
                    desc.setColor(net.md_5.bungee.api.ChatColor.GRAY);

                    textComponent.addExtra(desc);

                    components.add(textComponent);
                }
            } else {
                String cmdString = prefix.toString();

                TextComponent textComponent = new TextComponent(bullet);

                TextComponent cmdTextComponent = new TextComponent(cmdString);
                cmdTextComponent.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, ChatColor.stripColor(cmdString)));
                cmdTextComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.WHITE + "Click " + ChatColor.GRAY + "to type command.").create()));

                textComponent.addExtra(cmdTextComponent);

                if (!getDescription().isEmpty()) {
                    textComponent.addExtra(new TextComponent(ChatColor.DARK_GRAY + "- "));

                    TextComponent desc = new TextComponent(ChatColor.GRAY + "" + ChatColor.ITALIC + getDescription());
                    desc.setItalic(true);
                    desc.setColor(net.md_5.bungee.api.ChatColor.GRAY);

                    textComponent.addExtra(desc);
                }

                components.add(textComponent);
            }

            new ChatPaginator(((Player) sender), prefix.toString(), 7, components);
        } else {
            if (!subCommands.isEmpty()) {
                for (Map.Entry<String, SunkenCommand> entry : subCommands.entrySet()) {
                    SunkenCommand cmd = entry.getValue();

                    if (cmd.getPermission() != null && !sender.hasPermission(cmd.getPermission())) continue;

                    String desc = cmd.getDisplayArgs();
                    sender.sendMessage(ChatColor.DARK_GRAY + " * " + ChatColor.WHITE + prefix.toString() + " " + entry.getKey() +
                            (!desc.isEmpty() ? ChatColor.ITALIC + " " + desc : " ") + ChatColor.DARK_GRAY + "- " + ChatColor.GRAY + "" + ChatColor.ITALIC + cmd.getDescription());
                }
            } else {
                sender.sendMessage(ChatColor.DARK_GRAY + " * " + ChatColor.WHITE + prefix.toString() + " " + ChatColor.DARK_GRAY +
                        (!getDescription().isEmpty() ? "-" + ChatColor.GRAY + " " + ChatColor.ITALIC + getDescription() : ""));
            }

            sender.sendMessage(" ");
        }
    }

    public int getMinimumArgs() {
        int count = 0;

        for (CommandArgument argument : argumentList) {
            if (argument.isRequired()) {
                count++;
            }
        }

        if (getExtraMessageArgument() != null && getExtraMessageArgument().isRequired()) {
            count++;
        }

        return count;
    }

    private void registerArguments(Collection<CommandArgument> argumentCollection) {
        for (CommandArgument a : argumentCollection) {
            this.registerArgument(a);
        }
    }

    public void registerArgument(CommandArgument argument) {
        argumentList.add(argument);
        this.sortArguments();
    }

    private void sortArguments() {
        argumentList.sort((a1, a2) -> {
            if (a1.isRequired() && !a2.isRequired()) {
                return -1;
            } else if (!a1.isRequired() && a2.isRequired()) {
                return 1;
            } else {
                if (a1.getId() < a2.getId()) {
                    return -1;
                } else if (a1.getId() > a2.getId()) {
                    return 1;
                }
            }

            return 0;
        });
    }

    private void preCommandLogic(CommandSender sender, ImmutableList<String> args) {
        this.preCommandLogic(sender, args, new ArrayList<>());
    }

    private void preCommandLogic(CommandSender sender, ImmutableList<String> args, List<SunkenCommand> parentCommands) {
        if (!getSenderType().matches(sender)) {
            sender.sendMessage(MsgType.DANGER.format(senderType.getError()));
            return;
        }

        if (getPermission() != null && !sender.hasPermission(getPermission())) {
            sender.sendMessage(MsgType.DANGER.format("You do not have permission to use this command! <p>", ChatColor.GRAY + "(" + getPermission() + ")"));
            return;
        }

        if (args.size() < getMinimumArgs() || (args.size() == 1 && (args.get(0).equalsIgnoreCase("?") || args.get(0).equalsIgnoreCase("help")))) {
            sendUsage(sender, parentCommands);
            return;
        }

        if (args.size() >= 1) {
            SunkenCommand subCommand = getSubCommand(args.get(0));

            if (subCommand != null) {
                List<String> finalArgs = new ArrayList<>();

                for (int i = 1; i < args.size(); i++) {
                    finalArgs.add(args.get(i));
                }

                parentCommands.add(this);

                subCommand.preCommandLogic(sender, ImmutableList.copyOf(finalArgs), parentCommands);

                return;
            }
        }

        if ((sender instanceof Player) && hasUsageCooldown()) {
            Player p = (Player) sender;
            long nextUsage = getNextAllowedUsage(p.getUniqueId());

            if (System.currentTimeMillis() < nextUsage) {
                sender.sendMessage(MsgType.DANGER.format("You cannot use this command for another <p>!", TimeFormatter.formatTime(nextUsage, true)));
                return;
            }
        }

        ImmutableMap.Builder<String, Object> finalArgs = ImmutableMap.builder();

        List<String> argsAfterFlags = new ArrayList<>(args);

        int index = 0;
        for (CommandArgument argument : argumentList) {
            if (index >= argsAfterFlags.size()) {
                break;
            }

            try {
                Object o = argument.getValue(argsAfterFlags.get(index++));
                finalArgs.put(argument.getLabel(), o);
            } catch (IllegalArgumentException ex) {
                sender.sendMessage(MsgType.DANGER.format(ex.getMessage()));
                return;
            }
        }

        if (getExtraMessageArgument() != null) {
            StringBuilder finalMessage = new StringBuilder();
            for (int i = index; i < argsAfterFlags.size(); i++) {
                finalMessage.append(argsAfterFlags.get(i)).append(" ");
            }

            if (finalMessage.toString().endsWith(" ")) {
                finalMessage = new StringBuilder(finalMessage.substring(0, finalMessage.length() - 1));
            }

            try {
                String parsed = (String) getExtraMessageArgument().getValue(finalMessage.toString());
                finalArgs.put(getExtraMessageArgument().getLabel(), parsed);
            } catch (IllegalArgumentException ex) {
                sender.sendMessage(MsgType.DANGER.format(ex.getMessage()));
                return;
            }

        }

        try {
            runCommand(sender, finalArgs.build());
        } catch (IllegalArgumentException ex) {
            sender.sendMessage(MsgType.DANGER.format(ex.getMessage()));
            return;
        }

        if (hasUsageCooldown() && (sender instanceof Player)) {
            Player p = (Player) sender;
            setNextAllowedUsage(p.getUniqueId(), System.currentTimeMillis() + getUsageCooldown());
        }
    }

    public CommandArgument getNextArgument(int index) {
        if (getArgumentList().size() > index) {
            return getArgumentList().get(index);
        }

        return null;
    }

    @Override
    @Nonnull
    public List<String> tabComplete(@Nonnull CommandSender sender, @Nonnull String alias, @Nonnull String[] args) throws IllegalArgumentException {
        if (args.length == 0) {
            return ImmutableList.of();
        } else {
            SunkenCommand baseCommand = this;

            if (getPermission() != null && !sender.hasPermission(baseCommand.getPermission())) {
                return ImmutableList.of();
            }

            for (int i = 0; i < args.length - 1; i++) {
                String temp = args[i];
                SunkenCommand c = baseCommand.getSubCommand(temp);

                if (c != null) {
                    baseCommand = c;
                } else {
                    break;
                }
            }

            CommandArgument argument = baseCommand.getNextArgument(args.length - 1);
            String lastWord = args[args.length - 1];

            if (argument != null) {
                return argument.getTabCompletionResults(sender, lastWord);
            } else {
                List<String> subCommands = new ArrayList<>();

                for (SunkenCommand subCommand : baseCommand.subCommands.values()) {
                    if (subCommand.getLabel().toLowerCase().startsWith(lastWord.toLowerCase())) {
                        subCommands.add(subCommand.getLabel());
                    }
                }

                return subCommands.isEmpty() ? super.tabComplete(sender, alias, args) : subCommands;
            }
        }
    }

    @Override
    public boolean execute(@Nonnull CommandSender sender, @Nonnull String s, @Nonnull String[] args) {
        List<String> finalArgs = new ArrayList<>(Arrays.asList(args));

        preCommandLogic(sender, ImmutableList.copyOf(finalArgs));

        return true;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getPermission() {
        return permission;
    }

    public List<CommandArgument> getArgumentList() {
        return argumentList;
    }

    public AllowedSenderType getSenderType() {
        return senderType;
    }

    protected abstract void runCommand(CommandSender sender, ImmutableMap<String, Object> arguments);

}