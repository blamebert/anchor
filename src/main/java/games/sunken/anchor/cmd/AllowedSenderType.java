package games.sunken.anchor.cmd;

import org.bukkit.block.CommandBlock;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public enum AllowedSenderType {

    PLAYER("Only players can use this command!") {
        @Override
        public boolean matches(CommandSender sender) {
            return ((sender instanceof Player));
        }
    },
    CONSOLE("Only the console can use this command!") {
        @Override
        public boolean matches(CommandSender sender) {
            return ((sender instanceof ConsoleCommandSender));
        }
    },
    COMMAND_BLOCK("Only command blocks can use this command!") {
        @Override
        public boolean matches(CommandSender sender) {
            return ((sender instanceof CommandBlock));
        }
    },
    ANY("") {
        @Override
        public boolean matches(CommandSender sender) {
            return true;
        }
    };

    private String error;

    AllowedSenderType(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public abstract boolean matches(CommandSender sender);

}