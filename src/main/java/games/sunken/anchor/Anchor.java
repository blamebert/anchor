package games.sunken.anchor;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import games.sunken.anchor.chat.channel.ChannelManager;
import games.sunken.anchor.chat.channel.StaffChatRedisModule;
import games.sunken.anchor.chat.channel.impl.GlobalChannel;
import games.sunken.anchor.chat.channel.impl.StaffChannel;
import games.sunken.anchor.cmd.CommandManager;
import games.sunken.anchor.cmd.impl.ChatCommand;
import games.sunken.anchor.cmd.impl.ChatShortcutCommand;
import games.sunken.anchor.cmd.impl.ItemCommand;
import games.sunken.anchor.cmd.impl.perms.rank.RankCommand;
import games.sunken.anchor.cmd.impl.perms.user.UserCommand;
import games.sunken.anchor.database.Mongo;
import games.sunken.anchor.database.mysql.DatabaseInfo;
import games.sunken.anchor.database.mysql.DatabaseManager;
import games.sunken.anchor.database.mysql.MySQL;
import games.sunken.anchor.database.redis.Redis;
import games.sunken.anchor.game.Game;
import games.sunken.anchor.game.map.MapManager;
import games.sunken.anchor.game.state.GameState;
import games.sunken.anchor.game.teams.impl.Spectators;
import games.sunken.anchor.game.teams.listeners.SpectatorListener;
import games.sunken.anchor.items.enchant.EnchantmentManager;
import games.sunken.anchor.listener.ChatListener;
import games.sunken.anchor.listener.ConnectionListener;
import games.sunken.anchor.listener.WorldListener;
import games.sunken.anchor.npc.NPCManager;
import games.sunken.anchor.perms.RankManager;
import games.sunken.anchor.perms.modules.RankUpdateRedisModule;
import games.sunken.anchor.perms.modules.UserUpdateRedisModule;
import games.sunken.anchor.player.PlayerManager;
import games.sunken.anchor.server.ServerData;
import games.sunken.anchor.server.ServerDataRedisModule;
import games.sunken.anchor.util.ItemUtil;
import games.sunken.anchor.util.LocationUtil;
import games.sunken.anchor.util.SkinUtil;
import games.sunken.anchor.util.XMaterial;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import redis.clients.jedis.Jedis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class Anchor extends JavaPlugin {

    private Game game;

    private ChannelManager channelManager;
    private CommandManager commandManager;
    private EnchantmentManager enchantmentManager;
    private MapManager mapManager;
    private NPCManager npcManager;
    private PlayerManager playerManager;
    private RankManager rankManager;

    private ItemUtil itemUtil;
    private SkinUtil skinUtil;
    private LocationUtil locationUtil;

    private String serverId;

    private boolean redis;
    private boolean mySQLEnabled;

    private Gson GSON;

    public static final String DYNAMIC_SERVER_CHANNEL = "dynamic-server";
    public static final String RANK_UPDATE_CHANNEL = "rank-updates";
    public static final String STAFF_CHAT_CHANNEL = "staff-chat";
    public static final String USER_UPDATE_CHANNEL = "user-updates";

    private ServerData serverData;

    private Cache<String, ServerData> SERVER_CACHE = CacheBuilder.newBuilder()
            .expireAfterWrite(30, TimeUnit.SECONDS).build();

    private DatabaseManager databaseManager;
    private MySQL mySQL;

    @Override
    public void onEnable() {
        this.saveDefaultConfig();

        File mongoCreds = new File(getDataFolder() + File.separator + "mongo.json");

        if (!mongoCreds.exists()) mongoCreds.mkdirs();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(mongoCreds));
            JsonParser parser = new JsonParser();

            Mongo.init(parser.parse(reader).getAsJsonObject());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            getServer().shutdown();
        }

        this.GSON = new GsonBuilder().create();

        this.serverId = this.getConfig().getString("server-id");
        this.redis = getConfig().getBoolean("redis.enabled", false);
        this.mySQLEnabled = getConfig().getBoolean("mysql.enabled", false);

        // REDIS CONNECTION
        if (redis) {
            Redis.init(this.getConfig().getString("redis.hostname"), this.getConfig().getInt("redis.port"),
                    this.getConfig().getString("redis.password"), Sets.newHashSet(
                            new StaffChatRedisModule(),
                            new ServerDataRedisModule(),
                            new RankUpdateRedisModule(),
                            new UserUpdateRedisModule()
                    )
            );

            this.getServer().getScheduler().runTaskTimerAsynchronously(this, () -> {
                if (game == null) {
                    serverData = new ServerData(
                            this.serverId,
                            this.getServer().getIp() + ":" + this.getServer().getPort(),
                            this.getServer().getOnlinePlayers().size(),
                            this.getServer().getMaxPlayers(),
                            "Broken",
                            GameState.JoinPermission.DENIED,
                            XMaterial.REDSTONE_BLOCK
                    );
                } else {
                    serverData = new ServerData(
                            this.serverId,
                            this.getServer().getIp() + ":" + this.getServer().getPort(),
                            this.getServer().getOnlinePlayers().size(),
                            this.getServer().getMaxPlayers(),
                            game.getGameStates().getCurrentState().getName(),
                            game.getGameStates().getCurrentState().getJoinPermission(),
                            game.getGameStates().getCurrentState().getIcon()
                    );
                }

                try (Jedis jedis = Redis.getJedisPool().getResource()) {
                    jedis.publish(DYNAMIC_SERVER_CHANNEL, getGSON().toJson(serverData));
                }
            }, 0, 60);
        }

        if (mySQLEnabled) {
            try {
                this.databaseManager = new DatabaseManager(new DatabaseInfo(getConfig(), "mysql"));
                this.mySQL = new MySQL();

                mySQL.init(databaseManager, null);
            } catch (Exception e) {
                e.printStackTrace();
                getServer().shutdown();
                return;
            }
        }

        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

        this.channelManager = new ChannelManager(new GlobalChannel());
        this.commandManager = new CommandManager();
        this.enchantmentManager = new EnchantmentManager(this);
        this.mapManager = new MapManager();
        this.npcManager = new NPCManager(this);
        this.playerManager = new PlayerManager();
        this.rankManager = new RankManager();

        this.itemUtil = new ItemUtil(this);
        this.skinUtil = new SkinUtil(this);
        this.locationUtil = new LocationUtil();

        Stream.of(
                new ChatListener(),
                new ConnectionListener(this.playerManager),
                new WorldListener(this)
        ).forEach(listener -> getServer().getPluginManager().registerEvents(listener, this));

        Stream.of(
                new StaffChannel(this)
        ).forEach(channel -> getChannelManager().register(channel));

        commandManager.registerCommand(new RankCommand(rankManager));
        commandManager.registerCommand(new UserCommand(playerManager));
        commandManager.registerCommand(new ItemCommand());
        commandManager.registerCommand(new ChatCommand());
        channelManager.getChannels().forEach(chatChannel -> commandManager.registerCommand(new ChatShortcutCommand(chatChannel)));
    }

    @Override
    public void onDisable() {
        try {
            Field enabledField = JavaPlugin.class.getDeclaredField("isEnabled");
            enabledField.setAccessible(true);
            enabledField.set(this, true);
        } catch (ReflectiveOperationException ex) {
            getLogger().severe("Error while setting fake enabled status:");
            ex.printStackTrace();
        }

        if (getGame() != null && getGame().getMap() != null && getGame().getMap().getWorld() != null) getServer().unloadWorld(getGame().getMap().getWorld(), false);

        Bukkit.getScheduler().runTaskAsynchronously(this, () -> {
            playerManager.getOnlinePlayers().forEach(sunkenPlayer -> {
                Mongo.getPlayerDAO().save(sunkenPlayer);
                playerManager.removePlayer(sunkenPlayer.getUUID());
            });

            File worldFolder = getGame().getMap().getWorld().getWorldFolder();
            worldFolder.delete();
        });

        try {
            Field enabledField = JavaPlugin.class.getDeclaredField("isEnabled");
            enabledField.setAccessible(true);
            enabledField.set(this, true);
        } catch (ReflectiveOperationException ex) {
            getLogger().severe("Error while restoring real enabled status:");
            ex.printStackTrace();
        }

        getEnchantmentManager().unregisterAll();

        Mongo.getClient().close();

        if (redis) {
            Redis.disable();
        }

        if (mySQLEnabled) {
            if (getDatabaseManager() != null && getDatabaseManager().getDataSource() != null) {
                getDatabaseManager().getDataSource().close();
            }
        }
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;

        if (game.getTeam("Spectators") != null)
            getServer().getPluginManager().registerEvents(new SpectatorListener(game, (Spectators) game.getTeam("Spectators")), this);
    }

    public String getServerId() {
        return serverId;
    }

    public ServerData getServerData() {
        return serverData;
    }

    public void setServerData(ServerData serverData) {
        this.serverData = serverData;
    }

    public boolean isRedis() {
        return redis;
    }

    public boolean isMySQLEnabled() {
        return mySQLEnabled;
    }

    public MySQL getMySQL() {
        return mySQL;
    }

    public Gson getGSON() {
        return GSON;
    }

    public ChannelManager getChannelManager() {
        return channelManager;
    }

    public CommandManager getCommandManager() {
        return commandManager;
    }

    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    public EnchantmentManager getEnchantmentManager() {
        return enchantmentManager;
    }

    public MapManager getMapManager() {
        return mapManager;
    }

    public NPCManager getNPCManager() {
        return npcManager;
    }

    public PlayerManager getPlayerManager() {
        return playerManager;
    }

    public RankManager getRankManager() {
        return rankManager;
    }

    public ItemUtil getItemUtil() {
        return itemUtil;
    }

    public LocationUtil getLocationUtil() {
        return locationUtil;
    }

    public SkinUtil getSkinUtil() {
        return skinUtil;
    }

    public Cache<String, ServerData> getServerCache() {
        return SERVER_CACHE;
    }

}