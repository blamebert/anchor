package games.sunken.anchor.database;

import com.mongodb.event.ServerHeartbeatFailedEvent;
import com.mongodb.event.ServerHeartbeatStartedEvent;
import com.mongodb.event.ServerHeartbeatSucceededEvent;
import com.mongodb.event.ServerMonitorListener;
import games.sunken.anchor.Anchor;
import games.sunken.anchor.chat.channel.ChannelManager;
import games.sunken.anchor.chat.channel.impl.StaffChannel;
import org.bukkit.Bukkit;

public class MongoConnectionListener implements ServerMonitorListener {

    public enum ConnectionStatus {

        CONNECTING,
        CONNECTED,
        DISCONNECTED

    }

    private ConnectionStatus connectionStatus;

    public MongoConnectionListener() {
        this.connectionStatus = ConnectionStatus.DISCONNECTED;
    }

    @Override
    public void serverHearbeatStarted(ServerHeartbeatStartedEvent event) {
        this.connectionStatus = ConnectionStatus.CONNECTING;
    }

    @Override
    public void serverHeartbeatSucceeded(ServerHeartbeatSucceededEvent event) {
        this.connectionStatus = ConnectionStatus.CONNECTED;
    }

    @Override
    public void serverHeartbeatFailed(ServerHeartbeatFailedEvent event) {
        this.connectionStatus = ConnectionStatus.DISCONNECTED;

        ChannelManager channelManager = Anchor.getPlugin(Anchor.class).getChannelManager();

        if (channelManager != null && channelManager.getByName("Staff") != null) {
            ((StaffChannel) channelManager.getByName("Staff")).sendSystemMessage("MONGO CONNECTION HAS FAILED! PLEASE REPORT THIS TO ADMINS! SERVER SHUTTING DOWN...");
        }

        Bukkit.getServer().shutdown();
    }

    public ConnectionStatus getConnectionStatus() {
        return connectionStatus;
    }

}