package games.sunken.anchor.database.redis;

import javax.annotation.Nonnull;
import java.util.function.BiConsumer;

public interface RedisModule {

    @Nonnull String getName();
    @Nonnull String getChannel();
    boolean isDaemonThread();

    BiConsumer<String, String> onMessage();
    BiConsumer<String, Integer> onSubscribe();
    BiConsumer<String, Integer> onUnsubscribe();

    Runnable onDisable();

}