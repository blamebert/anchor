package games.sunken.anchor.database.redis;

import games.sunken.anchor.Anchor;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPubSub;

import java.util.Set;

public class Redis {

    private static JedisPool JEDIS_POOL;
    private static Set<RedisModule> redisModules;

    public static void init(String hostname, int port, String password, Set<RedisModule> redisModules) {
        if (JEDIS_POOL != null) {
            return;
        }

        Redis.redisModules = redisModules;

        ClassLoader previous = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(Anchor.class.getClassLoader());

        if (password != null && password.trim().length() > 0) {
            JEDIS_POOL = new JedisPool(new GenericObjectPoolConfig(), hostname, port, 5000, password);
        } else {
            JEDIS_POOL = new JedisPool(new GenericObjectPoolConfig(), hostname, port, 5000);
        }

        Thread.currentThread().setContextClassLoader(previous);

        for (RedisModule redisModule : redisModules) {
            registerModule(redisModule);
        }
    }

    public static void disable() {
        for (RedisModule redisModule : redisModules) {
            if (redisModule.onDisable() != null) redisModule.onDisable().run();
        }
    }

    public static void addModule(RedisModule redisModule) {
        redisModules.add(redisModule);
        registerModule(redisModule);
    }

    private static void registerModule(RedisModule redisModule) {
        Thread moduleRunner = new Thread(() -> {
            while (true) {
                try (Jedis jedis = JEDIS_POOL.getResource()) {
                    jedis.subscribe(new JedisPubSub() {
                        @Override
                        public void onMessage(String channel, String message) {
                            if (redisModule.onMessage() != null) redisModule.onMessage().accept(channel, message);
                        }

                        @Override
                        public void onSubscribe(String channel, int subscribedChannels) {
                            if (redisModule.onSubscribe() != null) redisModule.onSubscribe().accept(channel, subscribedChannels);
                        }

                        @Override
                        public void onUnsubscribe(String channel, int subscribedChannels) {
                            if (redisModule.onUnsubscribe() != null) redisModule.onUnsubscribe().accept(channel, subscribedChannels);
                        }
                    }, redisModule.getChannel());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        moduleRunner.setName(redisModule.getName());
        moduleRunner.setDaemon(redisModule.isDaemonThread());
        moduleRunner.start();
    }

    public static JedisPool getJedisPool() {
        return JEDIS_POOL;
    }

    public static Set<RedisModule> getRedisModules() {
        return redisModules;
    }

}