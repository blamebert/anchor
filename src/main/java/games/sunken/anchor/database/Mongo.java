package games.sunken.anchor.database;

import com.google.gson.JsonObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import games.sunken.anchor.Anchor;
import games.sunken.anchor.game.map.MapInfo;
import games.sunken.anchor.game.map.MapSettings;
import games.sunken.anchor.game.map.dao.MapInfoDAO;
import games.sunken.anchor.game.map.dao.MapSettingsDAO;
import games.sunken.anchor.perms.Rank;
import games.sunken.anchor.perms.RankDAO;
import games.sunken.anchor.player.SunkenPlayer;
import games.sunken.anchor.player.SunkenPlayerDAO;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.mapping.DefaultCreator;

public class Mongo {

    private static MongoClient client;
    private static Morphia morphia;
    private static Datastore datastore;
    private static MongoConnectionListener mongoConnectionListener = new MongoConnectionListener();

    private static SunkenPlayerDAO playerDAO;
    private static MapInfoDAO mapInfoDAO;
    private static MapSettingsDAO mapSettingsDAO;
    private static RankDAO rankDAO;

    public static MongoClient getClient() {
        return client;
    }

    public static Morphia getMorphia() {
        return morphia;
    }

    public static Datastore getDatastore() {
        return datastore;
    }

    public static MongoConnectionListener getConnectionListener() {
        return mongoConnectionListener;
    }

    public static SunkenPlayerDAO getPlayerDAO() { return playerDAO; }

    public static MapInfoDAO getMapInfoDAO() {
        return mapInfoDAO;
    }

    public static MapSettingsDAO getMapSettingsDAO() {
        return mapSettingsDAO;
    }

    public static RankDAO getRankDAO() {
        return rankDAO;
    }

    public static void init(JsonObject config) {
        MongoCredential credentials = null;

        if (config.has("username") && config.get("username").getAsString().length() > 0) {
            credentials = MongoCredential.createCredential(config.get("username").getAsString(), config.get("credentials_database").getAsString(), config.get("password").getAsString().toCharArray());
        }

        Mongo.client = new MongoClient(new ServerAddress(config.get("host").getAsString(), config.get("port").getAsInt()), credentials,
                new MongoClientOptions.Builder().addServerMonitorListener(mongoConnectionListener).build());
        Mongo.morphia = new Morphia();
        Mongo.datastore = morphia.createDatastore(client, config.get("database").getAsString());
        Mongo.datastore.ensureIndexes();

        morphia.getMapper().getOptions().setObjectFactory(new DefaultCreator() {
            @Override
            protected ClassLoader getClassLoaderForClass() {
                return Anchor.class.getClassLoader();
            }
        });

        Mongo.playerDAO = new SunkenPlayerDAO(SunkenPlayer.class, datastore);
        Mongo.mapInfoDAO = new MapInfoDAO(MapInfo.class, datastore);
        Mongo.mapSettingsDAO = new MapSettingsDAO(MapSettings.class, datastore);
        Mongo.rankDAO = new RankDAO(Rank.class, datastore);

        client.listDatabaseNames().iterator().close();
    }

}