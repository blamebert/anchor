package games.sunken.anchor.database.mysql;

import games.sunken.anchor.Anchor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import org.apache.commons.io.FileUtils;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.time.Instant;
import java.util.Date;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;

public class MySQL {

    private DatabaseManager manager;
    private ExecutorService queryManager;
    private Map<Class, Function<Object, Object>> serializable;

    public MySQL() {
        this.queryManager = Executors.newFixedThreadPool(20);
        this.serializable = new HashMap<>();

        serializable.put(UUID.class, Object::toString);
        serializable.put(java.util.Date.class, (object) -> new Timestamp(((Date) object).getTime()));
        serializable.put(ItemStack.class, (object) -> Anchor.getPlugin(Anchor.class).getItemUtil().serializeItem((ItemStack) object));
        serializable.put(TextComponent.class, (object) -> ComponentSerializer.toString((TextComponent) object));
        serializable.put(Instant.class, (object) -> ((Instant) object).toEpochMilli());
    }

    public void init(DatabaseManager manager, @Nullable List<File> autoExec) {
        this.manager = manager;
        manager.getConfig().addDataSourceProperty("leakDetectionThreshold", "5000");

        if (autoExec != null && !autoExec.isEmpty()) {
            for (File file : autoExec) {
                try {
                    executeAsync(FileUtils.readFileToString(file));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void executeAsync(String command, Object... obj) {
        queryManager.submit(() -> {
            try (Connection connection = manager.getConnection()) {
                makeStatement(command, connection, obj).execute();
            } catch (SQLException e) {
                Anchor.getPlugin(Anchor.class).getLogger().severe("ATTEMPTED COMMAND: " + command);
                e.printStackTrace();
            }
        });
    }

    public void executeSync(String command, Object... obj) {
        try (Connection connection = manager.getConnection()) {
            makeStatement(command, connection, obj).execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Query query(String command, Object... obj) {
        try {
            Connection connection = manager.getConnection();

            return new Query(connection, makeStatement(command, connection, obj).executeQuery());
        } catch (SQLException e) {
            Anchor.getPlugin(Anchor.class).getLogger().severe("ATTEMPTED COMMAND: " + command);

            StringBuilder builder = new StringBuilder();

            for (Object object : obj) {
                builder.append(object.toString()).append(" ");
            }

            Anchor.getPlugin(Anchor.class).getLogger().severe("PASSED ARGS: " + builder.toString().trim());

            e.printStackTrace();
        }

        return null;
    }

    public void executeSync(InputStream stream) {
        Scanner scanner = new Scanner(stream);
        scanner.useDelimiter("(;(\r)?\n)|(--\n)");

        try (Connection connection = manager.getConnection()) {
            Statement statement = connection.createStatement();

            while (scanner.hasNext()) {
                String line = scanner.next();

                if (line.startsWith("/*!") && line.endsWith("*/")) {
                    int index = line.indexOf(' ');
                    line = line.substring(index + 1, line.length() - " */".length());
                }

                if (line.trim().length() > 0) {
                    statement.execute(line);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private PreparedStatement makeStatement(String statement, Connection connection, Object... parameters) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(statement);

        if (parameters != null) {
            for (int i = 0; i < parameters.length; i++) {
                if (serializable.containsKey(parameters[i].getClass())) {
                    preparedStatement.setObject(i + 1, serializable.get(parameters[i].getClass()).apply(parameters[i]));
                } else {
                    preparedStatement.setObject(i + 1, parameters[i]);
                }
            }
        }

        return preparedStatement;
    }

    public static class Query implements AutoCloseable {

        private Connection connection;

        private ResultSet resultSet;

        private Query(Connection connection, ResultSet resultSet) {
            this.connection = connection;
            this.resultSet = resultSet;
        }

        public ResultSet getResultSet() {
            return this.resultSet;
        }

        @Override
        public void close() throws SQLException {
            this.connection.close();
        }

    }

}