package games.sunken.anchor.database.mysql;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class DatabaseManager {

    private DatabaseInfo info;
    private HikariConfig config;
    private HikariDataSource dataSource;

    public DatabaseManager(DatabaseInfo info) {
        this.info = info;
        this.config = new HikariConfig();
        config.setJdbcUrl("jdbc:mysql://" + info.getHost() + ":" + info.getPort() + "/" + info.getDatabase());
        config.setUsername(info.getUsername());
        config.setPassword(info.getPassword());
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        config.setMaximumPoolSize(35);

        this.dataSource = new HikariDataSource(config);
    }

    public Connection getConnection() throws SQLException {
        return getDataSource().getConnection();
    }

    public DatabaseInfo getInfo() {
        return info;
    }

    public HikariConfig getConfig() {
        return config;
    }

    public HikariDataSource getDataSource() {
        return dataSource;
    }

}