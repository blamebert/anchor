package games.sunken.anchor.database.mysql;

import org.bukkit.configuration.file.FileConfiguration;

public class DatabaseInfo {

    private String host;
    private int port;
    private String username;
    private String password;
    private String database;

    public DatabaseInfo(String host, int port, String username, String password, String database) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        this.database = database;
    }

    public DatabaseInfo(FileConfiguration conf, String path) {
        this.host = conf.getString(path + ".host");
        this.username = conf.getString(path + ".username");
        this.password = conf.getString(path + ".password");
        this.database = conf.getString(path + ".database");
        this.port = conf.getInt(path + ".port");
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getDatabase() {
        return database;
    }

}