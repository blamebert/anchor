package games.sunken.anchor.items;

import games.sunken.anchor.util.Reflection;
import games.sunken.anchor.util.XMaterial;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.InvocationTargetException;
import java.util.Objects;

public class EnchantmentGlow {

    public static ItemStack addGlow(ItemStack item) {
        Objects.requireNonNull(item);

        if (item.getEnchantments().size() == 0)
            if (Reflection.VERSION_NUMBER < 1110) {
                try {
                    Object nms = Reflection.getCraftClass("inventory.CraftItemStack").getMethod("asNMSCopy", ItemStack.class).invoke(null, item);

                    Class tagClass = Reflection.getMcClass("NBTTagCompound");
                    Object tag = !(boolean) nms.getClass().getMethod("hasTag").invoke(nms) ? tagClass.newInstance() : nms.getClass().getMethod("getTag").invoke(nms);

                    tag.getClass().getMethod("set", String.class, Reflection.getMcClass("NBTBase")).invoke(tag, "ench", Reflection.getMcClass("NBTTagList").newInstance());
                    nms.getClass().getMethod("setTag", tagClass).invoke(nms, tag);

                    return (ItemStack) Reflection.getCraftClass("inventory.CraftItemStack").getMethod("asCraftMirror", Reflection.getMcClass("ItemStack")).invoke(null, nms);
                } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException | InstantiationException e) {
                    e.printStackTrace();
                }
            } else {
                ItemStack itemStack = item.clone();
                itemStack.addUnsafeEnchantment(item.getType() != XMaterial.FISHING_ROD.parseMaterial() ? Enchantment.LURE : Enchantment.PROTECTION_FIRE, 1);

                ItemMeta itemMeta = itemStack.getItemMeta();
                itemMeta.addItemFlags(org.bukkit.inventory.ItemFlag.HIDE_ENCHANTS);

                itemStack.setItemMeta(itemMeta);

                return itemStack;
            }


        return item;
    }

}