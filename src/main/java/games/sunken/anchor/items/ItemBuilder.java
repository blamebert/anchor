package games.sunken.anchor.items;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import games.sunken.anchor.Anchor;
import games.sunken.anchor.items.enchant.CustomEnchantment;
import games.sunken.anchor.util.XMaterial;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.MaterialData;

import javax.annotation.Nonnull;
import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ItemBuilder {

    private final ItemStack is;

    public ItemBuilder(@Nonnull final Material mat) { is = new ItemStack(mat); }

    public ItemBuilder(@Nonnull final ItemStack is) {
        this.is = is.clone();
    }

    public ItemBuilder(@Nonnull final XMaterial xMaterial) { is = xMaterial.parseItem(); }

    @Nonnull
    public ItemBuilder withQuantity(final int amount) {
        is.setAmount(amount);
        return this;
    }

    @Nonnull
    public ItemBuilder withDisplayName(@Nonnull final String name) {
        final ItemMeta meta = is.getItemMeta();
        meta.setDisplayName(name);
        is.setItemMeta(meta);
        return this;
    }

    @Nonnull
    public ItemBuilder withLoreLine(@Nonnull final String line) {
        final ItemMeta meta = is.getItemMeta();
        List<String> lore = meta.getLore();

        if (lore == null) {
            lore = new ArrayList<>();
        }

        lore.add(line);
        meta.setLore(lore);

        is.setItemMeta(meta);

        return this;
    }

    @Nonnull
    public ItemBuilder withLore(@Nonnull final String... lore) {
        final ItemMeta meta = is.getItemMeta();
        meta.setLore(Arrays.asList(lore));

        is.setItemMeta(meta);

        return this;
    }

    @Nonnull
    public ItemBuilder withLore(@Nonnull final Stream<String> lore) {
        final ItemMeta meta = is.getItemMeta();
        meta.setLore(lore.collect(Collectors.toList()));

        is.setItemMeta(meta);

        return this;
    }

    @Nonnull
    public ItemBuilder withLore(@Nonnull final List<String> lore) {
        final ItemMeta meta = is.getItemMeta();
        meta.setLore(lore);

        is.setItemMeta(meta);

        return this;
    }

    @Nonnull
    public ItemBuilder withDurability(final int durability) {
        is.setDurability((short) durability);
        return this;
    }

    @SuppressWarnings("deprecation")
    @Nonnull
    public ItemBuilder withData(final int data) {
        is.setData(new MaterialData(is.getType(), (byte) data));

        return this;
    }

    @Nonnull
    public ItemBuilder withEnchantment(@Nonnull final Enchantment enchantment, final int level) {
        if (is.getType() == XMaterial.ENCHANTED_BOOK.parseMaterial()) {
            EnchantmentStorageMeta meta = (EnchantmentStorageMeta) is.getItemMeta();
            meta.addStoredEnchant(enchantment, level, true);
            is.setItemMeta(meta);
        } else {
            if (enchantment instanceof CustomEnchantment) {
                Anchor.getPlugin(Anchor.class).getEnchantmentManager().addEnchantment(is, (CustomEnchantment) enchantment, level);

                return this;
            }

            is.addUnsafeEnchantment(enchantment, level);
        }

        return this;
    }

    @Nonnull
    public ItemBuilder withEnchantment(@Nonnull final Enchantment enchantment) {
        return withEnchantment(enchantment, 1);
    }

    @Nonnull
    public ItemBuilder setType(@Nonnull final XMaterial xMaterial) {
        Objects.requireNonNull(xMaterial.parseMaterial());

        is.setType(Objects.requireNonNull(xMaterial.parseMaterial()));

        return this;
    }

    @Nonnull
    public ItemBuilder clearLore() {
        final ItemMeta meta = is.getItemMeta();
        meta.setLore(new ArrayList<>());
        is.setItemMeta(meta);

        return this;
    }

    @Nonnull
    public ItemBuilder clearEnchantments() {
        if (is.getType() == XMaterial.ENCHANTED_BOOK.parseMaterial()) {
            EnchantmentStorageMeta meta = (EnchantmentStorageMeta) is.getItemMeta();

            for (Map.Entry<Enchantment, Integer> entry : meta.getStoredEnchants().entrySet()) {
                meta.removeStoredEnchant(entry.getKey());
            }

            is.setItemMeta(meta);
        } else {
            for (final Enchantment e : is.getEnchantments().keySet()) {
                if (e instanceof CustomEnchantment)
                    Anchor.getPlugin(Anchor.class).getEnchantmentManager().removeEnchantment(is, (CustomEnchantment) e);

                is.removeEnchantment(e);
            }
        }

        return this;
    }

    @Nonnull
    public ItemBuilder withColor(@Nonnull Color color) {
        if (is.getType() == XMaterial.LEATHER_BOOTS.parseMaterial() || is.getType() == XMaterial.LEATHER_CHESTPLATE.parseMaterial()
                || is.getType() == XMaterial.LEATHER_HELMET.parseMaterial()
                || is.getType() == XMaterial.LEATHER_LEGGINGS.parseMaterial()) {
            LeatherArmorMeta meta = (LeatherArmorMeta) is.getItemMeta();
            meta.setColor(color);
            is.setItemMeta(meta);

            return this;
        } else {
            throw new IllegalArgumentException("withColor() is only applicable to leather armor!");
        }
    }

    @Nonnull
    public ItemBuilder withMeta(@Nonnull Consumer<ItemMeta> consumer) {
        ItemMeta meta = is.getItemMeta();
        consumer.accept(meta);
        is.setItemMeta(meta);

        return this;
    }

    @Nonnull
    public ItemBuilder withGlow() {
        EnchantmentGlow.addGlow(is);

        return this;
    }

    @Nonnull
    public ItemBuilder withItemFlags(ItemFlag... flag) {
        final ItemMeta meta = is.getItemMeta();
        meta.addItemFlags(flag);
        is.setItemMeta(meta);

        return this;
    }

    @Nonnull
    @SuppressWarnings("deprecation")
    public ItemBuilder withSkullOwner(String owner) {
        if (is.getType() == XMaterial.PLAYER_HEAD.parseMaterial()) {
            SkullMeta meta = (SkullMeta) is.getItemMeta();
            meta.setOwner(owner);
            is.setItemMeta(meta);

            return this;
        } else {
            throw new IllegalArgumentException("withSkullOwner() is only applicable to player skulls!");
        }
    }

    @Nonnull
    public ItemBuilder withSkinURL(String url) {
        if (is.getType() == XMaterial.PLAYER_HEAD.parseMaterial()) {
            SkullMeta meta = (SkullMeta) is.getItemMeta();
            GameProfile profile = new GameProfile(UUID.randomUUID(), null);
            byte[] encodedData = Base64.getEncoder().encode(String.format("{textures:{SKIN:{url:\"%s\"}}}", url).getBytes());
            profile.getProperties().put("textures", new Property("textures", new String(encodedData)));
            Field profileField;
            try {
                profileField = meta.getClass().getDeclaredField("profile");
                profileField.setAccessible(true);
                profileField.set(meta, profile);
            } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
                e1.printStackTrace();
            }
            is.setItemMeta(meta);

            return this;
        } else {
            throw new IllegalArgumentException("withSkinUrl() is only applicable to player skulls!");
        }
    }

    @Nonnull
    public ItemBuilder withNBTTag(String key, Object value) {
        Anchor.getPlugin(Anchor.class).getItemUtil().setTag(is, key, value);

        return this;
    }

    @Nonnull
    public ItemStack build() {
        return is.clone();
    }


}