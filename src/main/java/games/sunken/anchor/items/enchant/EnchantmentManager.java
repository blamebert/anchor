package games.sunken.anchor.items.enchant;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.util.RomanNumerals;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

public class EnchantmentManager {

    private Anchor plugin;
    private Set<CustomEnchantment> customEnchantments;

    public EnchantmentManager(Anchor plugin) {
        this.plugin = plugin;
        this.customEnchantments = new HashSet<>();
    }

    public boolean addEnchantment(ItemStack itemStack, CustomEnchantment customEnchantment) {
        int level = new Random().nextInt(customEnchantment.getMaxLevel());

        return addEnchantment(itemStack, customEnchantment, (level != 0 ? level : 1));
    }

    public boolean addEnchantment(ItemStack itemStack, CustomEnchantment customEnchantment, int level) {
        if (customEnchantment.canEnchantItem(itemStack)) {
            if (!itemStack.containsEnchantment(customEnchantment)) {
                itemStack.addUnsafeEnchantment(customEnchantment, level);

                if (itemStack.hasItemMeta()) {
                    ItemMeta meta = itemStack.getItemMeta();

                    if (meta.hasLore() && meta.getLore() != null) {
                        List<String> lore = new ArrayList<>(meta.getLore());
                        lore.add(customEnchantment.getChatColor() + customEnchantment.getName() + " " + getFormattedLevel(level));
                        meta.setLore(lore);
                    } else {
                        meta.setLore(Collections.singletonList(customEnchantment.getChatColor() + customEnchantment.getName() + " " + getFormattedLevel(level)));
                    }

                    itemStack.setItemMeta(meta);
                } else {
                    ItemMeta meta = itemStack.getItemMeta();
                    meta.setLore(Collections.singletonList(customEnchantment.getChatColor() + customEnchantment.getName() + " " + getFormattedLevel(level)));
                    itemStack.setItemMeta(meta);
                }

                return true;
            }

            return false;
        }

        return false;
    }

    public boolean removeEnchantment(ItemStack itemStack, CustomEnchantment customEnchantment) {
        if (itemStack.containsEnchantment(customEnchantment)) {
            int level = itemStack.getEnchantmentLevel(customEnchantment);

            if (itemStack.hasItemMeta()) {
                ItemMeta meta = itemStack.getItemMeta();

                if (meta.hasLore() && meta.getLore() != null) {
                    List<String> lore = new ArrayList<>(meta.getLore());
                    lore.remove(customEnchantment.getChatColor() + customEnchantment.getName() + " " + getFormattedLevel(level));
                    meta.setLore(lore);
                }

                itemStack.setItemMeta(meta);
            }

            itemStack.removeEnchantment(customEnchantment);
        }

        return false;
    }

    public String getFormattedLevel(int level) {
        return RomanNumerals.toRomanNumeral(level);
    }

    public Set<CustomEnchantment> getCustomEnchantments() {
        return customEnchantments;
    }

    public void registerEnchantment(CustomEnchantment enchantment) {
        customEnchantments.add(enchantment);
    }

    public void unregisterEnchantment(CustomEnchantment enchantment) {
        customEnchantments.remove(enchantment);
    }

    public void unregisterAll() {
        getCustomEnchantments().forEach(CustomEnchantment::unregister);
        getCustomEnchantments().clear();
    }


}