package games.sunken.anchor.items.enchant;

import games.sunken.anchor.Anchor;
import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;
import java.util.HashMap;

public abstract class CustomEnchantment extends Enchantment {

    public CustomEnchantment(int id) {
        super(id);

        register();
    }

    public int getActivationMultiplier() {
        return 100 / getMaxLevel();
    }

    public abstract boolean handleUsage(Player player, ItemStack itemStack, int level, Event event);

    public abstract ChatColor getChatColor();

    public void register() {
        try {
            try {
                Field f = Enchantment.class.getDeclaredField("acceptingNew");
                f.setAccessible(true);
                f.set(null, true);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                Enchantment.registerEnchantment(this);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }

            Anchor.getPlugin(Anchor.class).getEnchantmentManager().registerEnchantment(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void unregister() {
        try {
            Field byIdField = Enchantment.class.getDeclaredField("byId");
            Field byNameField = Enchantment.class.getDeclaredField("byName");

            byIdField.setAccessible(true);
            byNameField.setAccessible(true);

            HashMap<Integer, Enchantment> byId = (HashMap<Integer, Enchantment>) byIdField.get(null);
            HashMap<Integer, Enchantment> byName = (HashMap<Integer, Enchantment>) byNameField.get(null);

            byId.remove(this.getId());
            byName.remove(this.getName());
        } catch (Exception ignored) { }
    }

}