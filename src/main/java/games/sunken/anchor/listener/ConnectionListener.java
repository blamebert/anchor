package games.sunken.anchor.listener;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.database.Mongo;
import games.sunken.anchor.database.MongoConnectionListener;
import games.sunken.anchor.game.state.GameState;
import games.sunken.anchor.player.PlayerManager;
import games.sunken.anchor.player.SunkenPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ConnectionListener implements Listener {

    private PlayerManager playerManager;

    public ConnectionListener(PlayerManager playerManager) {
        this.playerManager = playerManager;
    }

    @EventHandler
    public void onAsyncPlayerPreLogin(AsyncPlayerPreLoginEvent event) {
        if (Mongo.getConnectionListener().getConnectionStatus() != MongoConnectionListener.ConnectionStatus.CONNECTED) {
            event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, ChatColor.RED + "This server has not finished starting up yet!");
            event.setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_OTHER);
            event.setKickMessage(ChatColor.RED + "This server has not finished starting up yet!");
            return;
        }

        if (Anchor.getPlugin(Anchor.class).getGame().getGameStates().getCurrentState().getJoinPermission() == GameState.JoinPermission.DENIED) {
            event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, ChatColor.RED + "This server is not joinable at the moment!");
            event.setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_OTHER);
            event.setKickMessage(ChatColor.RED + "This server is not joinable at the moment!");
            return;
        }

        SunkenPlayer sunkenPlayer = Mongo.getPlayerDAO().findOne("uuid", event.getUniqueId().toString());

        if (sunkenPlayer == null) sunkenPlayer = new SunkenPlayer(event.getUniqueId(), event.getName());

        playerManager.addPlayer(sunkenPlayer);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onAsyncPlayerPreLoginMonitor(AsyncPlayerPreLoginEvent event) {
        if (event.getLoginResult() != AsyncPlayerPreLoginEvent.Result.ALLOWED) {
            if (playerManager.getPlayer(event.getUniqueId()) != null) {
                playerManager.removePlayer(event.getUniqueId());
            }
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        SunkenPlayer sunkenPlayer = playerManager.getPlayer(event.getPlayer().getUniqueId());

        sunkenPlayer.recalculatePermissions(true);

        /*if (event.getPlayer().getName().equals("BlameBert")) {
            CreatureNPCType.CreatureNPC creatureNPC = new CreatureNPCType.CreatureNPC(EntityType.VILLAGER, event.getPlayer().getLocation(), NPCType.NPCVisibility.ALWAYS, (player) -> player.sendMessage("HRMPH"), ChatColor.RED + "" + ChatColor.BOLD + "TEST NPC", " ", ChatColor.GRAY + "Click me!");

            Anchor.getPlugin(Anchor.class).getNPCManager().getNPCType(EntityType.VILLAGER).registerNPC(creatureNPC);
            creatureNPC.spawn();
        }*/
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        SunkenPlayer sunkenPlayer = playerManager.getPlayer(player.getUniqueId());

        Bukkit.getScheduler().runTaskAsynchronously(Anchor.getPlugin(Anchor.class), () -> {
            Mongo.getPlayerDAO().save(sunkenPlayer);
            playerManager.removePlayer(player.getUniqueId());
        });
    }

}