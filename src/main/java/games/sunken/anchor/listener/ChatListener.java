package games.sunken.anchor.listener;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.chat.Prompt;
import games.sunken.anchor.player.SunkenPlayer;
import games.sunken.anchor.util.XSound;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class ChatListener implements Listener {

    @EventHandler
    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        if (event.isCancelled()) return;

        Player player = event.getPlayer();
        SunkenPlayer sunkenPlayer = Anchor.getPlugin(Anchor.class).getPlayerManager().getPlayer(player.getUniqueId());

        Prompt prompt = sunkenPlayer.getPrompt();
        if (sunkenPlayer.getMapCreator() != null) {
            if (sunkenPlayer.getPrompt() != null) {
                if (event.getMessage().equalsIgnoreCase("cancel")) {
                    sunkenPlayer.sendMessage(MsgType.DANGER, XSound.BLOCK_NOTE_BLOCK_BASS, new TextComponent("Map creation <p>!"), ChatColor.RED + "" + ChatColor.BOLD + "CANCELLED");
                    sunkenPlayer.getPrompt().onCancel(player);
                    sunkenPlayer.setPrompt(null);
                    sunkenPlayer.setMapCreator(null);
                } else {
                    String message = event.getMessage();

                    if (sunkenPlayer.getPrompt().onSubmit(player, message)) {
                        if (sunkenPlayer.getMapCreator().getPrompts().get(sunkenPlayer.getMapCreator().getCurrentPrompt() + 1) != null) {
                            sunkenPlayer.setPrompt(sunkenPlayer.getMapCreator().getPrompts().get(sunkenPlayer.getMapCreator().getCurrentPrompt() + 1));
                            sunkenPlayer.getMapCreator().setCurrentPrompt(sunkenPlayer.getMapCreator().getCurrentPrompt() + 1);
                        }
                    } else {
                        Prompt p = sunkenPlayer.getPrompt();
                        sunkenPlayer.setPrompt(p);
                    }
                }

                event.setCancelled(true);
                return;
            }
        } else if (prompt != null) {
            if (event.getMessage().equalsIgnoreCase("cancel")) {
                sunkenPlayer.sendMessage(MsgType.DANGER, XSound.BLOCK_NOTE_BLOCK_BASS, new TextComponent("Action <p>!"), ChatColor.RED + "" + ChatColor.BOLD + "CANCELLED");
                sunkenPlayer.getPrompt().onCancel(player);
                sunkenPlayer.setPrompt(null);
            } else {
                String message = event.getMessage();

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        boolean success = prompt.onSubmit(player, message);

                        if (success) {
                            sunkenPlayer.setPrompt(null);
                        } else {
                            sunkenPlayer.sendMessage(MsgType.DANGER, XSound.BLOCK_NOTE_BLOCK_BASS, new TextComponent("Invalid input. Please try again."));
                        }
                    }
                }.runTask(Anchor.getPlugin(Anchor.class));
            }

            event.setCancelled(true);
            return;
        }

        if (sunkenPlayer.getChatFocus() == null) sunkenPlayer.setChatFocus(Anchor.getPlugin(Anchor.class).getChannelManager().getByName("Global"), true);

        sunkenPlayer.getChatFocus().sendMessage(sunkenPlayer, event.getMessage());
        event.setCancelled(true);
    }

}