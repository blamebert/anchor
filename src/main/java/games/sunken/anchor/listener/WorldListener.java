package games.sunken.anchor.listener;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.chat.MsgType;
import games.sunken.anchor.player.SunkenPlayer;
import games.sunken.anchor.util.XSound;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.WorldLoadEvent;

public class WorldListener implements Listener {

    private Anchor plugin;

    public WorldListener(Anchor plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onWorldload(WorldLoadEvent event) {
        plugin.getMapManager().getWaitingForWorld().stream().filter(map -> map.getInfo().getUUID().equals(event.getWorld().getName())).findFirst().ifPresent(map -> map.setWorld(event.getWorld()));
        plugin.getMapManager().getWaitingForWorld().removeIf(map -> map.getInfo().getUUID().equals(event.getWorld().getName()));

        for (SunkenPlayer sunkenPlayer : plugin.getPlayerManager().getOnlinePlayers()) {
            if (sunkenPlayer.getMapCreator() != null) {
                if (sunkenPlayer.getMapCreator().getUUID().toString().equals(event.getWorld().getName())) {
                    if (sunkenPlayer.getMapCreator().getMapInfo() != null) {
                        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> plugin.getMapManager().refreshMaps(plugin.getGame().getName()));
                    }

                    event.getWorld().setAutoSave(false);

                    sunkenPlayer.getBukkitPlayer().teleport(event.getWorld().getSpawnLocation());
                    sunkenPlayer.setMapCreator(null);
                    sunkenPlayer.sendMessage(MsgType.SUCCESS, XSound.ENTITY_PLAYER_LEVELUP, new TextComponent("Map successfully loaded!"));
                    break;
                }
            }
        }
    }

}