package games.sunken.anchor.server;

import games.sunken.anchor.Anchor;
import games.sunken.anchor.database.redis.Redis;
import games.sunken.anchor.database.redis.RedisModule;
import games.sunken.anchor.game.state.GameState;
import games.sunken.anchor.util.XMaterial;
import redis.clients.jedis.Jedis;

import javax.annotation.Nonnull;
import java.util.function.BiConsumer;

public class ServerDataRedisModule implements RedisModule {

    @Nonnull
    @Override
    public String getName() {
        return "Dynamic Server Config Receiver";
    }

    @Nonnull
    @Override
    public String getChannel() {
        return Anchor.DYNAMIC_SERVER_CHANNEL;
    }

    @Override
    public boolean isDaemonThread() {
        return true;
    }

    @Override
    public BiConsumer<String, String> onMessage() {
        return (channel, message) -> {
            ServerData data = Anchor.getPlugin(Anchor.class).getGSON().fromJson(message, ServerData.class);
            Anchor.getPlugin(Anchor.class).getServerCache().put(data.getId(), data);
        };
    }

    @Override
    public BiConsumer<String, Integer> onSubscribe() {
        return (s, integer) -> Anchor.getPlugin(Anchor.class).getLogger().info("*** Dynamic Server Module Started! ***");
    }

    @Override
    public BiConsumer<String, Integer> onUnsubscribe() {
        return (s, integer) -> Anchor.getPlugin(Anchor.class).getLogger().info("*** Dynamic Server Module Stopped! ***");
    }

    @Override
    public Runnable onDisable() {
        return () -> {
            Anchor plugin = Anchor.getPlugin(Anchor.class);

            plugin.setServerData(new ServerData(
                    plugin.getServerId(),
                    plugin.getServer().getIp() + ":" + plugin.getServer().getPort(),
                    plugin.getServer().getOnlinePlayers().size(),
                    plugin.getServer().getMaxPlayers(),
                    "Restarting...",
                    GameState.JoinPermission.DENIED,
                    XMaterial.REDSTONE_BLOCK
            ));

            try (Jedis jedis = Redis.getJedisPool().getResource()) {
                jedis.publish(Anchor.DYNAMIC_SERVER_CHANNEL, plugin.getGSON().toJson(plugin.getServerData()));
            }
        };
    }

}