package games.sunken.anchor.server;

import games.sunken.anchor.game.state.GameState;
import games.sunken.anchor.util.XMaterial;

public class ServerData {

    private String id;
    private String ip;

    private int players;
    private int maxPlayers;

    private String currentState;
    private GameState.JoinPermission joinPermission;
    private XMaterial icon;

    public ServerData(String id, String ip, int players, int maxPlayers, String currentState, GameState.JoinPermission joinPermission, XMaterial icon) {
        this.id = id;
        this.ip = ip;
        this.players = players;
        this.maxPlayers = maxPlayers;
        this.currentState = currentState;
        this.joinPermission = joinPermission;
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public String getIp() {
        return ip;
    }

    public int getPlayers() {
        return players;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public String getCurrentState() {
        return currentState;
    }

    public GameState.JoinPermission getJoinPermission() {
        return joinPermission;
    }

    public XMaterial getIcon() {
        return icon;
    }

}