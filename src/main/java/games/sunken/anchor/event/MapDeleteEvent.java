package games.sunken.anchor.event;

import games.sunken.anchor.game.map.MapInfo;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class MapDeleteEvent extends Event {

    private static HandlerList handlers = new HandlerList();

    private MapInfo mapInfo;

    public MapDeleteEvent(MapInfo mapInfo) {
        this.mapInfo = mapInfo;
    }

    public MapInfo getMapInfo() {
        return mapInfo;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

}