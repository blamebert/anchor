package games.sunken.anchor.event;

import games.sunken.anchor.player.SunkenPlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.Map;

public class PlayerPreSetPermissionsEvent extends Event {

    private static HandlerList handlers = new HandlerList();

    private SunkenPlayer sunkenPlayer;
    private Map<String, Boolean> proposedPermissions;

    public PlayerPreSetPermissionsEvent(SunkenPlayer sunkenPlayer, Map<String, Boolean> proposedPermissions) {
        this.sunkenPlayer = sunkenPlayer;
        this.proposedPermissions = proposedPermissions;
    }

    public SunkenPlayer getSunkenPlayer() {
        return sunkenPlayer;
    }

    public Map<String, Boolean> getProposedPermissions() {
        return proposedPermissions;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

}