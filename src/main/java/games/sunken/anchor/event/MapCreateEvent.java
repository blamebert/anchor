package games.sunken.anchor.event;

import games.sunken.anchor.game.map.Map;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class MapCreateEvent extends Event {

    private static HandlerList handlers = new HandlerList();

    private Map map;

    public MapCreateEvent(Map map) {
        this.map = map;
    }

    public Map getMap() {
        return map;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

}